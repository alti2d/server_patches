class GetTimeCmd extends Command {
   public static void init() {
   }

   public GetTimeCmd(fe game) {
      super(game, "getTime", PermissionGroup.admin);
   }

   protected void execute(int[] a, String... b) {
      cV gameMode = this.getGame().T();
      int round = -1;
      float game = -1;

      // If the game mode is a one-life game mode there are multiple rounds per game,
      // we calculate the time in the current round.
      if (gameMode instanceof bX) {
         bX oneLifeMode = (bX) gameMode;
         round = oneLifeMode.getInitialRoundTime() - oneLifeMode.getRoundTime();
      }

      // If the game mode is timed, we can get the timer for the entire game.
      if (gameMode.getTimer() != null) {
         game = gameMode.getTimer().c() - gameMode.getTimer().h();
      }

      da log = JL.a("getTime");
      log.a("roundTimeTicks", round);
      log.a("gameTimeTicks", game);
      log.a("source", this.uuid);
      this.log(log);
   }
}
