import mods.Config;
import org.apache.log4j.Logger;

import java.util.List;

// AssignTeamCmd
public class za extends Os {
   private static final Logger LOG = org.apache.log4j.Logger.getLogger(za.class);
   private Sz nickname;
   //private WP b;
   private IntParam team;

   public za(fe var1) {
      super("assignTeam", var1);
      this.nickname = new Sz(var1);
      this.team = new IntParam("team", "team");
      this.a(new JI[]{this.nickname, this.team});
      //this.b = new WP("team", "team", new Integer[]{Integer.valueOf(-1), Integer.valueOf(0), Integer.valueOf(1)}, (String[])null);
      //this.a(new JI[]{this.a, this.b});
   }

   protected void a(int[] var1, String... var2) {
      int playerNo = ((JK) this.nickname.e()).c();
      if (playerNo >= 0) {
         mF player = this.j.J().u().c(playerNo);

         if (player == mF.c) {
            LOG.warn("Invalid player in /assignTeam: " + nickname + " (" + playerNo + ")");
         }

         mN plane = player.i();
         if (plane == null || !plane.aO()) {
            plane = null;
         }

         List gameModeTeams = this.j.T().i();
         int teamNo = this.team.value;

         if (gameModeTeams.size() > 2) {
            teamNo = teamNo - 3;
            if (teamNo < -1) teamNo = -1;
         }

         oD team = oD.c;
         if (teamNo >= 0 && teamNo < gameModeTeams.size()) {
            team = (oD) gameModeTeams.get(teamNo);
         }

         LOG.info("Updaing team for player " + player.e().a().E() + "(" + playerNo + ", " + this.nickname +
                 ")" + " from " + player.e().c().toString() +
                 " to: " + team.toString() + "(" + teamNo + ")");

         fv event = new fv(this.j, player.h(), team, plane);
         this.j.u().c().l().a((kY) event);
         LOG.info("Player's team is: " + player.e().c().toString());
      }

   }

   protected boolean a(NB ret, FW var2, Jt var3, String[] var4) {
      if (this.j.T().i().size() != 2) {
         boolean allow_ffa = Config.get(port()).ffa_allow_assign_team;

         if (!allow_ffa) {
            ret.a(ret.c() + "assignTeam only applies to team game modes. " +
                    "(try enabling ffa_allow_assign_team for port "+port()+" in mods.hjson)", false);
            return false;
         } else {
            JK player = (JK) this.nickname.e();
            if (player != null && player.c() >= 0) {
               int team = this.team.value - 3;
               if (team < -1) team = -1;

               if (team >= -1 && team < this.j.T().i().size()) {
                  if (!this.checkTouramnet(team, player)) {
                     ret.a(ret.c() + "Tournament in progress: " + player.a() + " is not allowed on team " + team, false);
                     return false;
                  }

                  ret.a("assigning " + player.a() + " to team " + team, true);
                  return true;
               } else {
                  ret.a("assignTeam Invalid team", false);
                  return false;
               }
            } else {
               ret.a("assignTeam player not found", false);
               return false;
            }
         }
      } else {
         JK player = (JK) this.nickname.e();
         if (player != null && player.c() >= 0) {
            int team = this.team.value;
            if (team >= -1 && team <= 1) {
               if (!this.checkTouramnet(team, player)) {
                  ret.a(ret.c() + "Tournament in progress: " + player.a() + " is not allowed on team " + team, false);
                  return false;
               }

               String var9 = team == -1 ? "spectator" : (team == 0 ? "left_team" : "right_team");
               ret.a("assigning " + player.a() + " to " + var9, true);
               return true;
            } else {
               ret.a("assignTeam valid teams are [-1=spectator, 0=left_team, 1=right]", false);
               return false;
            }
         } else {
            ret.a("assignTeam player not found", false);
            return false;
         }
      }
   }

   private boolean checkTouramnet(int team, JK player) {
      if (this.j.u().c().l().r() && team != -1) {
         oD var7 = this.j.u().c().l().c(player.b());
         oD var8 = (oD) this.j.T().i().get(team);
         if (!ty.a((Object) var7, (Object) var8)) {
            return false;
         }
      }
      return true;
   }

   public String a() {
      return "";
   }

   public String b() {
      return "";
   }
}
