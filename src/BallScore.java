import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

// The obfuscated version is `do.class`, which causes problems as 'do' is
// a reserved keyword, so we recompile it with a better name to fix.
public class BallScore extends QJ {
   private Map a;
   private Map b;
   private ArrayList c;
   private NY d;
   private NY e;

   public BallScore(fe var1, oD var2, oD var3, oD var4, Map var5, Map var6) {
      super(var1, sx.d(30.0F), var2);
      this.a = var5;
      this.b = var6;
      this.d = var1.G().a(KN.A);
      this.e = var1.G().a(KN.r);
      this.c = new ArrayList();
      this.c.add(var3);
      this.c.add(var4);
      Collections.sort(this.c);
      if (this.c.contains(var2) && !((oD) this.c.get(0)).equals(var2)) {
         Collections.reverse(this.c);
      }

   }

   public void a(float var1) {
      super.a(var1);
      byte var2 = 0;
      qd.k();
      qd.b(80.0F, 410.0F);
      if (ty.a((Object) this.d(), (Object) oD.c)) {
         qd.b(ip.a);
         this.d.a("Tie!", 225, 310 + var2, 17);
      } else {
         qd.b(this.d().f());
         this.d.a(this.d().h() + " Wins", 225, 310 + var2, 17);
      }

      boolean var3 = false;
      boolean var4 = false;
      int var5 = 310 - this.d.c() - this.d.c() + var2;
      StringBuilder var6 = new StringBuilder();

      int var7;
      oD var8;
      for (var7 = 0; var7 < this.c.size(); ++var7) {
         var8 = (oD) this.c.get(var7);
         var6.append(((Integer) this.a.get(var8)).intValue());
         if (var7 + 1 < this.c.size()) {
            var6.append(" - ");
         }
      }

      int var13 = this.d.a((CharSequence) var6.toString());
      int var14 = (450 - var13) / 2;

      for (var7 = 0; var7 < this.c.size(); ++var7) {
         var8 = (oD) this.c.get(var7);
         qd.b(var8.f());
         String var9 = String.valueOf(((Integer) this.a.get(var8)).intValue());
         this.d.a(var9, var14, var5, 36);
         var14 += this.d.a((CharSequence) var9);
         if (var7 + 1 < this.c.size()) {
            qd.b(ip.a);
            String var10 = " - ";
            this.d.a(var10, var14, var5, 36);
            var14 += this.d.a((CharSequence) var10);
         }
      }

      var5 -= 35;
      qd.b(ip.a);
      this.e.a("Possession %", 225, var5, 33);
      var5 -= 26;
      oD var15 = (oD) this.c.get(0);
      var8 = (oD) this.c.get(1);
      int var16 = ((AF) this.b.get(var15)).a;
      int var17 = ((AF) this.b.get(var8)).a;
      int var11 = var16 + var17;
      int var12 = var11 == 0 ? 50 : Math.round((float) var16 * 100.0F / (float) var11);
      qd.b(var15.f());
      this.e.a(Qk.a(var12), 215, var5, 40);
      qd.b(var8.f());
      this.e.a(Qk.a(100 - var12), 235, var5, 36);
      qd.l();
   }
}
