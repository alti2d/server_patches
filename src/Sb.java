import org.apache.log4j.Logger;

// EMP Event
// - add turretEmp JSON log event
public class Sb extends kY {
   private static final Logger a = Logger.getLogger(Sb.class);
   private int b;
   private kK c;
   private int d;
   private int e;

   public Sb(fe var1) {
      super(var1);
   }

   public Sb(fe var1, int var2, D var3, int var4, int var5) {
      this(var1);
      this.b = var2;
      this.c = var1.a((nq) var3);
      this.d = var4;
      this.e = var5;
   }

   public void a() {
      D var1 = (D) this.c.b(this.e());
      if (var1 instanceof mN) {
         mN var2 = (mN) var1;
         if (var2.aO()) {
            var2.a(this.d, this.b, this.e);
         }
      } else if (var1 instanceof Ww) {
         Ww var3 = (Ww) var1;

         // ==================
         if (this.e().u().c() != null) {
            da log = JL.a("turretEmp");
            log.a("player", this.b);
            this.e().u().c().a(log);
         }
         // ==================

         if (var3.K()) {
            var3.f(this.d);
         }
      } else {
         a.info("EEVT?: " + var1.getClass());
      }

   }

   public void a(nQ var1) {
      this.b = Vh.b(var1);
      this.c = this.e().a(var1, D.class);
      this.d = var1.b(12);
      this.e = var1.b(7);
   }

   public void a(nu var1) {
      Vh.a(var1, this.b);
      this.e().a(var1, this.c);
      var1.b(12, this.d);
      var1.b(7, this.e);
   }
}
