import mods.Config;

import java.util.*;

// SpawnReqEv extends Event
public class xX extends kY {
   private jV /*PlaneSetup*/ setup;
   private UC /*PlaneRandomType*/ random;
   private oD /*Team*/ team;

   public xX(fe /*AltGame*/ game) {
      super(game);
   }

   // shouldBroadcast
   public boolean b() {
      return false;
   }

   public xX(fe game, jV setup, UC random, oD team) {
      this(game);
      this.setup = setup;
      this.random = random;
      this.team = team;
   }

   // Decides which team a bot should spawn on.
   public static oD a(fe game, Jt client) {
      oD var2 = client.n();
      ArrayList var3 = new ArrayList(game.J().u().j());
      var3.remove(client.h());
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;
      int var7 = 0;
      int var8 = 0;
      List var9 = game.T().i();
      sT var10 = game.J().u().ak();
      int var13;
      int var14;
      if (var10.s() && var9.size() == 2) {
         oD var11 = (oD) var9.get(0);
         oD var12 = (oD) var9.get(1);
         var13 = 0;
         var14 = 0;
         Iterator var16 = var3.iterator();

         while (var16.hasNext()) {
            mF var15 = (mF) var16.next();
            oD var17 = var15.e().c();
            boolean var18 = var15.a();
            if (var17.equals(var11)) {
               ++var13;
               if (var18) {
                  ++var4;
               } else {
                  ++var7;
               }
            } else if (var17.equals(var12)) {
               ++var14;
               if (var18) {
                  ++var5;
               } else {
                  ++var8;
               }
            } else if (var17.equals(oD.c) && var18) {
               ++var6;
            }
         }

         if (var13 < var14) {
            var2 = var11;
         } else if (var14 < var13) {
            var2 = var12;
         }
      }

      int var19 = 0;
      int var20 = 0;
      var13 = 0;
      Iterator var22 = var3.iterator();

      while (var22.hasNext()) {
         mF var21 = (mF) var22.next();
         if (!var21.e().c().equals(oD.c)) {
            ++var13;
         }

         if (var21.a()) {
            ++var19;
         } else {
            ++var20;
         }
      }

      if (var20 == 0) {
         var2 = oD.c;
      } else if (var13 >= var10.t() && var9.size() == 2) {
         if (Math.abs(var7 - var8) <= var4 + var5) {
            var2 = oD.c;
         }
      } else if (var13 < var10.t() && var9.size() == 2) {
         var14 = var7 + var4;
         int var23 = var8 + var5;
         if (var14 == var23 && var6 == 0) {
            var2 = oD.c;
         }
      }

      return var2;
   }

   // apply()
   public void a() {


      //dp playerCont = this.e().J().u();

      VS /*GameHdr*/ hdr = this.e().u().c().l();
      Jt /*Client*/ client = this.d();
      oD /*Team*/ team = this.team;
      String reason = "";
      int var5 = this.e().T().K();

      int port = this.e().u().c().port();
      if (client.m()) { //if(client.isBot())
         boolean preventSwitch = Config.get(port).bots.prevent_team_change;
         if (preventSwitch) {
            //team = client.n();
         }
         else team = a(this.e(), client);
         this.setup.b(hdr.a(this.setup.c()));
      }

      boolean botsIgnoreTournament = Config.get(port).bots.allow_spawn_tournament;
      if (hdr.r() && !(botsIgnoreTournament && client.m())) {
         team = hdr.c(client.f().D());
         if (ty.a((Object) oD.c, (Object) team)) {
            reason = "^WHITE^A tournament is in progress.  You cannot spawn until /stopTournament.";
         }
      }

      boolean denySpawn = false;
      UUID vaporID = client.f().D();
      boolean allowSpawn = Config.get(this.e().u().c().port()).allow_spawn;
      if (vaporID != null && hdr.canSpawn.containsKey(vaporID)) {
         if (!hdr.canSpawn.get(vaporID)) {
            denySpawn = true;
         }
      } else if(!allowSpawn) {
         denySpawn = true;
      }

      if(denySpawn) {
         if (hdr.spawnDenyReason.containsKey(vaporID)) {
            reason = hdr.spawnDenyReason.get(vaporID);
         } else {
            reason = Config.get(port).deny_spawn_reason;
         }
      }

      if (reason.equals("")) {
         if (!hdr.blockedPerks.isEmpty()) {
            if (this.setup == jV.b || this.setup == jV.c) {
               reason = "Some perks have been blocked; Random is disabled.";
            }
         }
      }

      if (reason.equals("")) {
         HashSet allowedPerks = hdr.allowedPerks.get(vaporID);

         jV setup = client.h().e().b();

         if (setup == jV.b && hdr.blockedPerks.containsKey("Full Random")
             && (allowedPerks == null || allowedPerks.contains("Full Random"))) {
            reason = hdr.blockedPerks.get("Full Random");
         }

         if (setup == jV.c && hdr.blockedPerks.containsKey("Config Random")
             && (allowedPerks == null || allowedPerks.contains("Config Random"))) {
            reason = hdr.blockedPerks.get("Config Random");
         }
      }

      if (reason.equals("")) {
         HashSet allowedPerks = hdr.allowedPerks.get(vaporID);

         String redPerk = this.setup.i().b();
         if (hdr.blockedPerks.containsKey(redPerk) &&
                 (allowedPerks == null || !allowedPerks.contains(redPerk))) {
            reason = hdr.blockedPerks.get(redPerk);
         }

         String greenPerk = this.setup.k().b();
         if (hdr.blockedPerks.containsKey(greenPerk) &&
                 (allowedPerks == null || !allowedPerks.contains(greenPerk))) {
            reason = hdr.blockedPerks.get(greenPerk);
         }

         String bluePerk = this.setup.l().b();
         if (hdr.blockedPerks.containsKey(bluePerk) &&
                 (allowedPerks == null || !allowedPerks.contains(bluePerk))) {
            reason = hdr.blockedPerks.get(bluePerk);
         }
      }

      if (hdr.n().J().u().ak().A()) {
         reason = "^WHITE^Spawning is disabled.";
      }

      if (hdr.a(this.random)) {
         reason = "^WHITE^Plane random type \'" + this.random.d + "\' is not allowed on this server.";
      }

      mF var6 = client.h();
      var6.a(new Or(var6.e(), team));
      LK var7;
      if (ty.a((Object) oD.c, (Object) team)) {
         var7 = (LK) this.e().R().f().h().get(0);
      } else {
         var7 = this.e().T().b(var6);
      }

      float var8 = var7.f_().c;
      float var9 = var7.f_().d;
      float var10 = var7.D();
      Dl var11 = hdr.a(var6);
      if (var11 != null) {
         var8 = var11.a;
         var9 = var11.b;
         var10 = var11.c;
      }

      client.a((kY) (new nE(this.e(), this.setup, team, var8, var9, var10, reason, var5)));
   }

   public void a(nQ var1) {
      this.team = (oD) oD.p.b(var1);
      this.setup = (jV) jV.d.b(var1);
      this.random = (UC) UC.e.b(var1);
   }

   public void a(nu var1) {
      oD.p.a(this.team, var1);
      jV.d.a(this.setup, var1);
      UC.e.a(this.random, var1);
   }
}
