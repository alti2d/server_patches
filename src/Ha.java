import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

// Entity Encoder
// -- changed `yF.class` to `Ww.Shoot.Bullet.class` (Ww = Turret)
public class Ha extends bx {
   private Map a;

   public Ha(fe var1) {
      super(new Object[]{var1});
   }

   public void a() {
      this.a(nk.class);
      this.a(aaH.class);
      this.a(gE.class);
      this.a(sM.class);
      this.a(rK.class);
      this.a(S.class);
      this.a(Nd.class);
      this.a(Fa.class);
      this.a(hx.class);
      this.a(Iv.class);
      this.a(Br.class);
      this.a(wf.class);
      this.a(gJ.class);
      this.a(tA.class);
      this.a(Rz.class);
      this.a(HB.class);
      this.a(uV.class);
      this.a(wD.class);
      this.a(Sw.class);
      this.a(ot.class);
      this.a(kC.class);
      this.a(TG.class);
      this.a(YI.class);
      this.a(Pr.class);
      this.a(HM.class);
      this.a(Eb.class);
      this.a(Ww.class);
      this.a(Ww.Shoot.Bullet.class);
      this.a(DP.class);
      this.a(GF.class);
      this.a(aaE.class);
      this.a(Xq.class);
      this.a(ZZ.class);
      this.a(Gu.class);
      this.a(Oy.class);
      this.a(tc.class);
      this.a(ND.class);
      this.a(vS.class);
      this.a(JW.class);
      this.a(Jg.class);
   }

   public void c() {
      this.a = new HashMap();
      Iterator var2 = this.b().entrySet().iterator();

      while (var2.hasNext()) {
         Entry var1 = (Entry) var2.next();
         Class var3 = (Class) var1.getKey();
         int var4 = ((Integer) var1.getValue()).intValue();
         OF var5 = new OF(this);
         var5.a = (nq) this.a(var4);
         var5.b = var5.a.d();
         this.a.put(var3, var5);
      }

   }

   public JB b(Class var1) {
      if (this.a.containsKey(var1)) {
         return ((OF) this.a.get(var1)).b;
      } else {
         throw new RuntimeException("Must register " + var1.getName() + " with " + this.getClass().getSimpleName());
      }
   }

   // No idea what this is, it doesn't seem to do much
   static class OF {
      nq a;
      JB b;
      // $FF: synthetic field
      final Ha c;

      private OF(Ha var1) {
         this.c = var1;
      }
   }
}
