class EndRoundCmd extends Command {

   public EndRoundCmd(fe game) {
      super(game, "endRound", PermissionGroup.admin);
      this.setParams(new JI[]{});
   }

   protected void execute(int[] a, String... b) {
      this.getGame().T().G();
   }
}
