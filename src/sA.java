import java.util.*;
import java.util.Map.Entry;

// TDM Game mode
// - Added some methods for the /overrideTdmScore command
public class sA extends cV {
   private JN a;
   private List b;
   private oD c;
   private oD e;
   private Map f = new HashMap();
   private Map g = new HashMap();
   private oD h;
   private long i;
   private LK[] j;
   private Bu[] k;
   private Map l = new HashMap();

   public Map getScores() {
      return this.f;
   }

   public oD team_a() {
      return this.c;
   }

   public oD team_b() {
      return e;
   }

   public sA(DK var1, JN var2) {
      super(var1, var2);
      this.a = var2;
      List var3 = var1.h().f().h();
      this.j = (LK[]) var3.toArray(new LK[var3.size()]);
      this.k();
      LK var4 = null;
      HashSet var5 = new HashSet();
      LK[] var9 = this.j;
      int var8 = this.j.length;

      for (int var7 = 0; var7 < var8; ++var7) {
         LK var6 = var9[var7];
         if (!var6.aU().equals(oD.c)) {
            if (var4 == null || var6.f_().c < var4.f_().c) {
               var4 = var6;
            }

            var5.add(var6.aU());
         }
      }

      this.c = var4.aU();
      var5.remove(this.c);
      this.e = (oD) var5.iterator().next();
      this.b = nT.a((Object[]) (new oD[]{this.c, this.e}));
      Iterator var11 = nT.a((List) this.b, (Object[]) (new oD[]{oD.c})).iterator();

      while (var11.hasNext()) {
         oD var10 = (oD) var11.next();
         this.f.put(var10, new Integer(0));
         this.g.put(var10, new Integer(0));
      }

   }

   private void k() {
      List var1 = nT.a((Object[]) this.j);
      ArrayList var2 = new ArrayList();

      for (int var3 = 0; var3 < var1.size(); ++var3) {
         ArrayList var4 = new ArrayList();
         LK var5 = (LK) var1.remove(var3--);
         var4.add(var5);

         for (int var6 = var3 + 1; var6 < var1.size(); ++var6) {
            LK var7 = (LK) var1.get(var6);
            if (var5.f_().h(var7.f_()) < 40000.0F) {
               var1.remove(var6--);
               var4.add(var7);
            }
         }

         var2.add(new Bu(this, var4));
      }

      this.k = (Bu[]) var2.toArray(new Bu[var2.size()]);
   }

   public String c() {
      return "Team Deathmatch";
   }

   public String a(oD var1) {
      return this.a.a() == 0 ? "Rack up the most kills before the timer expires." : "First team to achieve " + this.a.a() + " kills wins.";
   }

   public void a() {
      super.a();
      this.i = 0L;
      Iterator var2 = this.b.iterator();

      while (var2.hasNext()) {
         oD var1 = (oD) var2.next();
         this.f.put(var1, Integer.valueOf(0));
      }

   }

   public void b() {
      if (!this.w()) {
         int var1 = this.a.a();
         if (var1 > 0) {
            int var2 = Math.max(((Integer) this.f.get(this.c)).intValue(), ((Integer) this.f.get(this.e)).intValue());
            if (var2 >= var1 && this.t().B()) {
               this.G();
            }
         }
      }

   }

   public LK b(mF var1) {
      LK var2 = null;
      long var3 = this.B();
      if (this.i == 0L) {
         this.i = var3;
      }

      boolean var5 = var3 - this.i > 300L;
      if (!var5) {
         var2 = super.b(var1);
      } else {
         oD var6 = var1.e().c();
         List var7 = this.t().J().u().k();
         ArrayList var8 = new ArrayList();

         int var9;
         for (var9 = 0; var9 < var7.size(); ++var9) {
            mN var10 = (mN) var7.get(var9);
            if (var10.aO()) {
               var8.add(var10);
            }
         }

         var9 = -9999999;
         ArrayList var22 = new ArrayList();

         for (int var11 = 0; var11 < this.k.length; ++var11) {
            Bu var12 = this.k[var11];

            for (int var13 = 0; var13 < Bu.a(var12).size(); ++var13) {
               LK var14 = (LK) Bu.a(var12).get(var13);
               long var15 = var3 - this.a(var14);
               int var17 = Math.round(dh.a((float) var15, 0.0F, 120.0F, 0.0F, 999.0F));

               for (int var18 = 0; var18 < var8.size(); ++var18) {
                  mN var19 = (mN) var8.get(var18);
                  float var20 = var19.f_().h(Bu.b(var12));
                  if (var20 < 640000.0F) {
                     int var21;
                     if (var20 < 250000.0F) {
                        var21 = 2000;
                     } else {
                        var21 = 1000;
                     }

                     if (!ty.a((Object) var19.aU(), (Object) var6)) {
                        var21 *= -2;
                     }

                     var17 += var21;
                  }
               }

               if (var17 >= var9) {
                  if (var17 > var9) {
                     var22.clear();
                     var9 = var17;
                  }

                  var22.add(var14);
               }
            }
         }

         if (var22.size() > 0) {
            var2 = (LK) dh.a((List) var22);
         }
      }

      if (var2 == null) {
         var2 = super.b(var1);
      }

      this.a(var2, Long.valueOf(var3));
      return var2;
   }

   private void a(LK var1, Long var2) {
      this.l.put(var1, var2);
   }

   private long a(LK var1) {
      Long var2 = (Long) this.l.get(var1);
      if (var2 == null) {
         var2 = Long.valueOf(0L);
         this.l.put(var1, var2);
      }

      return var2.longValue();
   }

   public void a(mN var1, mN var2) {
      if (var1 != null && var2 != null) {
         oD var3 = var1.aU();
         oD var4 = var2.aU();
         if (!ty.a((Object) var3, (Object) var4) && this.b.contains(var3) && this.b.contains(var4)) {
            int var5 = ((Integer) this.f.get(var3)).intValue() + 1;
            this.f.put(var3, Integer.valueOf(var5));
         }
      }

   }

   public List i() {
      return this.b;
   }

   public List l() {
      return Sr.p;
   }

   public void b(float var1) {
      super.b(var1);
      NY var2 = this.t().G().a(KN.r);
      qd.b(this.c.f());
      var2.a(String.valueOf(this.f.get(this.c)), 540, 715, 17);
      qd.b(this.e.f());
      var2.a(String.valueOf(this.f.get(this.e)), 740, 715, 17);
      if (this.x()) {
         float var3 = 1000.0F * this.e(var1);
         float var4 = 1000.0F * this.y();
         String var5 = me.a((long) var3, (long) var4);
         qd.a(0.8F, 0.8F, 0.8F, 1.0F);
         var2.a(var5, 640, 712, 17);
      }

   }

   public void a(nQ var1) {
      super.a(var1);
      Iterator var3 = this.b.iterator();

      while (var3.hasNext()) {
         oD var2 = (oD) var3.next();
         this.g.put(var2, Integer.valueOf(var1.a(0, 31)));
         this.f.put(var2, Integer.valueOf(var1.a(0, '\uffff')));
      }

   }

   public void a(nu var1) {
      super.a(var1);
      Iterator var3 = this.b.iterator();

      while (var3.hasNext()) {
         oD var2 = (oD) var3.next();
         var1.a(0, 31, ((Integer) this.g.get(var2)).intValue());
         var1.a(0, '\uffff', ((Integer) this.f.get(var2)).intValue());
      }

   }

   public void a(gk var1) {
      this.h = oD.c;
      int var2 = ((Integer) this.f.get(this.c)).intValue();
      int var3 = ((Integer) this.f.get(this.e)).intValue();
      if (var2 != var3) {
         this.h = var2 > var3 ? this.c : this.e;
      }

      oD.p.a(this.h, var1);
      this.c(this.h);
      super.a(var1);
   }

   public void a(LG var1) {
      this.h = (oD) oD.p.b(var1);
      Integer var2 = (Integer) this.g.get(this.h);
      Integer var3 = Integer.valueOf(var2.intValue() + 1);
      this.g.put(this.h, var3);
      super.a(var1);
   }

   public void a(Hc var1) {
      Iterator var3 = this.f.entrySet().iterator();

      while (var3.hasNext()) {
         Entry var2 = (Entry) var3.next();
         var1.a((oD) var2.getKey(), ((Integer) var2.getValue()).intValue());
      }

   }

   public yr j() {
      Eb var1 = null;
      if (this.h != oD.c) {
         var1 = new Eb(this.t());
         var1.a(this.h);
         lE var2 = new lE();
         var2.d = this.h == this.e;
         var1.a(var2);
         var1.c(this.t());
      }

      return new Wu(this.t(), this.h, this.c, this.e, this.f);
   }

   public String a(mF var1) {
      int var2 = ((Integer) var1.g().a.a()).intValue() - ((Integer) var1.g().b.a()).intValue();
      return var2 + " net kills";
   }

   public Comparator o() {
      return new je(this);
   }

   static class Bu {
      private Yr b;
      private List c;
      // $FF: synthetic field
      final sA a;

      public Bu(sA var1, List var2) {
         this.a = var1;
         this.c = var2;
         int var3 = var2.size();
         float var4 = 0.0F;
         float var5 = 0.0F;

         for (int var6 = 0; var6 < var3; ++var6) {
            Yr var7 = ((LK) var2.get(var6)).f_();
            var4 += var7.c;
            var5 += var7.d;
         }

         this.b = new Yr(var4 / (float) var3, var5 / (float) var3);
      }

      // $FF: synthetic method
      static List a(Bu var0) {
         return var0.c;
      }

      // $FF: synthetic method
      static Yr b(Bu var0) {
         return var0.b;
      }
   }
}
