import java.util.HashSet;
import java.util.UUID;

class SetSpecChatCmd extends Command {
   private IntParam player_no;
   private GO enabled;

   public SetSpecChatCmd(fe game) {
      super(game, "setSpecChat", PermissionGroup.admin);
      this.player_no = new IntParam("playerNo", "Player number");
      this.enabled = new GO("enabled", "True to block messages from spectators for this player");
      this.setParams(new JI[]{player_no, enabled});
   }

   protected void execute(int[] a, String... b) {
      this.getServer().setSpecChat(this.player_no.value, (Boolean) this.enabled.e());
   }
}
