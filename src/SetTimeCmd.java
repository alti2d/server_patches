class SetTimeCmd extends Command {
   private IntParam time;

   public SetTimeCmd(fe game) {
      super(game, "setTime", PermissionGroup.admin);
      this.time = new IntParam("time", "seconds * 30");
      this.setParams(new JI[]{time});
   }

   protected void execute(int[] a, String... b) {
      cV gameMode = this.getGame().T();
      oU timer = gameMode.getTimer();
      // The game mode timer is a countdown timer - the values stored are the
      // initial value and the current value - the current value starts at the
      // initial value then decreases until it get's to 0, at which point the
      // timer has expired.

      if (timer != null) {
         // timer.setCurrent(timer.getInitial() - time.value);
         timer.c(timer.c() - this.time.value);

         // Send a new GameModeStatusEv to all players
         fN server = this.getServer();
         VS gameMsgHandler = server.l();
         h gameModeEv = new h(this.getGame(), gameMsgHandler);
         gameMsgHandler.a(gameModeEv);
      }
   }
}
