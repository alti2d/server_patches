// PlayerInfoEv extends Event
public class Bi extends kY {
   private boolean connected;
   private int playerNo;
   private Or playerInfo;
   private String leaveReason;

   public Bi(fe game) {
      super(game);
   }

   public Bi(fe game, boolean connected, int playerNo, Or info) {
      this(game);
      this.connected = connected;
      this.playerNo = playerNo;
      this.playerInfo = info;
   }

   public void a(String var1) {
      this.leaveReason = var1;
   }

   public boolean c() {
      boolean shouldApply = super.c();
      if (this.d() != null) {
         shouldApply &= this.d().g() == this.playerNo;
      }

      VS gameHdr = this.e().u().c().l();
      UC randomType = dp.a(this.playerInfo);
      if (this.playerInfo.c() != oD.c && gameHdr.a(randomType)) {
         this.playerInfo = new Or(this.playerInfo, oD.c);
         Jt client = this.e().u().c().l().a(this.playerNo);
         if (client != null) {
            String msg = "Plane random type \'" + randomType.d + "\' is not allowed.";
            client.a(new DT(this.e(), -1, false, msg));
            mF player = this.e().J().u().c(this.playerNo);
            mN plane = player.i();
            if (plane == null || !plane.aO()) {
               plane = null;
            }

            client.a(new fv(this.e(), client.g(), this.playerInfo.c(), plane));
         }
      }

      return shouldApply;
   }

   public void a() {
      if (this.e().u().c() != null) {
         da log = JL.a("playerInfoEv");
         log.a("leaving", !this.connected);
         log.a("player", this.playerNo);
         log.a("team", this.playerInfo.c().c());
         log.a("level", this.playerInfo.d().c());
         log.a("aceRank", this.playerInfo.e());
         log.a("plane", this.playerInfo.b().c().g);
         log.a("perkRed", this.playerInfo.b().i().b());
         log.a("perkGreen", this.playerInfo.b().k().b());
         log.a("perkBlue", this.playerInfo.b().l().b());
         this.e().u().c().a(log);
      }
      this.f().a(this.connected, this.playerNo, this.playerInfo, this.leaveReason);
   }

   public void a(nQ var1) {
      this.connected = var1.d();
      this.playerNo = Vh.b(var1);
      this.leaveReason = (String) Dr.c.b(var1);
      this.playerInfo = (Or) Or.a.b(var1);
   }

   public void a(nu var1) {
      var1.a(this.connected);
      Vh.a(var1, this.playerNo);
      Dr.c.a(this.leaveReason, var1);
      Or.a.a(this.playerInfo, var1);
   }
}
