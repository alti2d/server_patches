import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Base
// - More information added to JSON log events
public class Eb extends dC implements D, ou {
   private static final Logger a = Logger.getLogger(Eb.class);
   private fe game;
   private Rl c;
   private int d;
   private oD e;
   private lE f;
   private Jh g;
   private Jh h;
   private int i;
   private int j;
   private em k;
   private kt l;
   private int m;
   private boolean n;
   private int o;
   private On p;
   private int q;
   private List r;
   private final Yr t;
   private Yr u;
   private Yr v;
   private Yr w;
   private Yr x;
   private static final int y = Math.round(sx.d(0.3F).a());

   public Eb(fe var1) {
      this(var1.G());
   }

   public Eb(Rl var1) {
      super(Yr.a(new float[]{-50.0F, -15.0F, 50.0F, -15.0F, -50.0F, 25.0F}), true);
      this.d = 0;
      this.r = new ArrayList();
      this.t = new Yr();
      this.c = var1;
      this.f = new lE();
      this.a((oD) oD.m.get(0));
      this.E();
   }

   public oD aU() {
      return this.e;
   }

   private static Jh a(Rl var0) {
      return var0.e("map/base/base.poly");
   }

   public void a(oD var1) {
      if (!oD.m.contains(var1)) {
         var1 = oD.d;
      }

      this.e = var1;
      this.g = a(this.c);
      this.a(this.y());
   }

   public int c() {
      return this.d;
   }

   public void a(int var1) {
      this.d = var1;
      this.L();
   }

   public lE y() {
      return this.f;
   }

   public em z() {
      return this.h.e().a();
   }

   public void a(lE var1) {
      this.f = new lE(var1);
      this.h = Ht.a(this.g, var1);
      this.h.e().a(this.c.a("render/map/base/" + this.e.d() + "_front.png"));
      this.k = this.c.a("render/map/base/" + this.e.d() + "_back.png");
      this.a((Yr[]) this.h.a());
      Yr[] var2 = this.e();
      int var3 = -1;
      float var4 = -1.0F;

      for (int var5 = 0; var5 < var2.length; ++var5) {
         float var6 = var2[var5].i(var2[(var5 + 1) % var2.length]);
         if (var6 > var4) {
            var4 = var6;
            var3 = var5;
         }
      }

      this.u = var2[var3];
      this.v = var2[(var3 + 1) % var2.length];
      this.w = this.v.e(this.u);
      this.w.g();
      this.x = this.w.d(90.0F);
   }

   private void L() {
      this.i = DX.e(this.c(), this.h.e().c());
      this.j = DX.b(this.k.l()[0]);
   }

   public int u() {
      return this.i;
   }

   public int o() {
      return this.o;
   }

   public int p() {
      return 0;
   }

   private boolean a(vk var1) {
      this.t.a(var1.a);
      this.t.d(this.f_());
      float var2 = zv.a(this.u, this.v, this.t);
      return var2 < 5.0F;
   }

   public Yr A() {
      return this.u;
   }

   public Yr B() {
      return this.v;
   }

   public Yr C() {
      return this.w;
   }

   public Yr D() {
      return this.x;
   }

   public boolean a(mN var1) {
      boolean var2 = this.aU().equals(var1.aU());
      boolean var3 = var2;
      if (this.game.T() instanceof bX) {
         if (var2) {
            var3 = true;
         } else {
            var3 = var1.ab() instanceof vS;
         }
      }

      return var3;
   }

   public boolean a(float var1, vk var2, Yr var3) {
      if (this.a(var2)) {
         float var4 = dh.b(var1);
         float var5 = dh.c(var1);
         float var6 = var4 * this.w.c + var5 * this.w.d;
         if (Math.abs(var6) > 0.74F) {
            float var7 = var3.f(this.x);
            if (Math.abs(var7) < 4.0F) {
               return true;
            }
         }
      }

      return false;
   }

   public void E() {
      this.m = 4800;
      this.n = true;
      this.o = qf.c | qf.f;
      this.r.clear();
      if (this.game != null && this.game.T() != null) {
         this.game.T().B();
      }

   }

   private void M() {
      this.m = 0;
      this.n = false;
      this.o = 0;
      if (this.p != null) {
         this.p.a();
      }

   }

   public void a(Mb var1) {
      this.M();
      this.b(var1);
   }

   private void b(Mb var1) {
      Yr[] var2 = this.i();
      byte var3 = 6;
      if (this.r.size() > 0) {
         vU.a(this.game, var1, this.f_(), 330, 500, 1.4F, 1.0F, true, -1, true);
         Iterator var5 = this.r.iterator();

         while (var5.hasNext()) {
            JW var4 = (JW) var5.next();
            var4.a();
         }

         this.r.clear();
      }

      for (int var9 = 0; var9 < var3; ++var9) {
         Yr var10 = var2[var9 * var2.length / var3];
         Yr var6 = new Yr(this.f_());
         var6.c(var10);
         vU.a(this.game, var1, var6, 170, 200, 1.2F, 1.0F, true, -1, true);
         kr var7 = this.game.G().b("map/base/base_explode.wav");
         DA var8 = this.game.ah().a(var7, var6, dc.m);
         var8.l();
      }

   }

   public void a(NG var1, vX var2) {
      if (this.n && this.f().a(var1)) {
         var2.a(this.l, this.j);
         var2.a(this, this.i);
      }

   }

   public void a(Mb entity, float var2, float var3) {
      if (this.n && entity.aU() != this.aU()) {
         if (var2 < 165.0F) {
            var2 = (float) ((double) var2 * 0.0D);
         }

         mN var4 = entity instanceof mN ? (mN) entity : null;
         if (this.game.S() && var4 != null && !var4.aT().a()) {
            rT var5 = Ja.R().m().Q().c();
            if (var5.equals(rT.a)) {
               var2 *= 4.0F;
            } else if (var5.equals(rT.b)) {
               var2 *= 2.0F;
            }
         }

         if (var4 != null) {
            var2 *= var4.aH();
         }

         float var13 = dh.a(var2, (float) (this.m - 4800), (float) this.m);
         this.m = (int) ((float) this.m - var13);
         if (var4 != null) {
            var4.aT().g().g.a(var2);
            if (this.game.B()) {
               this.p.a(var4, var13);
            }
         }

         if (this.game.B() && this.m == 0) {
            float damage = this.a(0, entity);
            int xp = (int) damage;

            // Create and send events
            kG ev = new kG(this.game, this, entity); // Create hit event
            this.game.u().c().l().a((kY) ev); // Send event
            if (entity instanceof mN) { // if (entity instanceof Plane) {
               Er var9 = new Er(this.game, (mN) entity, this, 30 + xp);
               this.game.u().c().l().a((kY) var9);
            }

            if (damage > 0.0F) {
               try {
                  da log = JL.a("structureDamage");
                  log.a("player", entity.aS());
                  log.a("target", "base");
                  log.a("xp", xp);
                  log.a("exactXp", (double) damage);
                  log.a("positionX", this.f_().c); // this.getPos().x
                  log.a("positionY", this.f_().d); // this.getPos().y
                  this.game.u().c().a(log);
               } catch (Exception var12) {
                  a.error(var12, var12);
               }
            }
            try {
               da log = JL.a("structureDestroy");
               log.a("player", entity.aS());
               log.a("target", "base");
               log.a("xp", 30);
               log.a("positionX", this.f_().c); // this.getPos().x
               log.a("positionY", this.f_().d); // this.getPos().y
               this.game.u().c().a(log);
            } catch (Exception var11) {
               a.error(var11, var11);
            }
         }

         this.q = UH.b;
      }

   }

   public boolean F() {
      return this.n;
   }

   public boolean q() {
      return true;
   }

   public void a(JW var1) {
      if (this.r.size() > 0 && ((JW) this.r.get(0)).q()) {
         var1.a(Math.min(((JW) this.r.get(0)).c(), var1.c()));
      }

      this.r.add(var1);
   }

   public List G() {
      return this.r;
   }

   public void b(int var1) {
      this.m = dh.a(var1, 0, 4800);
   }

   public int H() {
      return this.m;
   }

   public int I() {
      return 4800;
   }

   public void e_() {
      this.b().f();
      this.a_(0.0F);
      super.e_();
      --this.q;
      if (this.game.B()) {
         this.a(y, (Mb) null);
      }

   }

   private float a(int var1, Mb var2) {
      float var3 = 0.0F;
      List var4 = this.p.b();

      for (int var5 = 0; var5 < var4.size(); ++var5) {
         Dd var6 = (Dd) var4.get(var5);
         if (var6.b > 48.0F && this.game.Y() - var6.c >= var1) {
            float dmg = var6.b / 48.0F;
            int xp = (int) dmg;
            xp = xp / 1 * 1;
            var6.b -= (float) xp * 48.0F;
            if (!(var6.a instanceof mN)) {
               a.error("invalid non-plane damage source");
            } else {
               mN var9 = (mN) var6.a;
               if (var9 == var2) {
                  var3 = dmg;
               } else {
                  Er var10 = new Er(this.game, var9, this, xp);
                  this.game.u().c().l().a((kY) var10);

                  try {
                     da log = JL.a("structureDamage");
                     log.a("player", var9.aS());
                     log.a("target", (Object) "base");
                     log.a("xp", xp);
                     log.a("positionX", this.f_().c); // this.getPos().x
                     log.a("positionY", this.f_().d); // this.getPos().y
                     log.a("exactXp", (double) dmg);
                     this.game.u().c().a(log);
                  } catch (Exception var12) {
                     a.error(var12, var12);
                  }
               }
            }
         }
      }

      return var3;
   }

   public void b(float var1) {
      int var2 = Math.round(this.f_().c + this.b().c * var1);
      int var3 = Math.round(this.f_().d + this.b().d * var1);
      qd.k();
      qd.b((float) var2, (float) var3);
      this.J();
      qd.l();
   }

   public void a(float var1) {
      qd.k();
      int var2 = Math.round(this.f_().c + this.b().c * var1);
      int var3 = Math.round(this.f_().d + this.b().d * var1);
      qd.b((float) var2, (float) var3);
      this.K();
      qd.l();
      if (dp.a) {
         super.a(var1);
      }

   }

   public void J() {
      qd.b(ip.a);
      qd.k();
      qd.a(this.h.e().b());
      qd.b(37.0F, 49.0F);
      this.k.a();
      qd.l();
   }

   public void K() {
      qd.b(ip.a);
      this.h.e().d();
   }

   public static void a(Rl var0, float var1, ip var2) {
      qd.b(ip.a);
      em var3 = var0.a("map/turret/health_bar_bg.png");
      var3.a();
      int var4 = var3.c() - 2;
      int var5 = var3.d() - 2;
      eK var6 = var0.a("map/turret/health_bar_fg_texture.png").l()[0];
      float var7 = (float) var6.c();
      float var8 = (float) var6.b();
      int var9 = Math.round(var1 * (float) var4);
      byte var10 = 1;
      byte var11 = 1;
      qd.b(var2);
      float var12 = 0.0F;
      float var13 = (float) var9 / var7;
      float var14 = 0.0F;
      float var15 = (float) var5 / var8;
      float var16 = (float) var10;
      float var17 = (float) (var10 + var9);
      float var18 = (float) var11;
      float var19 = (float) (var11 + var5);
      qd.a(7, var6);
      qd.d(var12, var14);
      qd.c(var16, var18);
      qd.d(var13, var14);
      qd.c(var17, var18);
      qd.d(var13, var15);
      qd.c(var17, var19);
      qd.d(var12, var15);
      qd.c(var16, var19);
      qd.m();
   }

   public JB d() {
      return new dd(this, this);
   }

   public void c(fe var1) {
      this.game = var1;
      if (var1.B()) {
         this.p = new On(var1, 10);
      }

      this.a((JZ) (new JZ(1.0F, 0.1F, 0.35F, 0.0F, 0.35F)));
      this.f(1000000.0F);
      if (var1.F()) {
         this.l = new yQ(this);
      }

   }

   public float k() {
      return 0.0F;
   }

   public void a(fe var1) {
   }

   public void b(fe var1) {
      var1.R().b(this);
   }

   public final void a(nu var1) {
      try {
         throw new IOException();
      } catch (IOException e1) {
         e1.printStackTrace();
      }
   }

   public final void a(nQ var1) {
      try {
         throw new IOException();
      } catch (IOException e1) {
         e1.printStackTrace();
      }
   }

   // $FF: synthetic method
   static int a(Eb var0) {
      return var0.m;
   }

   // $FF: synthetic method
   static int b(Eb var0) {
      return var0.q;
   }

   // $FF: synthetic method
   static void a(Eb var0, int var1) {
      var0.m = var1;
   }

   // $FF: synthetic method
   static boolean c(Eb var0) {
      return var0.n;
   }

   // $FF: synthetic method
   static void d(Eb var0) {
      var0.M();
   }

   // $FF: synthetic method
   static oD e(Eb var0) {
      return var0.e;
   }

   // $FF: synthetic method
   static int f(Eb var0) {
      return var0.d;
   }

   // $FF: synthetic method
   static lE g(Eb var0) {
      return var0.f;
   }
}
