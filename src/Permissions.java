import mods.Config;
import org.apache.log4j.Logger;
import org.hjson.JsonObject;
import org.hjson.JsonValue;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;

public class Permissions {
   private static Logger logger = Logger.getLogger(Permissions.class);
   private boolean enabled = false;
   private boolean fallback = false;
   private Map<String, Set<UUID>> groups = new HashMap<>();
   private Map<String, Set<String>> commands = new HashMap<>();

   public boolean isEnabled() {
      return enabled;
   }

   PermissionResult check_command(UUID id, String cmd, YY group) {
      Set<String> groups = commands.get(cmd);
      if (groups == null) {
         if (fallback) {
            return PermissionResult.Missing;
         } else {
            logger.warn("Command '" + cmd + "' not specified in permissions file");
            return PermissionResult.Disabled;
         }
      }

      if (groups.size() == 0) return PermissionResult.Disabled;
      if (groups.contains("all")) return PermissionResult.Allow;
      if (group == YY.d) {
         if (groups.contains("vote")) {
            return PermissionResult.Allow;
         }
         return PermissionResult.NoPermissionVote;
      }

      for (String g : groups) {
         Set<UUID> users = this.groups.get(g);
         if (g.equals("admins") && group == YY.b) return PermissionResult.Allow;
         if (g.equals("vote") && group == YY.b) return PermissionResult.Allow;
         if (users == null) {
            if (!g.equals("vote") && !g.equals("all") && !g.equals("admins")) {
               logger.warn("Group '" + g + "' not defined in permissions file");
            }
            continue;
         }
         for (UUID x : users) {
            if (x.equals(id)) return PermissionResult.Allow;
         }
      }

      return PermissionResult.NoPermission;
   }

   enum PermissionResult {
      Allow,
      NoPermission,
      NoPermissionVote,
      Disabled,
      Missing
   }

   public Permissions(int port) {
      String path;
      try {
         Config.Permissions conf = Config.get(port).permissions;
         if (!conf.enabled) return;

         path = conf.file_name;
         this.enabled = true;
         this.fallback = conf.fallback;
      } catch (Exception e) {
         logger.error("Failed to get permissions configuration from global config: " + e);
         return;
      }

      File f = new File(jt.a, path);
      if (f.exists()) {
         try {
            logger.info("Loading permissions file " + f.getPath());
            byte[] encoded = Files.readAllBytes(f.toPath());
            String data = new String(encoded, Charset.defaultCharset());
            JsonObject root = JsonValue.readHjson(data).asObject();

            for (JsonObject.Member m : root.get("user_groups").asObject()) {
               String name = m.getName();
               Set<UUID> members = new HashSet<>();
               for (JsonValue n : m.getValue().asArray()) {
                  try {
                     String uuid_str = n.asString();
                     UUID uuid = UUID.fromString(uuid_str);
                     members.add(uuid);
                  } catch (Exception e) {
                     logger.warn(
                             "Failed to parse a vapor ID in group " + "'" + name + "': " + e);
                  }
               }
               this.groups.put(name, members);
            }

            for (JsonObject.Member m : root.get("commands").asObject()) {
               String command = m.getName();
               Set<String> groups = new HashSet<>();

               for (JsonValue n : m.getValue().asArray()) {
                  groups.add(n.asString());
               }

               this.commands.put(command, groups);
            }

         } catch (Exception e) {
            logger.error("Error reading permissions file: " + e);
         }
      } else {
         logger.info("Error: Permissions file " + f.getPath() + " doesn't exist");
      }
   }
}
