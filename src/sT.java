import org.apache.log4j.Logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Server config encoding
// - modified write method to optionally omit map & command lists.
public class sT {
   private static final Logger a = Logger.getLogger(sT.class);
   private fe b;
   private String c = "";
   private Float d = new Float(1.0F);
   private int e = 0;
   private int f = 0;
   private float g = 1.0F;
   private float h = 1.0F;
   private float i = 1.0F;
   private List j;
   private List k;
   private List l;
   private lw m;
   private Ks n;
   private G o;
   private ZY p;
   private JN q;
   private boolean r;
   private boolean s;
   private static final vc t;
   private Map u;
   private boolean v;
   private boolean w;
   private int x;
   private int y;
   private float z = 1.0F;
   private float A = 1.0F;
   private boolean B = false;

   static {
      t = new vc(up.a, LC.a);
   }

   public sT(fe var1) {
      this.b = var1;
   }

   public String a() {
      return this.c;
   }

   public void a(String var1) {
      this.c = var1;
   }

   public Float b() {
      return this.d;
   }

   public void a(Float var1) {
      this.d = var1;
      Iterator var3 = this.b.J().u().k().iterator();

      mN var2;
      while (var3.hasNext()) {
         var2 = (mN) var3.next();
         var2.a(var1);
      }

      if (this.b.J().u().g() != null) {
         var3 = this.b.R().d().iterator();

         while (var3.hasNext()) {
            var2 = (mN) var3.next();
            var2.a(var1);
         }
      }

   }

   public void a(int var1) {
      this.e = var1;
      this.b.c(var1);
   }

   public int c() {
      return this.e;
   }

   public void b(int var1) {
      this.f = var1;
   }

   public int d() {
      return this.f;
   }

   public float e() {
      return this.g;
   }

   public void a(float var1) {
      this.g = var1;
   }

   public List f() {
      return this.j;
   }

   public void a(List var1) {
      this.j = var1;
   }

   public List g() {
      return this.k;
   }

   public void b(List var1) {
      this.k = var1;
   }

   public List h() {
      return this.l;
   }

   public void c(List var1) {
      this.l = var1;
   }

   public lw i() {
      return this.m;
   }

   public void a(lw var1) {
      this.m = var1;
   }

   public Ks j() {
      return this.n;
   }

   public void a(Ks var1) {
      this.n = var1;
   }

   public G k() {
      return this.o;
   }

   public void a(G var1) {
      this.o = var1;
   }

   public ZY l() {
      return this.p;
   }

   public void a(ZY var1) {
      this.p = var1;
   }

   public JN m() {
      return this.q;
   }

   public void a(JN var1) {
      this.q = var1;
   }

   public boolean n() {
      return this.r;
   }

   public void a(boolean var1) {
      this.r = var1;
   }

   public boolean o() {
      return this.s;
   }

   public void b(boolean var1) {
      this.s = var1;
   }

   public boolean p() {
      return this.u != null;
   }

   public boolean q() {
      return this.v;
   }

   public Map r() {
      return this.u;
   }

   public void a(Map var1) {
      this.u = var1;
      if (this.u != null) {
         int var2 = 0;
         Iterator var4 = var1.values().iterator();

         while (var4.hasNext()) {
            int var3 = ((Integer) var4.next()).intValue();
            if (var3 == 0) {
               ++var2;
            } else if (var3 == 1) {
               --var2;
            }
         }

         this.v = var2 == 0;
      } else {
         this.v = false;
      }

   }

   public boolean s() {
      return this.w;
   }

   public void c(boolean var1) {
      this.w = var1;
   }

   public int t() {
      return this.x;
   }

   public void c(int var1) {
      this.x = var1;
   }

   public int u() {
      return this.y;
   }

   public void d(int var1) {
      this.y = var1;
   }

   public boolean v() {
      return this.i < 0.5F;
   }

   public float w() {
      return this.h;
   }

   public void b(float var1) {
      this.h = var1;
   }

   public float x() {
      return this.i;
   }

   public void c(float var1) {
      this.i = var1;
   }

   public float y() {
      return this.z;
   }

   public void d(float var1) {
      this.z = var1;
   }

   public float z() {
      return this.A;
   }

   public void e(float var1) {
      this.A = var1;
   }

   public boolean A() {
      return this.B;
   }

   public void d(boolean var1) {
      this.B = var1;
      if (var1) {
         Iterator var3 = this.b.J().u().k().iterator();

         while (var3.hasNext()) {
            mN var2 = (mN) var3.next();
            if (var2.aO()) {
               var2.aL();
            }
         }
      }

   }

   public static byte[] a(VS var0, boolean minimal) throws IOException {
      RX var1 = new RX();
      DataOutputStream var2 = new DataOutputStream(var1);
      hp var3 = var0.d().m();
      var2.writeUTF(var3.y());
      var2.writeFloat(var3.K());
      var2.writeFloat(var3.L());
      var2.writeBoolean(var3.M());
      nu var4 = new nu(var2);
      var4.a(40, 300, var3.B());
      var4.a(40, 300, var3.N());
      var4.a(0, 3, var3.O());
      var4.a(0, 3, var3.P());
      var4.a(0, 1000, var3.D());
      var4.a(0, 1000, var3.E());

      if (minimal) {
         Dr.b.a(new ArrayList(), var4);
         Dr.b.a(new ArrayList(), var4);
         Dr.b.a(new ArrayList(), var4);
      } else {
         Dr.b.a(var3.k(), var4);
         Dr.b.a(var3.t(), var4);
         Dr.b.a(var3.v(), var4);
      }

      (new Km(lw.a)).a(var3.F(), var4);
      (new Km(Ks.a)).a(var3.G(), var4);
      (new Km(G.a)).a(var3.H(), var4);
      (new Km(ZY.a)).a(var3.I(), var4);
      (new Km(JN.a)).a(var3.J(), var4);
      var4.a(var3.f());
      var4.a(var3.g());
      var4.a(var3.Q().d());
      var4.a(0, 63, var3.Q().e());
      var4.a(0, 1000, var3.m());
      var4.a(var0.r());
      if (var0.r()) {
         t.a(var0.s(), var4);
      }

      var4.d();
      return var1.b();
   }

   public void a(byte[] var1) throws IOException {
      DataInputStream var2 = new DataInputStream(new y(var1));
      this.a(var2.readUTF());
      this.d(var2.readFloat());
      this.e(var2.readFloat());
      this.d(var2.readBoolean());
      nQ var3 = new nQ(var2);
      this.a((float) var3.a(40, 300) / 100.0F);
      this.a(Float.valueOf((float) var3.a(40, 300) / 100.0F));
      this.a(var3.a(0, 3));
      this.b(var3.a(0, 3));
      this.c((float) var3.a(0, 1000) / 100.0F);
      this.b((float) var3.a(0, 1000) / 100.0F);
      this.a(Dr.b.a(var3));
      this.b(Dr.b.a(var3));
      this.c(Dr.b.a(var3));
      this.a((lw) (new Km(lw.a)).b(var3));
      this.a((Ks) (new Km(Ks.a)).b(var3));
      this.a((G) (new Km(G.a)).b(var3));
      this.a((ZY) (new Km(ZY.a)).b(var3));
      this.a((JN) (new Km(JN.a)).b(var3));
      this.a(var3.d());
      this.b(var3.d());
      this.c(var3.d());
      this.c(var3.a(0, 63));
      this.d(var3.a(0, 1000));
      boolean var4 = var3.d();
      if (var4) {
         this.a(t.a(var3));
      } else {
         this.a((Map) null);
      }

      var3.b();
   }
}
