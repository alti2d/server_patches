import org.apache.log4j.Logger;

public class fJ extends kY {
   private static final Logger a = Logger.getLogger(fJ.class);
   private kK b;
   private int c;

   public fJ(fe var1) {
      super(var1);
   }

   public fJ(fe var1, yJ var2, int var3) {
      this(var1);
      this.b = this.e().a((nq)var2);
      this.c = var3;
   }

   private bE i() {
      try {
         yJ var1 = (yJ)this.b.b(this.e());
         return var1.W()[this.c];
      } catch (RuntimeException var3) {
         a.info("carrierId=" + this.b);
         a.info("carrierId.getNetworkObject(getGame())=" + this.b.b(this.e()));
         nq var2 = this.b.b(this.e());
         a.info("Plane.class.isAssignableFrom(" + var2.getClass() + ")=" + mN.class.isAssignableFrom(var2.getClass()));
         a.info("abilityId=" + this.c);
         throw var3;
      }
   }

   public void a() {
      this.i().f();
      if (this.e().B() && this.c == 3) {
         int player = -1;
         float x = 0.0F;
         float y = 0.0F;
         float velX = 0.0F;
         float velY = 0.0F;
         float angle = 0.0F;
         String powerup = "unknown";

         try {
            mN var7 = (mN)this.b.b(this.e());
            player = var7.aS();
            x = var7.f_().c;
            y = var7.f_().d;
            velX = var7.b().c;
            velY = var7.b().d;
            angle = var7.m();
            powerup = LX.a(var7.ab().getClass()).h;
         } catch (Exception var9) {
            a.error(var9, var9);
         }

         try {
            da var10 = JL.a("powerupUse");
            var10.a("player", player);
            var10.a("powerup", (Object)powerup);
            JL.a(var10, "positionX", x);
            JL.a(var10, "positionY", y);
            JL.a(var10, "velocityX", velX);
            JL.a(var10, "velocityY", velY);
            JL.a(var10, "playerAngle", angle);
            this.e().u().c().a(var10);
         } catch (Exception var8) {
            a.error(var8, var8);
         }
      }

   }

   public void a(nQ var1) {
      try {
         this.b = this.e().a(var1, mN.class);
         this.c = var1.b(2);
         this.i().a(var1);
      } catch (RuntimeException var3) {
         a.error("Runtime Exception reading AAE: " + this.b + ", " + this.c);
         a.error(" -> " + this.b.b(this.e()));
         throw var3;
      }
   }

   public void a(nu var1) {
      this.e().a(var1, this.b);
      var1.b(2, this.c);
      this.i().a(var1);
   }

   public String toString() {
      return super.toString() + "(" + this.b + ", " + this.c + ") -> " + this.i().toString();
   }
}
