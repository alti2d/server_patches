import org.apache.log4j.Logger;

import java.util.*;
import java.util.Map.Entry;

// BallGameMode
// Modified to add methods for despawning the ball.
public class QK extends cV {
   private static final Logger a = Logger.getLogger(QK.class);
   private ZY config;
   private List teams;
   private oD leftTeam;
   private oD rightTeam;
   private Map scores = new HashMap();
   private oD startingTeam;
   private Map goals;
   private Map ballSpawnPoints;
   private Map ballPossession;
   private oD l;
   private nB m;
   private DV[] n;
   private n o;
   private eq p;
   private static final float missingBalltimeA = sx.d(1.5F).a();
   private static final float missingBallTimeB = sx.d(9.0F).a();
   private float missingBallTimeout;
   private int missingBallCounter;
   private Jg ball;
   private int roundTimeCounter;
   private oD prevScoreTeam;
   private int prevScoreer;
   private int prevAssisterA;
   private int prevAssisterB;
   // $FF: synthetic field
   private static int[] A;

   public QK(DK levelMgr, ZY config) {
      super(levelMgr, config);
      this.startingTeam = oD.c;
      this.n = DV.values();
      this.missingBallTimeout = missingBalltimeA;
      this.missingBallCounter = 0;
      this.roundTimeCounter = 0;
      this.prevScoreer = -1;
      this.prevAssisterA = -1;
      this.prevAssisterB = -1;
      this.config = config;
      List spawn_points = levelMgr.h().f().h();
      LK leftSpawn = null;
      HashSet spawnMap = new HashSet();

      LK point;
      Iterator it;
      for (it = spawn_points.iterator(); it.hasNext(); spawnMap.add(point.aU())) {
         point = (LK) it.next();
         if (leftSpawn == null || point.f_().c < leftSpawn.f_().c) {
            leftSpawn = point;
         }
      }

      this.leftTeam = leftSpawn.aU();
      spawnMap.remove(this.leftTeam);
      this.rightTeam = (oD) spawnMap.iterator().next();
      this.teams = nT.a((Object[]) (new oD[]{this.leftTeam, this.rightTeam}));
      this.ballPossession = new HashMap();
      it = nT.a((List) this.teams, (Object[]) (new oD[]{oD.c})).iterator();

      while (it.hasNext()) {
         oD team = (oD) it.next();
         this.scores.put(team, new Integer(0));
         this.ballPossession.put(team, new AF(0));
      }

      this.goals = new HashMap();
      it = levelMgr.h().f().m().iterator();

      while (it.hasNext()) {
         GF goal = (GF) it.next();
         if (this.teams.contains(goal.aU())) {
            this.goals.put(goal.aU(), goal);
         }
      }

      this.ballSpawnPoints = new HashMap();
      it = levelMgr.h().f().i().iterator();

      while (it.hasNext()) {
         HM spawner = (HM) it.next();
         if (spawner.m().contains(LX.g)) {
            this.ballSpawnPoints.put(spawner.aU(), spawner);
            spawner.a(true);
         }
      }

   }

   public String c() {
      return "Plane Ball";
   }

   public String a(oD var1) {
      return "Shoot the ball into the opposing team\'s goal";
   }

   public List i() {
      return this.teams;
   }

   public List l() {
      return Sr.q;
   }

   public void setBall(Jg ball) {
      this.missingBallCounter = 0;
      this.ball = ball;
      if (this.m != null) {
         this.m.f();
      }
   }

   public void a(Jg ball) {
      this.setBall(ball);
   }

   public void a() {
      super.a();
      this.scores.put(this.leftTeam, Integer.valueOf(0));
      this.scores.put(this.rightTeam, Integer.valueOf(0));
      ((AF) this.ballPossession.get(this.leftTeam)).a = 0;
      ((AF) this.ballPossession.get(this.rightTeam)).a = 0;
   }

   public void b() {
      if (!this.L() && !this.w()) {
         if (this.t().B() && this.config.a() > 0) {
            if (this.roundTimeCounter > 0) {
               --this.roundTimeCounter;
               if (this.roundTimeCounter == 0) {
                  // this.endRound();
                  this.G();
               }
            } else {
               int left = (Integer) this.scores.get(this.leftTeam);
               int right = (Integer) this.scores.get(this.rightTeam);
               if (Math.max(left, right) > this.config.a() / 2 || left + right >= this.config.a()) {
                  this.roundTimeCounter = Math.max(1, (int) (this.I() - 10.0F));
               }
            }
         }

         if (this.ball != null && this.ball.J() != null) {
            mN carrier = this.ball.J();
            if (carrier.af() && carrier.ab() instanceof Jg) {
               GF goal = (GF) this.goals.get(this.b(carrier.aU()));
               if (goal != null) {
                  et var3 = goal.z();
                  float var4 = carrier.f_().c - var3.f_().c;
                  float var5 = carrier.f_().d - var3.f_().d;
                  float var6 = carrier.b().c;
                  float var7 = carrier.b().d;
                  float var8 = carrier.aA() / 2.0F;
                  float var9;
                  if (var6 * var6 + var7 * var7 > var8 * var8) {
                     var9 = var8 / dh.f(var6 * var6 + var7 * var7);
                     var6 *= var9;
                     var7 *= var9;
                  }

                  var9 = var4 + var6;
                  float var10 = var5 + var7;
                  if (zv.a(var3.i(), var4, var5) || zv.a(var3.i(), var9, var10)) {
                     this.t().ab().b(new ZW(this.t(), carrier, goal.aU()));
                     carrier.ae();
                  }
               }
            }
         }

         if (this.m == null) {
            this.m = new nB(this, this.t());
            this.t().R().b(this.m);
         }

         boolean ball_alive = false;
         if (this.ball != null) {

            if (this.ball.q()) {
               ball_alive = true;
            } else {
               mN carrier = this.ball.J();
               if (carrier != null && carrier.aO() && carrier.ab() == this.ball) {
                  ball_alive = true;
               }
            }
         }

         if (ball_alive) {
            this.missingBallCounter = 0;
            oD team = this.ball.m();
            mN plane = this.ball.J();
            boolean isCarrier = plane != null && plane.aO() && plane.ab() == this.ball;
            if (this.teams.contains(team) && (this.ball.t() || isCarrier)) {
               ++((AF) this.ballPossession.get(team)).a;
            }
         } else {
            ++this.missingBallCounter;

            // Check the ball's fade timer, if it's expired, the ball has been lost.
            if (this.ball != null && this.ball.M().e() && this.ball.J() == null && this.t().B()) {
               this.looseBall();
            }

            // If the ball can't be found, but the time hasn't expired, it's probably
            // because a player has disconnected while holding it -- so we respawn it
            // without resetting the players.;w

            this.ball = null;
            if (this.t().B() && (float) this.missingBallCounter >= this.missingBallTimeout) {
               // Gets the ball spawn point for the last team who started.
               Yr newSpawn = ((HM) this.ballSpawnPoints.get(this.startingTeam)).f_();
               this.newBall(newSpawn);
            }
         }

         this.m();
      }
   }

   public Yr ballSpawnPos(oD team) {
      return ((HM) this.ballSpawnPoints.get(team)).f_();
   }

   public void looseBall() {
      this.prevScoreTeam = null;
      this.prevScoreer = -1;
      this.prevAssisterA = -1;
      this.prevAssisterB = -1;
      this.a(DV.a);
   }

   public void newBall(Yr position) {
      this.ball = null;

      this.missingBallCounter = 0;
      this.ball = new Jg(this.t());
      this.ball.f_().a(position);
      this.ball.c(true);
      oa var17 = new oa(this.t(), this.ball);
      VS var18 = this.t().u().c().l();
      var18.a((kY) var17);
      this.missingBallTimeout = missingBallTimeB;
   }

   private void m() {
      // if(this.getGame().isPlayer() {
      if (this.t().F()) {
         boolean var2 = this.ball != null;
         mN var3 = var2 ? this.ball.J() : null;
         Object var1;
         if (var3 != null) {
            var1 = var3;
         } else {
            var1 = this.ball;
         }

         if (var1 != this.o) {
            if (this.p != null) {
               this.p.c();
               this.p = null;
            }

            this.o = (n) var1;
            if (this.o instanceof Jg) {
               this.p = new eq(this.t(), (Jg) this.o);
               this.t().R().b(this.p);
            }
         }
      }

   }

   public void a(oD var1, int var2, int var3, int var4, oD var5) {
      if (var1 != null && this.teams.contains(var1) && !ty.a((Object) var1, (Object) var5)) {
         this.prevScoreTeam = var1;
         this.prevScoreer = var2;
         this.prevAssisterA = var3;
         this.prevAssisterB = var4;
         this.a(DV.a);
      }

   }

   private void a(DV var1) {
      VS var2 = this.t().u().c().l();
      cG var3 = new cG(this.t(), var2, (byte) var1.ordinal());
      var2.a((kY) var3);
   }

   public void a(byte var1, nu var2) {
      DV var3 = this.n[var1];
      switch (k()[var3.ordinal()]) {
         case 1:
            boolean var4 = this.prevScoreTeam != null;
            var2.a(var4);
            if (var4) {
               oD.p.a(this.prevScoreTeam, var2);
               Vh.a(var2, this.prevScoreer);
               Vh.a(var2, this.prevAssisterA);
               Vh.a(var2, this.prevAssisterB);
            }
         default:
      }
   }

   public void a(byte var1, nQ var2) {
      DV var3 = this.n[var1];
      switch (k()[var3.ordinal()]) {
         case 1:
            boolean var4 = var2.d();
            if (var4) {
               this.prevScoreTeam = (oD) oD.p.b(var2);
               this.prevScoreer = Vh.b(var2);
               this.prevAssisterA = Vh.b(var2);
               this.prevAssisterB = Vh.b(var2);
               Integer var5 = (Integer) this.scores.get(this.prevScoreTeam);
               Integer var6 = Integer.valueOf(var5.intValue() + 1);
               this.scores.put(this.prevScoreTeam, var6);
               mF var7 = this.t().J().u().c(this.prevScoreer);
               var7.g().l.a(1);
               mF var8 = null;
               mF var9 = null;
               if (this.prevAssisterA != -1) {
                  var8 = this.t().J().u().c(this.prevAssisterA);
                  var8.g().m.a(1);
               }

               if (this.prevAssisterB != -1) {
                  var9 = this.t().J().u().c(this.prevAssisterB);
                  var9.g().m.a(1);
               }

               if (this.t().F()) {
                  this.t().J().u().C().a(this.prevScoreer, aI.c, 50);
                  if (var7.i() != null && this.t().b(this.prevScoreer)) {
                     bC.a(this.t(), "Goal +50", var7.i().f_());
                  }

                  if (var8 != null) {
                     this.t().J().u().C().a(this.prevAssisterA, aI.c, 30);
                     if (var8.i() != null && this.t().b(this.prevAssisterA)) {
                        bC.a(this.t(), "Assist +30", var8.i().f_());
                     }
                  }

                  if (var9 != null) {
                     this.t().J().u().C().a(this.prevAssisterB, aI.c, 30);
                     if (var9.i() != null && this.t().b(this.prevAssisterB)) {
                        bC.a(this.t(), "Assist +30", var9.i().f_());
                     }
                  }

                  kr var10 = this.t().G().b("map/mode/goal_scored.wav");
                  DA var11 = this.t().ah().a(var10);
                  this.t().J().u().N().a("Scored Goal!", var11, var7, var7.i(), true, var8, var9);
               }

               if (this.t().B()) {
                  try {
                     da var22 = JL.a("goal");
                     var22.a("player", var7.h());
                     var22.a("xp", 50);
                     var22.a("assister", this.prevAssisterA);
                     var22.a("secondaryAssister", this.prevAssisterB);
                     this.t().u().c().a(var22);
                  } catch (Exception var13) {
                     a.error(var13, var13);
                  }
               }

               this.startingTeam = this.b(this.prevScoreTeam);
            } else {
               if (this.t().F()) {
                  String var14 = "Ball Lost";
                  this.a(ip.h, var14, 5.0F);
                  kr var17 = this.t().G().b("planes/bomber/lock_acquired.wav");
                  DA var20 = this.t().ah().a(var17);
                  var20.l();
               }

               if (this.t().B()) {
                  try {
                     da var15 = JL.a("ballLost");
                     this.t().u().c().a(var15);
                  } catch (Exception var12) {
                     a.error(var12, var12);
                  }
               }

               this.startingTeam = oD.c;
            }

            this.missingBallTimeout = missingBalltimeA;
            if (this.ball != null) {
               this.ball.a();
            }

            ArrayList var16 = new ArrayList(this.t().R().aB());
            Iterator var21 = var16.iterator();

            while (var21.hasNext()) {
               Yl var18 = (Yl) var21.next();
               if (var18.q() && var18 instanceof Jg) {
                  ((Jg) var18).a();
               }
            }

            var21 = this.t().J().u().k().iterator();

            while (var21.hasNext()) {
               mN var19 = (mN) var21.next();
               var19.aI();
               if (var19.aO()) {
                  var19.aL();
                  var19.aT().a(var19);
               }
            }

            this.t().J().u().b(this.t().T().I());
            this.t().T().a((int) this.t().T().I());
         default:
      }
   }

   private oD b(oD var1) {
      return var1 == oD.c ? oD.c : (ty.a((Object) var1, (Object) this.leftTeam) ? this.rightTeam : this.leftTeam);
   }

   public void a(int var1, int var2) {
      this.scores.put((oD) this.teams.get(0), Integer.valueOf(var1));
      this.scores.put((oD) this.teams.get(1), Integer.valueOf(var2));
   }

   public void a(nQ var1) {
      super.a(var1);
      Iterator var3 = this.teams.iterator();

      while (var3.hasNext()) {
         oD var2 = (oD) var3.next();
         this.scores.put(var2, Integer.valueOf(var1.a(0, 255)));
      }

   }

   public void a(nu var1) {
      super.a(var1);
      Iterator var3 = this.teams.iterator();

      while (var3.hasNext()) {
         oD var2 = (oD) var3.next();
         var1.a(0, 255, ((Integer) this.scores.get(var2)).intValue());
      }

   }

   public void a(gk var1) {
      this.l = oD.c;
      int var2 = ((Integer) this.scores.get(this.leftTeam)).intValue();
      int var3 = ((Integer) this.scores.get(this.rightTeam)).intValue();
      if (var2 != var3) {
         this.l = var2 > var3 ? this.leftTeam : this.rightTeam;
      }

      oD.p.a(this.l, var1);
      this.c(this.l);
      Iterator var5 = this.teams.iterator();

      while (var5.hasNext()) {
         oD var4 = (oD) var5.next();
         float var6 = Math.min(3600.0F, sx.e((float) ((AF) this.ballPossession.get(var4)).a));
         var1.a(0.0F, 3600.0F, 1.0F, var6);
      }

      super.a(var1);
   }

   public void a(LG var1) {
      this.l = (oD) oD.p.b(var1);
      if (this.t().F()) {
         this.t().J().u().O().a();
      }

      oD var2;
      float var4;
      for (Iterator var3 = this.teams.iterator(); var3.hasNext(); ((AF) this.ballPossession.get(var2)).a = Math.round(sx.b(var4))) {
         var2 = (oD) var3.next();
         var4 = var1.a(0.0F, 3600.0F, 1.0F);
      }

      super.a(var1);
   }

   public void b(float var1) {
      super.b(var1);
      NY var2 = this.t().G().a(KN.r);
      qd.b(this.leftTeam.f());
      short var3 = 715;
      if (this.L()) {
         qd.a(0.8F, 0.8F, 0.8F, 1.0F);
         var2.a("Warmup ends in " + (int) Math.ceil((double) sx.e((float) this.M())), 640, var3, 17);
      } else {
         int var4 = 640;
         if (this.x()) {
            var4 -= 50;
            short var5 = 675;
            float var6 = 1000.0F * this.e(var1);
            float var7 = 1000.0F * this.y();
            String var8 = me.a((long) var6, (long) var7);
            qd.a(0.8F, 0.8F, 0.8F, 1.0F);
            var2.a(var8, var5, var3, 20);
         }

         int var9 = ((Integer) this.scores.get(this.leftTeam)).intValue();
         int var10 = ((Integer) this.scores.get(this.rightTeam)).intValue();
         qd.a(0.8F, 0.8F, 0.8F, 1.0F);
         qd.b(this.leftTeam.f());
         var2.a(Qk.a(var9), var4 - 10, var3, 24);
         qd.b(this.rightTeam.f());
         var2.a(Qk.a(var10), var4 + 10, var3, 20);
      }

   }

   public void a(Hc var1) {
      Iterator var3 = this.scores.entrySet().iterator();

      while (var3.hasNext()) {
         Entry var2 = (Entry) var3.next();
         var1.a((oD) var2.getKey(), ((Integer) var2.getValue()).intValue());
      }

   }

   public yr j() {
      Eb var1 = null;
      if (this.l != oD.c) {
         var1 = new Eb(this.t());
         var1.a(this.l);
         lE var2 = new lE();
         var2.d = this.l == this.rightTeam;
         var1.a(var2);
         var1.c(this.t());
      }

      return new BallScore(this.t(), this.l, this.leftTeam, this.rightTeam, this.scores, this.ballPossession);
   }

   public String a(mF var1) {
      xs var2 = var1.g();
      return var2.l.a() + " goals, " + var2.k.a() + " XP";
   }

   public Comparator o() {
      return new aaK(this);
   }

   public n q() {
      return this.o;
   }

   public boolean p() {
      return true;
   }

   public String r() {
      return "Ball Camera";
   }

   // $FF: synthetic method
   static Jg a(QK var0) {
      return var0.ball;
   }

   // $FF: synthetic method
   static List b(QK var0) {
      return var0.teams;
   }

   // $FF: synthetic method
   static oD a(QK var0, oD var1) {
      return var0.b(var1);
   }

   // $FF: synthetic method
   static Map c(QK var0) {
      return var0.goals;
   }

   // $FF: synthetic method
   static int[] k() {
      int[] var10000 = A;
      if (A != null) {
         return var10000;
      } else {
         int[] var0 = new int[DV.values().length];

         try {
            var0[DV.a.ordinal()] = 1;
         } catch (NoSuchFieldError var1) {
            ;
         }

         A = var0;
         return var0;
      }
   }
}
