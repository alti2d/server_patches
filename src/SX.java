public abstract class SX implements OQ {
   private xs xs;
   String name;
   Number value;
   Number this_life;
   boolean changed;

   SX(xs xs, String var2) {
      this.xs = xs;
      this.name = var2;
      xs.s.add(this);
   }

   public Number getThisLife() {
      return this.this_life;
   }

   public Number a() {
      return this.value;
   }

   public void setThisLife(Number value) {
      if (!ty.a(this.this_life, value)) {
         this.changed = true;
      }
      this.this_life = value;
   }

   public void a(Number value) {
      if (!ty.a(this.value, value)) {
         this.changed = true;
      }
      this.value = value;
   }

   public String toString() {
      return this.value.toString();
   }

   public String b() {
      return this.name;
   }
}
