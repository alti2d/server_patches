import org.apache.log4j.Logger;

import java.util.List;

// Turret
public class Ww extends dC implements D, Sp, ou, yJ {
   private static final Logger a = Logger.getLogger(Ww.class);
   private fe game;
   private Rl resourceMgr;
   private int layer;
   private oD team;
   private lE transformation;
   private float turnLeft;
   private float turnRight;
   private float maxDistance;
   private int j;
   private Jh k; // TexturedPoly
   private Jh l; // TexturedPoly
   private Yr m; // Vector
   private Jh n; // TexturedPoly
   private int o;
   private int health;
   private int q;
   private int r;
   private int s;
   private float cooldownSec;
   private float bulletSpeed;
   private int bulletDir;
   private int blastDmg;
   private int blastRadius;
   private int y;
   private float z;
   private boolean A;
   private float B;
   private float C;
   private float D;
   private boolean E;
   private oU F; // Delay
   private bE[] G; // Action[]
   private On H;
   private int I;
   private int J;
   private Ax K;
   private Mb L; // TeamEntity
   private int M;
   private PG N;
   private static final int O = Math.round(sx.d(0.3F).a());
   private Yr P; // Vector
   private Up Q;

   public Ww(fe var1) {
      this(var1.G());
   }

   public Ww(Rl var1) {
      super(Yr.a(new float[]{-50.0F, -15.0F, 50.0F, -15.0F, -50.0F, 25.0F}), true);
      this.layer = 0;
      this.turnLeft = 45.0F;
      this.turnRight = 45.0F;
      this.maxDistance = 600.0F;
      this.j = this.k(this.maxDistance);
      this.health = 600;
      this.cooldownSec = 0.6F;
      this.bulletSpeed = 17.0F;
      this.bulletDir = 10;
      this.blastDmg = 30;
      this.blastRadius = 80;
      this.F = new oU(sx.d(this.cooldownSec));
      this.G = new bE[]{new Shoot(this)};
      this.P = new Yr();
      this.Q = new Up();
      this.resourceMgr = var1;
      this.transformation = new lE();
      this.a((oD) oD.m.get(0));
      this.J();
   }

   public oD aU() {
      return this.team;
   }

   public int aS() {
      return -1;
   }

   private static Jh a(Rl var0, oD var1) {
      String var2 = "render/map/turret/" + var1.d() + "_base.poly";
      return var0.e(var2);
   }

   public void a(oD var1) {
      if (!oD.m.contains(var1)) {
         var1 = oD.d;
      }

      this.team = var1;
      this.k = a(this.resourceMgr, var1);
      this.a(this.H());
   }

   public int c() {
      return this.layer;
   }

   public void a(int var1) {
      this.layer = var1;
      this.L();
   }

   public float y() {
      return this.turnLeft;
   }

   public float z() {
      return this.bulletSpeed;
   }

   public void b(float var1) {
      this.bulletSpeed = var1;
      this.j = this.k(this.maxDistance);
   }

   public int A() {
      return this.bulletDir;
   }

   public void b(int var1) {
      this.bulletDir = var1;
   }

   public int B() {
      return this.blastDmg;
   }

   public void c(int var1) {
      this.blastDmg = var1;
   }

   public int C() {
      return this.blastRadius;
   }

   public void d(int var1) {
      this.blastRadius = var1;
   }

   public int D() {
      return this.health;
   }

   public void e(int var1) {
      if (var1 <= 0) {
         var1 = 1;
      }

      this.health = var1;
      this.y = var1;
   }

   public float E() {
      return this.cooldownSec;
   }

   public void c(float var1) {
      this.cooldownSec = var1;
      this.F.b(sx.d(var1).a());
   }

   public void h(float var1) {
      this.turnLeft = var1;
   }

   public float F() {
      return this.turnRight;
   }

   public void i(float var1) {
      this.turnRight = var1;
   }

   public float G() {
      return this.maxDistance;
   }

   private int k(float var1) {
      return (int) Math.ceil((double) (var1 / this.bulletSpeed));
   }

   public void j(float var1) {
      this.maxDistance = var1;
      this.j = this.k(var1);
   }

   public lE H() {
      return this.transformation;
   }

   public void a(lE var1) {
      this.transformation = new lE(var1);
      this.l = Ht.a(this.k, var1);
      String var2 = "render/map/turret/" + this.team.d() + "_gun.poly";
      Jh var3 = this.resourceMgr.e(var2);
      this.m = Ht.a(new Yr(0.0F, 5.0F), var1);
      this.B = dh.a(this.m);
      this.C = this.B;
      this.n = Ht.a(var3, var1);
      this.a((Yr[]) this.l.a());
      this.r = 0;
      this.s = 0;
      Yr[] var7;
      int var6 = (var7 = this.i()).length;

      for (int var5 = 0; var5 < var6; ++var5) {
         Yr var4 = var7[var5];
         int var8 = Math.round(var4.d);
         this.r = Math.min(this.r, var8);
         this.s = Math.max(this.s, var8);
      }

   }

   public void f(int var1) {
      this.J = Math.max(this.J, var1);
      if (this.game.F()) {
         if (this.K != null && this.K.q()) {
            this.K.c().c((float) this.J);
         } else {
            this.K = new Ax(this.game, this, new qt((float) this.J));
            this.game.R().b(this.K);
         }
      }

   }

   public void a(int var1, Mb var2) {
      this.L = var2;
      this.M = Math.max(this.M, var1);
      if (this.game.F()) {
         if (this.N != null && this.N.q()) {
            this.N.c().c((float) this.M);
         } else {
            this.N = new PG(this.game, this, new qt((float) this.M));
            this.game.R().b(this.N);
         }
      }

   }

   public float I() {
      return this.B;
   }

   private void L() {
      this.o = DX.e(this.c(), this.l.e().c());
   }

   public int u() {
      return this.o;
   }

   public int o() {
      return this.q;
   }

   public int p() {
      return 0;
   }

   public void J() {
      this.y = this.health;
      this.z = 0.0F;
      this.A = true;
      this.q = qf.c | qf.f;
      this.M = 0;
      this.J = 0;
   }

   private void M() {
      this.y = 0;
      this.A = false;
      this.q = 0;
      if (this.K != null) {
         this.K.c().j();
      }

      if (this.N != null) {
         this.N.c().j();
      }

      this.M = 0;
      this.L = null;
      if (this.H != null) {
         this.H.a();
      }

   }

   public void a(Mb var1) {
      this.M();
      this.b(var1);
      if (var1 instanceof mN && this.game.a(((mN) var1).aT())) {
         this.game.y().a(Lu.H).a(1);
      }

   }

   private void b(Mb var1) {
      vU.a(this.game, var1, this.f_(), 95, 90, 1.0F, 1.0F, true, -1, true);
      kr var2 = this.game.G().b("map/turret/turret_explode.wav");
      DA var3 = this.game.ah().a(var2, this.f_(), dc.a);
      var3.l();
   }

   public void a(NG var1, vX var2) {
      if (this.A && this.f().a(var1)) {
         var2.a(this, this.u());
      }

   }

   // hit()
   public void a(Mb entity, float dmg, float angle) {
      if (this.A && entity.aU() != this.aU()) {
         mN plane = entity instanceof mN ? (mN) entity : null;
         if (this.game.S() && plane != null && !plane.aT().a()) {
            rT difficulty = Ja.R().m().Q().c(); // Bot difficulty
            if (difficulty.equals(rT.a)) { // Easy
               dmg *= 4.0F;
            } else if (difficulty.equals(rT.b)) { // Normal
               dmg *= 2.0F;
            }
         }

         if (plane != null) {
            dmg *= plane.aH();
         }

         if (this.M > 0) {
            dmg *= 1.33F;
         }

         float var14 = dh.a(dmg, (float) (this.y - this.health), (float) this.y);
         this.z += var14;
         int var6 = (int) this.z;
         this.z -= (float) var6;
         this.y = dh.a(this.y - var6, 0, this.health);
         if (plane != null) {
            plane.aT().g().g.a(dmg);
            if (this.game.B()) {
               this.H.a(plane, var14);
            }
         }

         if (this.game.B() && this.y == 0) {
            float damage = this.b(0, entity);
            int xp = (int) damage;

            // Create and send events
            Ft ev_a = new Ft(this.game, this, entity);
            this.game.u().c().l().a(ev_a);
            if (entity instanceof mN) {
               Er ev_b = new Er(this.game, (mN) entity, this, 10 + xp);
               this.game.u().c().l().a(ev_b);
            }

            if (damage > 0.0F) {
               try {
                  da log = JL.a("structureDamage");
                  log.a("player", entity.aS());
                  log.a("target", "turret");
                  log.a("xp", xp);
                  log.a("positionX", this.f_().c); // this.getPos().x
                  log.a("positionY", this.f_().d); // this.getPos().y
                  log.a("exactXp", (double) damage);
                  this.game.u().c().a(log);
               } catch (Exception var13) {
                  a.error(var13, var13);
               }
            }
            try {
               da log = JL.a("structureDestroy");
               log.a("player", entity.aS());
               log.a("target", "turret");
               log.a("xp", 10);
               log.a("positionX", this.f_().c); // this.getPos().x
               log.a("positionY", this.f_().d); // this.getPos().y
               this.game.u().c().a(log);
            } catch (Exception var12) {
               a.error(var12, var12);
            }
         }

         this.I = UH.b;
      }

   }

   private float b(int var1, Mb entity) {
      float var3 = 0.0F;
      List var4 = this.H.b();

      for (int var5 = 0; var5 < var4.size(); ++var5) {
         Dd x = (Dd) var4.get(var5);
         if (x.b > 48.0F && this.game.Y() - x.c >= var1) {
            float dmg = x.b / 48.0F;
            int xp = (int) dmg;
            xp = xp / 1 * 1;
            x.b -= (float) xp * 48.0F;
            if (!(x.a instanceof mN)) {
               a.error("invalid non-plane damage source");
            } else {
               mN var9 = (mN) x.a;
               if (var9 == entity) {
                  var3 = dmg;
               } else {
                  Er var10 = new Er(this.game, var9, this, xp);
                  this.game.u().c().l().a((kY) var10);

                  try {
                     da log = JL.a("structureDamage");
                     log.a("player", var9.aS());
                     log.a("target", (Object) "turret");
                     log.a("xp", xp);
                     log.a("positionX", this.f_().c); // this.getPos().x
                     log.a("positionY", this.f_().d); // this.getPos().y
                     log.a("exactXp", (double) dmg);
                     this.game.u().c().a(log);
                  } catch (Exception var12) {
                     a.error(var12, var12);
                  }
               }
            }
         }
      }

      return var3;
   }

   public boolean K() {
      return this.A;
   }

   public boolean aN() {
      return this.K();
   }

   public boolean q() {
      return true;
   }

   private void a(Yr var1, float var2) {
      Yr var3 = this.f_();
      var1.a(var3.c + dh.b(var2) * 30.0F, var3.d + dh.c(var2) * 30.0F);
   }

   private void N() {
      this.E = false;
      this.D = this.maxDistance;
      float var1 = (this.maxDistance + 200.0F) * (this.maxDistance + 200.0F);
      float var2 = 0.0F;
      List var3 = this.game.J().u().k();
      cV var4 = this.game.T();

      float var7;
      for (int var5 = 0; var5 < var3.size(); ++var5) {
         mN var6 = (mN) var3.get(var5);
         if (var4.a((Mb) this, (Sp) var6)) {
            var7 = this.f_().h(var6.f_());
            if (var7 < var1) {
               this.a(this.P, this.C);
               zv.a(var6.f_(), var6.b(), this.P, this.bulletSpeed, this.Q);
               if (!Float.isNaN(this.Q.a) && this.Q.b <= (float) this.j) {
                  float var8 = dh.b(this.Q.a - this.B, -180.0F, 180.0F);
                  if (-this.turnRight <= var8 && var8 <= this.turnLeft) {
                     var2 = var8;
                     var1 = var7;
                     this.E = true;
                     this.D = dh.a(this.Q.b * this.bulletSpeed, 0.0F, this.maxDistance);
                  }
               }
            }
         }
      }

      float var9 = 0.0F;
      float var10 = this.B + var2 + var9;
      var7 = dh.b(var10 - this.C, -180.0F, 180.0F);
      if (Math.abs(var7) > 3.2F) {
         this.E = false;
         var7 = dh.a(var7, -3.2F, 3.2F);
      }

      this.C += var7;
   }

   public float aA() {
      return this.f().E();
   }

   public bE[] W() {
      return this.G;
   }

   public void e_() {
      this.b().f();
      this.a_(0.0F);
      super.e_();
      --this.I;
      if (this.M > 0) {
         --this.M;
         if (this.K()) {
            float var1 = 67.0F / (float) uV.y;
            var1 /= 1.33F;
            this.a(this.L, var1, this.m());
         }

         if (this.M == 0) {
            this.L = null;
         }
      }

      if (this.J > 0) {
         --this.J;
      } else {
         this.N();
         this.F.b();
         if (this.game.B() && this.A && this.E && this.G[0].b()) {
            this.game.ab().a((kY) (new fJ(this.game, this, 0)));
         }
      }

      if (this.game.B()) {
         this.b(O, (Mb) null);
      }

   }

   public void a(float var1) {
      qd.b(ip.a);
      qd.k();
      int var2 = Math.round(this.f_().c + this.b().c * var1);
      int var3 = Math.round(this.f_().d + this.b().d * var1);
      qd.b((float) var2, (float) var3);
      qd.k();
      qd.a(this.m);
      qd.c(this.C - this.B);
      this.n.e().d();
      qd.l();
      this.l.e().d();
      int var4;
      if (dh.b(this.B, -180.0F, 180.0F) > 0.0F) {
         var4 = this.r - 10;
      } else {
         var4 = this.s + 4;
      }

      qd.b(-20.0F, (float) var4);
      float var5 = (float) this.y / (float) this.health;
      Eb.a(this.resourceMgr, var5, gx.a(var5));
      qd.l();
      if (dp.a) {
         super.a(var1);
      }

   }

   public JB d() {
      return new zd(this, this);
   }

   public void c(fe var1) {
      this.game = var1;
      if (var1.B()) {
         this.H = new On(var1, 10);
      }

      this.f(1000000.0F);
   }

   public float k() {
      return 0.0F;
   }

   public void a(fe var1) {
   }

   public void b(fe var1) {
      var1.R().b(this);
   }

   public final void a(nu var1) {
   }

   public final void a(nQ var1) {
   }

   // $FF: synthetic methods
   static int a(Ww var0) {
      return var0.bulletDir;
   }

   static int b(Ww var0) {
      return var0.blastDmg;
   }

   static int c(Ww var0) {
      return var0.blastRadius;
   }

   static fe d(Ww var0) {
      return var0.game;
   }

   static float e(Ww var0) {
      return var0.C;
   }

   static float f(Ww var0) {
      return var0.D;
   }

   static int a(Ww var0, float var1) {
      return var0.k(var1);
   }

   static float g(Ww var0) {
      return var0.bulletSpeed;
   }

   static void a(Ww var0, Yr var1, float var2) {
      var0.a(var1, var2);
   }

   static oD h(Ww var0) {
      return var0.team;
   }

   static oU i(Ww var0) {
      return var0.F;
   }

   static int j(Ww var0) {
      return var0.j;
   }

   static int k(Ww var0) {
      return var0.health;
   }

   static int l(Ww var0) {
      return var0.y;
   }

   static int m(Ww var0) {
      return var0.I;
   }

   static void a(Ww var0, int var1) {
      var0.y = var1;
   }

   static boolean n(Ww var0) {
      return var0.A;
   }

   static void o(Ww var0) {
      var0.M();
   }

   static int p(Ww var0) {
      return var0.layer;
   }

   static float q(Ww var0) {
      return var0.turnLeft;
   }

   static float r(Ww var0) {
      return var0.turnRight;
   }

   static float s(Ww var0) {
      return var0.maxDistance;
   }

   static lE t(Ww var0) {
      return var0.transformation;
   }

   static float u(Ww var0) {
      return var0.cooldownSec;
   }

   static class Shoot implements bE {
      private float b;
      private int c;
      private oa d;
      // $FF: synthetic field
      final Ww a;

      private Shoot(Ww var1) {
         this.a = var1;
      }

      public void f() {
         Bullet var1;
         if (d(this.a).B()) {
            this.b = dh.b(e(this.a), -180.0F, 180.0F);
            this.c = Ww.a(this.a, Ww.f(this.a));
            var1 = new Bullet(d(this.a));
            this.d = new oa(d(this.a), var1);
            this.d.a();
         } else {
            this.d.a();
            var1 = (Bullet) this.d.i();
         }

         Yr var2 = new Yr(g(this.a) * dh.b(this.b), g(this.a) * dh.c(this.b));
         Yr var3 = new Yr();
         Ww.a(this.a, var3, this.b);
         mN var4 = d(this.a).R().a(h(this.a));
         var1.a(var4, var3, (Yr) null, var2, (Yr) null, this.b);
         var1.A().c((float) this.c);
         i(this.a).d();
         Bullet.a(var1, this.a);
      }

      public boolean b() {
         return i(this.a).e();
      }

      public void a(nu var1) {
         Vh.a(var1, this.b);
         var1.a(0, j(this.a), this.c);
         this.d.a(var1);
      }

      public void a(nQ var1) {
         this.b = (float) Vh.a(var1);
         this.c = var1.a(0, j(this.a));
         this.d = new oa(d(this.a));
         this.d.a(var1);
      }

      public void c() {
      }

      public static class Bullet extends Jp {
         private Ww a;

         public Bullet(fe var1) {
            super(var1, "map/turret/bullet.poly", 10, sx.d(1.55F));
            this.a("map/turret/turret_fire.wav", this.e, this.f);
         }

         public void a(mN var1, Yr var2, Yr var3, Yr var4, Yr var5, float var6) {
            super.a(var1, var2, var3, var4, var5, var6);
         }

         private void a(Ww var1) {
            this.a = var1;
            this.b(Ww.a(var1));
            this.a((Mb) var1);
         }

         public void a(Yl var1) {
            if (var1 != this.a) {
               super.a(var1);
            }

         }

         public void D_() {
            if (Ww.b(this.a) > 0 && Ww.c(this.a) > 0) {
               vU.a(this.t(), this.a, this.f_(), Ww.c(this.a), Ww.b(this.a), 0.0F, 1.0F, false, -1, false);
               if (this.t().F() && this.t().a(this.f_().c, this.f_().d, (float) (Ww.c(this.a) + 40))) {
                  this.u().b(new HK(this.t(), this.u(), new Yr(this.f_()), (float) Ww.c(this.a), 0));
               }

               kr var1 = this.t().G().b("map/turret/turret_shot_explode.wav");
               DA var2 = this.t().ah().a(var1, this.f_(), dc.a);
               var2.l();
            }

         }

         // $FF: synthetic method
         static void a(Bullet var0, Ww var1) {
            var0.a(var1);
         }
      }
   }
}
