import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

// ServerCommand
// - Added dynamic permissions system
public abstract class Os extends lj {
   private static final Logger logger = Logger.getLogger(Os.class);
   public static final String[] i = new String[]{"Yes", "No"};
   protected fe j;
   protected double k = 0.5D;
   private List<YY> commandGroups = new ArrayList<>();
   protected UUID uuid;

   public Os(String name, fe game) {
      super(name);
      this.j = game;
      this.commandGroups.add(YY.b);
   }

   protected void a(NB listener, String... args) {
      if (!this.j.ab().o()) {
         listener.a(listener.c() + "You must be connected to a server to execute \'" + this.f() + "\'.", false);
      } else {
         String commandString = lj.a(this.f(), args);
         this.j.ao().a(commandString, listener);
         rC commandEv = new rC(this.j, commandString);
         if (this.j.B()) {
            System.err.println("Cannot execute console command: " + commandEv);
         } else {
            this.j.ab().b(commandEv);
         }
      }
   }

   protected int port() { return this.j.u().c().port(); }

   // Returns true if a command received from a client should be executed.
   public boolean b(NB listener, FW userGroup, Jt client, String... args) {
      Permissions.PermissionResult result = Permissions.PermissionResult.Missing;

      Permissions permissions = this.j.u().c().getPermissions();

      if (permissions.isEnabled()) {
         // Check custom permissions
         result = permissions.check_command(
                 userGroup.a().D(), this.f(), userGroup.c());
      }

      // If the configuration is not found, or is disabled, use
      // the default system.
      if (result == Permissions.PermissionResult.Missing) {
         boolean allow = false;
         // Loop over this command's allowed groups.
         for (YY commandGroup : this.commandGroups) {
            // If the user is in this group, allow them to use the command.
            if (commandGroup.equals(userGroup.c())) {
               allow = true;
               break;
            }
         }
         if (!allow) {
            // Send a message to the command listener telling the user they
            // don't have permission. `listener.c()` set's the colour of the
            // text.
            listener.a(listener.c() + "You do not have permission to run " +
                    "the command '" + this.f() + "'.", false);
            // Return false, indicating that the command failed
            return false;
         } else {
            // Checks that the arguments are valid and run the abstract
            // `canExecute` method.
            return this.c(listener, args) && this.a(listener, userGroup, client, args);
         }
      } else if (result == Permissions.PermissionResult.Allow) {
         return this.c(listener, args) && this.a(listener, userGroup, client, args);
      } else if (result == Permissions.PermissionResult.NoPermission) {
         listener.a(listener.c() + "You do not have permission to run " +
                 "the command '" + this.f() + "'.", false);
         return false;
      } else if (result == Permissions.PermissionResult.NoPermissionVote) {
         listener.a(listener.c() + "You do not have permission to call a vote" +
                 " for the command " + this.f() + ".", false);
         return false;
      } else {
         listener.a(listener.c() + "The command '" + this.f() + "' is disabled" +
                 ".", false);
         return false;
      }
   }

   // canExecute - non-permission based check to see if the command can
   // be executed, for example, /startTournament checks if there are at least
   // two non-spectating players.
   protected abstract boolean a(NB var1, FW var2, Jt var3, String[] var4);

   public ai a(FW _var1, Jt _var2, String... var3) {
      StringBuilder var4 = new StringBuilder();
      var4.append(this.f());

      for (String aVar3 : var3) {
         var4.append(" ");
         var4.append(aVar3);
      }

      ai var6 = new ai();
      var6.a(var4.toString(), 0);
      return var6;
   }

   // Execute the command.
   public void a(NB listener, FW userInfo, Jt client, int[] var4, String... args) {
      if (this.b(listener, userInfo, client, args)) {
         try {
            UUID uuid;
            String group;
            if (userInfo != null && userInfo.a() != null && userInfo.a().D() != null) {
               uuid = userInfo.a().D();
               group = userInfo.c().a();
            } else {
               uuid = new UUID(0L, 0L);
               group = "server";
            }
            this.uuid = uuid;

            da log = JL.a("consoleCommandExecute");
            log.a("source", uuid);
            log.a("group", group);
            log.a("command", this.f());
            log.a("arguments", nT.a((Object[]) args));
            this.j.u().c().a(log);
         } catch (Exception e) {
            logger.error(e, e);
         }

         this.a(var4, args);
      }
   }

   protected abstract void a(int[] var1, String... var2);

   // Get permissions
   public List g() {
      return this.commandGroups;
   }

   // Remove permission
   public void a(YY x) {
      this.commandGroups.remove(x);
   }

   // Add permission
   public void b(YY x) {
      this.a(x);
      this.commandGroups.add(x);
   }

   // Add permissions
   public void a(Collection c) {
      this.b(c);
      this.commandGroups.addAll(c);
   }

   // Remove permissions
   public void b(Collection c) {
      this.commandGroups.removeAll(c);
   }

   // Replace permissions
   public void c(Collection var1) {
      this.commandGroups.clear();
      this.a(var1);
   }

   public double c() {
      return this.k;
   }

   public void a(double var1) {
      this.k = var1;
   }

   public String[] a(String[] x) {
      return i;
   }

   public boolean a(int[] var1) {
      int var2 = var1[0] + var1[1];
      return (double) var1[0] / (double) var2 > this.c();
   }

   public boolean a(int[] a, int b) {
      double var3 = this.c();
      int var5 = (int) Math.ceil(((double) b + 0.01D) * var3);
      int var6 = (int) Math.ceil((double) b * (1.0D - var3));
      return a[0] >= var5 || a[1] >= var6;
   }

   public boolean d() {
      return true;
   }

   public boolean e() {
      return true;
   }
}
