import em.entry.EntryServerLauncher;
import mods.Config;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

// ServerLauncher
// - The `serverPatches` JSON log event is sent just after `sessionStart`
public class jt {
   private static final Logger h = Logger.getLogger(jt.class);
   public static final File a = new File("servers");
   public static final File b;
   public static final File c;
   public static final File d;
   public static final File e;
   public static final File f;
   public static final File g;
   private static final File i;
   private String[] k;
   private Cz l;
   private DZ m;
   private mo n;
   private hY o;
   private uT p;
   private kg q;
   private qo r;
   private long s;
   private List t;
   private InetAddress u;
   private boolean v;
   private boolean w;
   private boolean x = GraphicsEnvironment.isHeadless();
   private boolean y;
   private JFrame z;
   private ry A;
   private JTextArea B;
   private JScrollPane C;
   private JScrollBar D;
   private String E = null;
   private String F = null;
   private String[] G = null;
   private String[] H = null;
   private String[] I = null;
   private Object J;
   private boolean K = true;
   private boolean L = false;

   static {
      b = new File(a, "server_launcher.pid");
      c = new File(a, "launcher_config.xml");
      d = new File(a, "ban_list.xml");
      e = new File(a, "command.txt");
      f = new File(a, "custom_json_commands.txt");
      g = new File(a, "log.txt");
      i = new File(a, "log_old.txt");
   }

   public jt(oZ var1, String[] var2) {
      this.a(var2);
      if (this.x) {
         qH.a((Layout) (new xM()));
      } else {
         Vj.a();
         this.l();
      }

      PK j = var1.a();
      this.k = var2;
      this.a(var1);
      if (this.l.a().size() == 0) {
         String var3 = "No servers specified in " + zX.d(c);
         var3 = var3 + "\nRun \'Server Configurator\' to create a launchable server config.";
         this.a(var3);
      }

      this.d();
      this.e();
      this.f();
      this.i();
      this.j();
      this.k();
      this.h();
      this.g();

      try {
         h.info("Resolving update server " + Config.global.vapor_address);
         this.m = yV.a();
         h.info("Update server is " + this.m);
         this.n = new mo(this, this.m, var1, this.u, this.l.b());
      } catch (Exception var6) {
         h.error("ServerLauncher update initialization failed", var6);
         this.a("ServerLauncher update initialization failed, exiting.");
      }

      try {
         tJ var7 = this.n.d();
         h.info("Initial update check result: " + var7);
         if (var7 == tJ.d) {
            this.v();
            return;
         }
      } catch (Exception var5) {
         h.error("Problem performing initial update check", var5);
      }

      try {
         this.o = new hY(this.m, j, this.r, this.t, this.q, this.u, this.l.a());
      } catch (Exception var4) {
         h.error("ServerLauncher server initialization failed", var4);
         this.a("ServerLauncher server initialization failed, exiting.");
      }

      this.t();
   }

   private void a(String[] var1) {
      for (int var2 = 0; var2 < var1.length; ++var2) {
         String var3 = var1[var2];
         String var4 = var2 + 1 < var1.length ? var1[var2 + 1] : null;
         if (var3.equalsIgnoreCase("-noui")) {
            this.x = true;
         } else {
            String var5;
            String var6;
            if (var3.toLowerCase().startsWith("-ip")) {
               var5 = var3.substring("-ip".length());
               var6 = var5.length() > 0 ? var5 : var4;
               this.E = var6;
            } else if (var3.toLowerCase().startsWith("-updateport")) {
               var5 = var3.substring("-updateport".length());
               var6 = var5.length() > 0 ? var5 : var4;
               this.F = var6;
            } else if (var3.toLowerCase().startsWith("-gameports")) {
               var5 = var3.substring("-gameports".length());
               var6 = var5.length() > 0 ? var5 : var4;
               this.G = var6.split("\\,");
            } else if (var3.toLowerCase().startsWith("-maxplayers")) {
               var5 = var3.substring("-maxplayers".length());
               var6 = var5.length() > 0 ? var5 : var4;
               this.H = var6.split("\\,");
            } else if (var3.toLowerCase().startsWith("-downloadmaxkilobytespersecond")) {
               var5 = var3.substring("-downloadmaxkilobytespersecond".length());
               var6 = var5.length() > 0 ? var5 : var4;
               this.I = var6.split("\\,");
            } else if (var3.toLowerCase().equalsIgnoreCase("--help")) {
               var5 = "Command line overrides:";
               var5 = var5 + "\n   -ip X";
               var5 = var5 + "\n   -updatePort X";
               var5 = var5 + "\n   -gamePorts X,Y,Z";
               var5 = var5 + "\n   -maxPlayers X,Y,Z";
               var5 = var5 + "\n   -downloadMaxKilobytesPerSecond X,Y,Z";
               var5 = var5 + "\nExample for 1 instance: ./server_launcher -ip 192.168.1.3 -updatePort 40000 -gamePorts 40001 -maxPlayers 14 -downloadMaxKilobytesPerSecond 40";
               var5 = var5 + "\nExample for 3 instances: ./server_launcher -ip 192.168.1.3 -updatePort 40000 -gamePorts 40001,40002,40003 -maxPlayers 8,14,14 -downloadMaxKilobytesPerSecond 40,80,80";
               var5 = var5 + "\n\nOther options:";
               var5 = var5 + "\n   -noui            disables graphical user interface in non-headless environments";
               this.a(var5);
            }
         }
      }

   }

   private void d() {
      int var1 = this.l.a().size();
      if (this.E != null) {
         this.l.a(this.E);
      }

      if (this.F != null) {
         this.l.a(Integer.parseInt(this.F));
      }

      String var2;
      int var7;
      if (this.G != null) {
         if (this.G.length != var1) {
            var2 = "Server count (" + var1 + ") does not match comma separated -gamePorts count (" + this.G.length + ").";
            var2 = var2 + "\nExample for 3 servers: -gamePorts 48000,48001,48002";
            this.a(var2);
         }

         for (var7 = 0; var7 < var1; ++var7) {
            try {
               ((hp) this.l.a().get(var7)).a(Integer.parseInt(this.G[var7]));
            } catch (Exception var6) {
               this.a("gamePorts value \'" + this.G[var7] + "\' is not a valid integer.");
            }
         }
      }

      if (this.H != null) {
         if (this.H.length != var1) {
            var2 = "Server count (" + var1 + ") does not match comma separated -maxPlayers count (" + this.H.length + ").";
            var2 = var2 + "\nExample for 3 servers: -maxPlayers 8,14,14";
            this.a(var2);
         }

         for (var7 = 0; var7 < var1; ++var7) {
            try {
               ((hp) this.l.a().get(var7)).b(Integer.parseInt(this.H[var7]));
            } catch (Exception var5) {
               this.a("maxPlayers value \'" + this.H[var7] + "\' is not a valid integer.");
            }
         }
      }

      if (this.I != null) {
         if (this.I.length != var1) {
            var2 = "Server count (" + var1 + ") does not match comma separated -downloadMaxKilobytesPerSecond count (" + this.I.length + ").";
            var2 = var2 + "\nExample for 3 servers: -downloadMaxKilobytesPerSecond 300,40,40";
            this.a(var2);
         }

         for (var7 = 0; var7 < var1; ++var7) {
            try {
               ((hp) this.l.a().get(var7)).f(Integer.parseInt(this.I[var7]));
            } catch (Exception var4) {
               this.a("downloadMaxKilobytesPerSecond value \'" + this.I[var7] + "\' is not a valid integer.");
            }
         }
      }

   }

   private void e() {
      try {
         String var1 = String.valueOf(WZ.a());
         h.info("PID is " + var1 + " [written to " + b.getAbsolutePath() + "]");
         gj.a(var1.getBytes(), b);
      } catch (Exception var2) {
         h.error("Failed to write PID", var2);
      }

   }

   private void f() {
      try {
         this.p = new uT();
         h.info("Reading commands from " + zX.d(e));
      } catch (Exception var2) {
         h.error("ServerCommandManager initialization failed", var2);
      }

   }

   private void g() {
      if (this.l.d()) {
         h.info("Configuring UPnP");

         try {
            JP var1 = (new Nq()).a(DZ.a(), 3000);
            List var2 = var1.a(2000);
            if (var1 != JP.a) {
               ArrayList var3 = new ArrayList();
               var3.add(Integer.valueOf(this.l.b()));
               Iterator var5 = this.l.a().iterator();

               while (var5.hasNext()) {
                  hp var4 = (hp) var5.next();
                  var3.add(Integer.valueOf(var4.a()));
               }

               fe.a("ALTS", "UDP", var3, var1, var2, false);
               h.info("UPnP mappings: " + var1.a(2000));
            } else {
               h.info("No UPnP gateway found");
            }
         } catch (Exception var6) {
            h.error("UPnP configuration failed", var6);
         }
      }

   }

   private void a(oZ var1) {
      xO var2 = new xO(var1);
      h.info("Loading ServerLauncher config from " + zX.d(c));
      if (!c.exists()) {
         this.l = new Cz(var1);
         h.info("Launcher config does not exist, creating default config: " + zX.d(c));

         try {
            eo.a((File) c, (Object) this.l, (Kl) var2);
         } catch (Exception var5) {
            h.error("Failed to create default launcher config", var5);
         }
      } else {
         try {
            this.l = (Cz) eo.a((File) c, (Kl) var2);
         } catch (Exception var4) {
            h.error("Failed to load ServerLauncher config from " + zX.d(c), var4);
            this.l = new Cz(var1);
         }
      }

   }

   private void h() {
      try {
         if (this.l.c().length() > 0) {
            this.u = InetAddress.getByName(this.l.c());
         }
      } catch (Exception var2) {
         h.error(var2, var2);
         this.u = null;
      }

      if (this.u == null) {
         this.u = DZ.a();
      }

   }

   private void i() {
      ey var1 = new ey(g, i, 4194304L, (SimpleDateFormat) null, (String) null, false);
      if (var1.a()) {
         h.info("Logging server activity to " + zX.d(g));
         this.q = new kg(var1);

         try {
            da var2 = JL.a("sessionStart");
            var2.a("date", (Object) (new SimpleDateFormat("yyyy MMM dd HH:mm:ss:SSS z")).format(new Date()));
            this.q.a(-1, var2);
            this.q.a(-1, ServerPatches.make_log());
         } catch (Exception var3) {
            h.error(var3, var3);
         }

         kS.b(new Wg(this));
      }

   }

   private void j() {
      h.info("Loading ban list from " + zX.d(d));
      if (!d.exists()) {
         this.r = new qo();
         h.info("Ban list does not exist, creating empty ban list: " + zX.d(d));

         try {
            eo.a((File) d, (Object) this.r, (Kl) qo.a);
         } catch (Exception var3) {
            h.error("Failed to create empty ban list", var3);
         }
      } else {
         try {
            this.r = (qo) eo.a(d, qo.a);
         } catch (Exception var2) {
            h.error("Failed to load ban list from " + zX.d(d), var2);
            this.r = new qo();
         }
      }

      this.u();
   }

   private void k() {
      h.info("Loading dynamic command JSON definitions from " + zX.d(f));
      this.t = new ArrayList();

      try {
         // Add commands defined by patches
         this.t.addAll(Commands.get_client_commands());
         this.h.info("mod_commands_json: " + Commands.get_all_commands());

         BufferedReader var1 = new BufferedReader(new FileReader(f));

         String var2;
         while ((var2 = var1.readLine()) != null) {
            var2 = var2.trim();
            if (var2.length() > 0) {
               this.t.add(var2);
            }
         }
      } catch (Exception var3) {
         if (f.exists()) {
            h.error("Failed to load dynamic commands", var3);
         }
      }

   }

   private void l() {
      this.z = new JFrame();
      JOptionPane.setRootFrame(this.z);
      this.z.setDefaultCloseOperation(0);
      this.z.addWindowListener(new Wf(this));
      this.z.setIconImage(Vj.a("server_launcher.png").getImage());
      this.z.setTitle("Altitude Server Launcher");
      this.z.setJMenuBar(this.n());
      this.z.getContentPane().add(this.q(), "North");
      this.z.getContentPane().add(this.m(), "Center");
      this.p();
      int var1 = Toolkit.getDefaultToolkit().getScreenSize().height;
      this.z.setSize(new Dimension(800, Math.round(0.9F * (float) var1) - 80));
      this.z.setLocationRelativeTo((Component) null);
      this.z.setVisible(true);
   }

   private JScrollPane m() {
      this.B = new JTextArea();
      this.B.setEditable(false);
      this.B.setFont(new Font("Courier New", 0, 12));
      boolean var1 = true;
      this.B.getDocument().addDocumentListener(new Wl(this));
      this.C = new JScrollPane(this.B);
      this.D = this.C.getVerticalScrollBar();
      return this.C;
   }

   private JMenuBar n() {
      JMenuBar var1 = new JMenuBar();
      JMenu var2 = new JMenu("File");
      var2.setMnemonic('f');
      var1.add(var2);
      this.A = ro.a();
      JMenuItem var3 = new JMenuItem(Wc.a("Minimize to tray", 77, 77, new Wk(this)));
      var3.setEnabled(this.A.a());
      var2.add(var3);
      JMenuItem var4 = new JMenuItem(Wc.a("Exit", 88, 0, new Wj(this)));
      var2.add(var4);
      return var1;
   }

   private void o() {
      if (this.J != null) {
         this.A.a(this.J);
         this.J = null;
      }

      if (this.z != null) {
         this.z.setVisible(true);
      }

      this.K = true;
   }

   private void p() {
      JScrollBar var1 = this.D;
      this.B.addMouseWheelListener(new Wi(this, var1));
      var1.addAdjustmentListener(new Wo(this, var1));
      PatternLayout var2 = new PatternLayout("[%d{DATE}] %p: %m%n");
      WriterAppender var3 = new WriterAppender(var2, new Wq(this));
      Logger.getRootLogger().addAppender(var3);
   }

   private JPanel q() {
      JPanel var1 = new JPanel(new GridBagLayout());
      JTextArea var2 = new JTextArea("Log File: " + zX.d(EntryServerLauncher.a));
      var2.setBackground(var1.getBackground());
      var2.setEditable(false);
      JPopupMenu var3 = new JPopupMenu();
      JMenuItem var4 = new JMenuItem("Copy");
      var4.addActionListener(new Wn(this, var2));
      var3.add(var4);
      var2.addMouseListener(new Rb(this, var4, var2, var3));
      GridBagConstraints var5 = new GridBagConstraints();
      var5.insets.set(2, 2, 2, 2);
      var5.gridx = 1;
      var5.gridy = 1;
      var1.add(var2, var5);
      ++var5.gridx;
      var5.weightx = 1.0D;
      var1.add(new JPanel(), var5);
      return var1;
   }

   private void a(String var1) {
      h.error(var1);
      if (this.x) {
         try {
            h.error("Press [Enter] to exit.");
            if (fe.b) {
               System.err.println("developer mode -- automatically exiting without [Enter]");
            } else {
               System.in.read();
            }
         } catch (IOException var3) {
            h.error(var3, var3);
         }
      } else {
         this.o();
         JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), var1, "Fatal Error", 0);
      }

      this.r();
      System.exit(1);
   }

   private void r() {
      if (this.p != null) {
         this.p.a();
         this.p = null;
      }

      if (this.q != null) {
         this.q.a();
         this.q = null;
      }

   }

   private void s() {
      if (this.o != null) {
         this.o.c();
      }

      this.r();
      System.exit(0);
   }

   private void t() {
      this.n.b();
      this.o.a();
      if (this.p != null) {
         this.p.a(this, this.o);
      }

      for (; !this.w && this.n.a() != tJ.d && this.n.a() != tJ.b; kS.a(500)) {
         if (!this.v && this.o.d()) {
            this.v = true;
            h.warn("All servers have unexpectedly shut down.");
         }

         long var1 = (long) (this.r.c() ? 30 : 300) * 1000L;
         if (System.currentTimeMillis() - this.s > var1) {
            this.s = System.currentTimeMillis();
            this.u();
         }

         if (!this.y) {
            boolean var3 = true;
            boolean var4 = false;

            for (int var5 = 0; var5 < this.o.f().size(); ++var5) {
               sl var6 = (sl) this.o.f().get(var5);
               if (var6.a.q() == null || !var6.a.d().b().b()) {
                  var3 = false;
                  break;
               }

               if (var6.a.m().a() != var6.a.q().h() || var6.a.d().b().d()) {
                  var4 = true;
               }
            }

            if (var3) {
               this.y = true;
               if (var4 && !this.x) {
                  new Kx(this.z, this.z.getIconImage(), new Rc(this));
               }
            }
         }
      }

      this.v();
   }

   private void u() {
      try {
         this.r.a(System.currentTimeMillis());
         if (this.r.c()) {
            h.info("Saving updated ban list to " + d);
            this.r.a(d);
         }
      } catch (Exception var2) {
         h.error("Failed to save ban list", var2);
      }

   }

   private void v() {
      h.info("Preparing ServerLauncher for restart");
      if (this.q != null) {
         try {
            da var1 = JL.a("updatePrepareRestart");
            this.q.a(-1, var1);
         } catch (Exception var7) {
            h.error(var7, var7);
         }
      }

      try {
         if (this.n != null) {
            this.n.c();
         }

         if (this.o != null) {
            this.o.b();
         }

         this.r();
         if (this.r != null) {
            this.u();
         }

         kS.a(1000);
         h.info("Releasing memory: " + ml.a());
         this.l = null;
         this.r = null;
         this.o = null;
         this.n = null;

         for (int var9 = 0; var9 < 6; ++var9) {
            System.gc();
         }

         h.info("Release complete: " + ml.a());
         h.info("Restarting ServerLauncher");
         File var10 = new File(".");
         short var2 = 1500;
         String[] var3 = VZ.a(var2, var10, "server_launcher", this.k);
         Process var4 = Ma.a(var10, "relauncher", var3);
         kS.a(1000);

         try {
            h.info("Restart process exit value: " + var4.exitValue());
         } catch (IllegalThreadStateException var6) {
            h.info("Restart process is running");
         }
      } catch (Throwable var8) {
         h.error("Error relaunching ServerLauncher", var8);
      }

      System.exit(0);
   }

   public static void a(oZ var0, String[] var1) {
      try {
         new jt(var0, var1);
      } catch (OutOfMemoryError var3) {
         h.error(var3, var3);
         new ce((JFrame) null, fe.an(), bG.b);
      } catch (Throwable var4) {
         h.error(var4, var4);
         new ce((JFrame) null, fe.an(), bG.a);
      }
   }

   public void a() {
      if (this.o != null) {
         this.o.e();
      }

   }

   public void b() {
      this.w = true;
   }

   // $FF: synthetic method
   static kg a(jt var0) {
      return var0.q;
   }

   // $FF: synthetic method
   static Logger c() {
      return h;
   }

   // $FF: synthetic method
   static void b(jt var0) {
      var0.s();
   }

   // $FF: synthetic method
   static JTextArea c(jt var0) {
      return var0.B;
   }

   // $FF: synthetic method
   static JFrame d(jt var0) {
      return var0.z;
   }

   // $FF: synthetic method
   static void e(jt var0) {
      var0.o();
   }

   // $FF: synthetic method
   static Object f(jt var0) {
      return var0.J;
   }

   // $FF: synthetic method
   static ry g(jt var0) {
      return var0.A;
   }

   // $FF: synthetic method
   static void a(jt var0, Object var1) {
      var0.J = var1;
   }

   // $FF: synthetic method
   static JScrollPane h(jt var0) {
      return var0.C;
   }

   // $FF: synthetic method
   static void a(jt var0, boolean var1) {
      var0.K = var1;
   }

   // $FF: synthetic method
   static boolean i(jt var0) {
      return var0.L;
   }

   // $FF: synthetic method
   static boolean j(jt var0) {
      return var0.K;
   }

   // $FF: synthetic method
   static void b(jt var0, boolean var1) {
      var0.L = var1;
   }

   // $FF: synthetic method
   static hY k(jt var0) {
      return var0.o;
   }
}
