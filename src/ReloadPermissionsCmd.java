class ReloadPermissionsCmd extends Command {
   public ReloadPermissionsCmd(fe game) {
      super(game, "reloadPermissions", PermissionGroup.admin);
      this.setParams(new JI[]{});
   }

   protected void execute(int[] a, String... b) {
      this.getServer().loadPermissions();
   }
}
