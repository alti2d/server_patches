import org.apache.log4j.Logger;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Plane
// - Added more info to JSON logs
public class mN extends gi implements D, Sp, gI, n, ou, yJ {
   private static final Logger a = Logger.getLogger(mN.class);
   private static final ip s = new ip(0.5F, 1.0F, 0.5F, 0.5F);
   public static final qt b = sx.d(1.5F);
   public static final qt c = sx.d(2.5F);
   private static final float t;
   public static final int d;
   protected tp e;
   private float u;
   public float f;
   private float v = 8.37F;
   private float w;
   private float x;
   private float y;
   private float z;
   private boolean A;
   private float B;
   private float C;
   private float D;
   private float E;
   private float F;
   private float G;
   private float H;
   private float I;
   private static final float J;
   private final Yr K;
   private static final float L;
   private float M;
   private final Yr N;
   private float O;
   private float P;
   private final Yr Q;
   private float R;
   private int S;
   private vk T;
   private boolean U;
   private boolean[] V;
   public boolean g;
   public boolean h;
   public boolean i;
   public boolean j;
   public boolean k;
   public boolean l;
   public boolean m;
   public boolean n;
   private boolean W;
   private bE[] X;
   private SR Y;
   private Nr Z;
   private String aa;
   private String ab;
   private String ac;
   private int ad;
   private float ae;
   public static final float o;
   private float af;
   private float ag;
   private float ah;
   private float ai;
   private float aj;
   private float ak;
   private float al;
   private static final JZ am;
   private float an;
   private float ao;
   private float ap;
   private float aq;
   private float ar;
   private int as;
   private oD at;
   private boolean au;
   private boolean av;
   private boolean aw;
   private int ax;
   public static final int p;
   private On ay;
   private UH az;
   private boolean aA;
   private boolean aB;
   private Eb aC;
   private float aD;
   private float aE;
   private PY[] aF;
   private PY[] aG;
   private float[] aH;
   private XQ[] aI;
   private float aJ;
   private tK aK;
   private float aL;
   private float aM;
   private float aN;
   private static final Yr aO;
   private Yr aP;
   private Yr aQ;
   private Yr aR;
   private Yr aS;
   private fe aT;
   private static final Map aU;
   private static final Map aV;
   private static int aW;
   private BT aX;
   private BT aY;
   private BT aZ;
   private BT ba;
   private BT bb;
   private float bc;
   private Ax bd;
   private PG be;
   private mS bf;
   private int bg;
   private int bh;
   private static final float bi;
   private int bj;
   private int bk;
   private int bl;
   private int bm;
   private int bn;
   private float bo;
   private jV bp;
   private Tx bq;
   private Tx br;
   private Tx bs;
   private sQ bt;
   private Float bu;
   private iD bv;
   private static final qt bw;
   private oU bx;
   private static final Yr by;
   private static final ip bz;
   private ip bA;
   private static final String[] bB;
   private static final int bC;
   private OP bD;
   private kt bE;
   private kt bF;
   private boolean bG;
   protected HF q;
   protected HF r;
   private Kq bH;
   private Hf bI;
   private static final qt bJ;
   private float bK;
   private float bL;
   private float bM;
   private float bN;
   private int bO;
   private int bP;
   private boolean bQ;
   private int bR;
   private sU bS;
   private List bT;
   private int bU;
   private int bV;
   private OC bW;
   private OC notBx;
   private OC bY;
   private boolean bZ;
   private static final float ca;

   private float last_x;
   private float last_y;
   private float last_angle;

   static {
      t = c.a() / 3.0F;
      d = Am.values().length;
      J = dh.e(0.953F, 1.0F);
      L = dh.e(0.8F, 1.0F);
      o = sx.d(4.0F).a();
      am = new JZ(1.0F, 0.0F, 0.0F, 0.125F, 0.0F);
      p = (int) sx.d(5.0F).a();
      aO = new Yr();
      aU = new HashMap();
      aV = new HashMap();
      aW = -1;
      bi = sx.d(4.0F).a();
      bw = sx.d(10.0F);
      by = new Yr();
      bz = new ip(0.45F, 0.45F, 0.45F);
      bB = new String[]{"effects/veteran_bars/bronze_bar.png", "effects/veteran_bars/silver_bar.png", "effects/veteran_bars/gold_bar.png"};
      bC = DX.d(1, (eK) null);
      bJ = sx.d(0.8F);
      ca = sx.d(12.0F).a();
   }

   public mN(fe var1) {
      super(true);
      this.w = this.v;
      this.x = 2.45F;
      this.y = 4.24F;
      this.z = 0.49F;
      this.C = 0.6F;
      this.D = 6.5F;
      this.F = 0.5F;
      this.H = 35.0F;
      this.I = 35.0F;
      this.K = new Yr();
      this.N = new Yr();
      this.Q = new Yr();
      this.R = 0.0F;
      this.S = 0;
      this.V = new boolean[d];
      this.X = new bE[]{sf.a, sf.a, sf.a, sf.a};
      this.aa = "?NONAME?";
      this.ab = "?NONAME?";
      this.ad = 250;
      this.ah = 0.5F;
      this.ai = 13.5F;
      this.aj = 1.0F;
      this.ak = 1.0F;
      this.al = 1.0F;
      this.an = 1000.0F;
      this.aq = 100.0F;
      this.ar = 0.3F;
      this.az = new UH();
      this.aP = aO;
      this.aQ = new Yr();
      this.aR = aO;
      this.aS = new Yr();
      this.bg = 0;
      this.bh = 0;
      this.bj = 0;
      this.bk = 0;
      this.bl = 0;
      this.bm = 0;
      this.bn = 0;
      this.bo = 1.0F;
      this.bA = ip.a;
      this.bE = new LP(this);
      this.bF = new MC(this);
      this.bT = null;
      this.bV = 30;
      this.bW = new OC(this.bV);
      this.notBx = new OC(this.bV);
      this.bY = new OC(this.bV);
      this.aT = var1;
      this.bu = var1.J().u().ak().b();
      this.ay = new On(var1, 6);
      this.aK = tK.a(this.getClass());
      this.b("?NONAME?");
      this.c("?NONAME?");
      this.h(0.6F);
      this.i(6.5F);
      this.j(8.37F);
      this.k(0.49F);
      this.b(250);
      this.m(1.0F);
      this.l(20.0F);
      this.c(90.0F, 0.3F);
   }

   public void a(oD var1) {
      this.at = var1;
      StringBuilder var2 = new StringBuilder();
      String var3 = this.ag().toLowerCase();
      var2.append("render/planes/");
      var2.append(var3);
      var2.append("/");
      var2.append(var1.d());
      var2.append(".animatedpoly");
      this.a(var2.toString());
   }

   public Float I() {
      return this.bu;
   }

   public void a(Float var1) {
      if (!var1.equals(this.bu)) {
         this.bu = var1;
         this.a(this.aV());
      }

   }

   protected void a(String var1) {
      this.ac = var1;
      Rl var2 = this.aT.G();
      GQ var3 = var2.d(this.I(), var1);
      int var4 = -1;
      int var5 = -1;
      List var6 = var3.a();

      int var8;
      for (int var7 = 0; var7 < var6.size(); ++var7) {
         var8 = ((xz) var6.get(var7)).a();
         if (var8 == 0) {
            var4 = var7;
         } else if (var8 == 1800) {
            var5 = var7;
         }
      }

      if (var4 != -1 && var5 != -1) {
         this.aF = new PY[var6.size()];
         this.aH = new float[var6.size()];
         XQ[] var11 = var2.b(this.I(), var1);
         this.aI = new XQ[var6.size()];

         for (var8 = 0; var8 < var6.size(); ++var8) {
            xz var9 = (xz) var6.get(var8);
            float var10 = dh.b((float) var9.a() / 10.0F, -180.0F, 180.0F);
            this.aF[var8] = var9.b().e();
            this.aH[var8] = var10;
            if (-this.aL <= var10 && var10 <= this.aL || var10 <= -180.0F + this.aL || var10 >= 180.0F - this.aL) {
               this.aI[var8] = var11[var8];
            }
         }

         this.a((XQ[]) this.aI);
         this.a((JZ) am);
         this.aJ = this.f().E();
         this.e = new tp(this.getGame().G().a("effects/afterburner/afterburner1.png"), this, new Yr(-this.aJ, 0.0F), 180.0F);
      } else {
         throw new RuntimeException("Failed to find key frame indices; upright=" + var4 + ", flipped=" + var5);
      }
   }

   public JZ l() {
      return am;
   }

   public fe getGame() {
      return this.aT;
   }

   public fe J() {
      return this.getGame();
   }

   public void b(String var1) {
      this.aa = var1;
   }

   public void c(String var1) {
      this.ab = var1;
   }

   public void h(float var1) {
      this.C = var1 * 1.0F;
   }

   public void i(float var1) {
      this.D = var1 * 1.0F;
   }

   public void j(float var1) {
      this.v = var1 * 1.0F;
      this.w = this.v;
   }

   public float K() {
      return this.w;
   }

   public float L() {
      return this.b().d() / this.v;
   }

   public void c(float var1, float var2) {
      this.aq = var1;
      this.ar = var2;
   }

   public float M() {
      return this.aq;
   }

   public float N() {
      return this.ar;
   }

   public void k(float var1) {
      this.z = 0.8F * var1 * 1.0F * 1.0F;
   }

   public void b(int var1) {
      this.ad = var1;
      this.ae = (float) var1;
   }

   public void l(float var1) {
      this.ap = var1;
   }

   public void m(float var1) {
      this.ag = var1;
   }

   public float O() {
      return this.ag;
   }

   public void a(float var1, float var2, float var3, float var4) {
      this.ah = var1;
      this.ai = var2;
      this.aj = var3;
      this.ak = var4;
   }

   public float P() {
      return this.al;
   }

   public float Q() {
      return this.ai;
   }

   public void a(jV var1) {
      this.bp = var1;
      this.bq = var1.i();
      this.br = var1.k();
      this.bs = var1.l();
      this.ad = (int) ((float) this.ad * this.getGame().J().u().ak().z());
      if (this.a(RR.ao)) {
         this.ad = Math.round(17.0F + 1.16F * (float) this.ad);
      }

      if (this.a(RR.ap)) {
         this.bt = new sQ(this);
      }

      if (this.a(RR.aq)) {
         this.i(this.D * 1.2F);
         this.h(this.C * 1.2F);
      }

      if (this.a(RR.ar)) {
         this.ap *= 1.2F;
      }

      if (this.a(RR.as)) {
         this.an = 1350.0F;
      }

      if (this.a(RR.at)) {
         this.I *= 1.9F;
         this.H *= 1.9F;
      }

      if (this.getGame().F() && this.getGame().t().e().s() && var1.n()) {
         QO var2 = var1.j().h();
         String var3 = var2.f();
         String var4 = "render/skins/" + this.bc().h + "/" + var3 + ".animatedpoly";
         List var5 = this.aT.G().d(this.I(), var4).a();
         this.aG = new PY[var5.size()];

         for (int var6 = 0; var6 < this.aG.length; ++var6) {
            this.aG[var6] = ((xz) var5.get(var6)).b().e();
         }
      }

   }

   public jV R() {
      return this.bp;
   }

   public boolean a(Tx var1) {
      return this.bq == var1 || this.br == var1 || this.bs == var1;
   }

   public Yr S() {
      return by;
   }

   public float T() {
      return (float) this.bn / 9.0F;
   }

   public void U() {
      this.au = false;
      this.a(ip.a);
      this.ae = (float) this.ad;
      this.ao = this.an;
      this.G = 1000.0F;
      this.f = this.w * this.w;
      this.u = this.f_().d;
      this.bW.a(this.f_().c);
      this.notBx.a(this.f_().d);
      this.bY.a(this.m());
      this.az.a(Math.round(c.a() - (float) this.az.w()));
      this.az.b(Math.round(b.a() - (float) this.az.w()));
      this.aD = 0.0F;
      float var1 = this.m();
      this.g(this.aX() ? 0.0F : 180.0F);
      this.b(false);
      this.bj();
      if (this.aB) {
         this.aE = 0.0F;
         this.aD = 180.0F;
         this.bj();
      }

      this.g(var1);
      if (this.az.w() == 0 && this.af()) {
         this.aT.F();
      }

      if (this.getGame().S()) {
         rT var2 = Ja.R().m().Q().c();
         if (var2.equals(rT.a)) {
            this.n(0.6F);
         } else if (var2.equals(rT.b)) {
            this.n(0.8F);
         }
      }

   }

   public boolean[] V() {
      return this.V;
   }

   public void a(boolean[] var1) {
      int var2;
      for (var2 = 0; var2 < 5; ++var2) {
         if (this.V[var2] != var1[var2]) {
            this.bQ = true;
         }
      }

      for (var2 = 0; var2 < this.V.length; ++var2) {
         this.V[var2] = var1[var2];
      }

      this.g = this.V[Am.a.ordinal()];
      this.h = this.V[Am.b.ordinal()];
      this.i = this.V[Am.c.ordinal()];
      this.j = this.V[Am.d.ordinal()];
      this.l = this.V[Am.e.ordinal()];
      if ((this.getGame().J().u().ak().d() & 1) != 0) {
         this.l = false;
      }

      this.m = this.V[Am.f.ordinal()];
      if ((this.getGame().J().u().ak().d() & 2) != 0) {
         this.m = false;
      }

      boolean var3 = this.n;
      this.n = this.V[Am.g.ordinal()];
      if (this.n && !var3) {
         this.W = true;
      }

   }

   public void a(bE var1) {
      this.X[0] = var1;
   }

   public void b(bE var1) {
      this.X[1] = var1;
   }

   public void c(bE var1) {
      this.X[2] = var1;
   }

   private void d(bE var1) {
      this.X[3] = var1;
   }

   public bE[] W() {
      return this.X;
   }

   public bE X() {
      return this.X[0];
   }

   public bE Y() {
      return this.X[1];
   }

   public bE Z() {
      return this.X[2];
   }

   public bE aa() {
      return this.X[3];
   }

   public void a(SR var1) {
      float var2;
      if (var1.f(this)) {
         if (this.getGame().F()) {
            var2 = var1.f_().c;
            float var3 = var1.f_().d;
            Hf var4 = new Hf(sx.d(1.3F), this.aT.G().a(KN.t), 33, ip.f, "Defused!", (mN) null, var2, var3, var2, var3 + 40.0F);
            this.getGame().R().b(var4);
            kr var5 = this.getGame().G().b("map/mode/bomb_defused.wav");
            DA var6 = this.getGame().ah().a(var5, var1.f_(), dc.a);
            var6.l();
         }

         if (this.getGame().B()) {
            Er var10 = new Er(this.aT, this, var1, 10);
            this.aT.u().c().l().a((kY) var10);

            try {
               da var12 = JL.a("powerupDefuse");
               var12.a("player", this.aS());
               var12.a("powerup", (Object) LX.a(var1.getClass()).h);
               var12.a("xp", 10);
               JL.a(var12, "positionX", var1.f_().c);
               JL.a(var12, "positionY", var1.f_().d);
               JL.a(var12, "velocityX", var1.b().c);
               JL.a(var12, "velocityY", var1.b().d);
               this.getGame().u().c().a(var12);
            } catch (Exception var9) {
               a.error(var9, var9);
            }
         }
      } else {
         da var11;
         if (var1.x()) {
            var1.a(this).f();
            if (this.getGame().B()) {
               try {
                  var11 = JL.a("powerupAutoUse");
                  var11.a("player", this.aS());
                  var11.a("powerup", (Object) LX.a(var1.getClass()).h);
                  JL.a(var11, "positionX", var1.f_().c);
                  JL.a(var11, "positionY", var1.f_().d);
                  JL.a(var11, "playerVelX", this.b().c);
                  JL.a(var11, "playerVelY", this.b().d);
                  JL.a(var11, "playerAngle", this.m());
                  JL.a(var11, "velocityX", var1.b().c);
                  JL.a(var11, "velocityY", var1.b().d);
                  this.getGame().u().c().a(var11);
               } catch (Exception var8) {
                  a.error(var8, var8);
               }
            }
         } else if (this.aO()) {
            if (this.getGame().B()) {
               try {
                  var11 = JL.a("powerupPickup");
                  var11.a("player", this.aS());
                  var11.a("powerup", (Object) LX.a(var1.getClass()).h);
                  JL.a(var11, "positionX", var1.f_().c);
                  JL.a(var11, "positionY", var1.f_().d);
                  JL.a(var11, "playerVelX", this.b().c);
                  JL.a(var11, "playerVelY", this.b().d);
                  JL.a(var11, "playerAngle", this.m());
                  JL.a(var11, "velocityX", var1.b().c);
                  JL.a(var11, "velocityY", var1.b().d);
                  this.getGame().u().c().a(var11);
               } catch (Exception var7) {
                  a.error(var7, var7);
               }
            }

            this.b(var1);
            if (this.af() && this.aT.F()) {
               var2 = 15.0F;
               Yr var13 = new Yr(-var2, this.aX() ? -var2 : var2);
               int var14 = DX.c(5, (eK) null);
               if (this.Z != null && !this.Z.c().e()) {
                  this.Z.c().j();
               }

               Kb var15 = this.aT.R();
               this.Z = new Nr(this.getGame(), this, var1, var13, var14);
               if (this.az.e()) {
                  this.Z.c().a(new qt((float) this.az.f()));
               }

               var15.b(this.Z);
            }
         }
      }

      var1.b(this);
      var1.M().d();
      var1.a();
   }

   public SR ab() {
      return this.Y;
   }

   public Yr ac() {
      return this.K;
   }

   public void r() {
      super.r();
      this.z();
   }

   private void z() {
      this.Q.a(this.b());
      this.R = this.h();
   }

   private void A() {
      this.e(this.b().c - this.Q.c, this.b().d - this.Q.d);
      this.r(this.h() - this.R);
      this.b().a(this.Q);
      this.a_(this.R);
      this.S = 0;
   }

   private void B() {
      float var1 = Math.abs(this.bM);
      float var2 = Math.abs(this.bN);
      float var3 = Math.abs(this.bL);
      float var4 = Math.max(var1, var2);
      float var5 = Math.max(var4, var3);
      ke var6 = this.aT.u().a();
      float var7 = (float) (var6 == null ? 0 : var6.a() + var6.b());
      this.bK = 8.0F + sx.a(var7);
      if (var5 > 90.0F) {
         this.bK = 1.0F;
         this.C();
      } else {
         this.bM /= this.bK;
         this.bN /= this.bK;
         this.bL /= this.bK;
      }

   }

   private void C() {
      if (this.bK > 0.0F) {
         --this.bK;
         this.g(this.m() + this.bL);
         Yr var10000 = this.f_();
         var10000.c += this.bM;
         var10000 = this.f_();
         var10000.d += this.bN;
         this.u += this.bN;
         if (this.bK <= 0.0F) {
            this.bL = 0.0F;
            this.bM = 0.0F;
            this.bN = 0.0F;
         }
      }

   }

   public void e_() {
      if (this.bv != null) {
         this.bv.b();
      }

      if (this.bt != null) {
         this.bt.d();
      }

      float var1;
      float var2;
      if (this.aO() && this.az.q()) {
         var1 = 67.0F / (float) uV.y;
         var2 = var1 * ((mN) this.az.s()).aH();
         boolean var3 = var2 >= this.ae;
         float var4 = var1 / 1.33F;
         Object var5 = var3 ? this.az.s() : this;
         this.a((Mb) var5, var4, this.m());
      }

      this.A();
      this.az.y();
      if (this.az.j() == 0 && this.af()) {
         Eb var6 = this.az.l();
         if (var6.aU().equals(this.aU())) {
            this.getGame().ab().b(new Ze(this.getGame(), this, var6));
         } else {
            JW var8 = new JW(this.getGame());
            var8.a(this, var6);
            this.getGame().ab().a((kY) (new oa(this.getGame(), var8)));
         }
      }

      if (this.az.f() == 0) {
         this.D();
         var1 = dh.f(this.f);
         var2 = this.m();
         this.aC();
         this.aB();
         this.b().a(var1 * dh.b(var2), var1 * dh.c(var2));
      }

      boolean var7 = this.az.e() && this.aO();
      if (!var7) {
         if (!this.af()) {
            this.C();
         }

         this.aE();
         if (this.U) {
            this.U = false;
         } else {
            super.e_();
         }
      }

      float var10;
      if (this.au) {
         for (int var9 = 0; var9 < this.V.length; ++var9) {
            this.V[var9] = false;
         }

         this.a(this.V);
         this.A = true;
      } else if (this.af > 0.0F) {
         var2 = (float) this.ad / o;
         var10 = dh.c(this.af, var2);
         this.af -= var10;
         this.ae = dh.a(this.ae + var10, 0.0F, (float) this.ad);
      }

      if (!var7) {
         this.bj();
      }

      this.F();
      if (this.af() && this.getGame().D()) {
         this.getGame().u().f().b(this);
      }

      if (!var7) {
         this.G();
      }

      this.E();
      --this.bO;
      --this.bP;
      ++this.bR;
      this.bW.a(this.f_().c);
      this.notBx.a(this.f_().d);
      this.bY.a(this.m());
      ++this.bU;
      if (this.getGame().F() && !this.au) {
         this.ad();
      }

      if (this.getGame().B() && this.au) {
         this.bx.b();
      }

      if (this.au && this.aT.K() == 0.0F && this.b().e() < 4.0F) {
         if (this.b().d() < 0.01F) {
            var2 = dh.a(0.0F, 360.0F);
            var10 = 2.0F;
            this.b().a(dh.b(var2) * var10, dh.c(var2) * var10);
         } else {
            this.b().b(2.0F / this.b().d());
         }
      }

      if (this.aQ()) {
         this.b().f();
      }

      if (this.getGame().Y() % 30 == 0 && this.Y instanceof Jg) {
         this.aT().g().n.a(1);
      }

   }

   private void D() {
      if (this.aT.B()) {
         fN var1 = this.aT.u().c();
         Jt var2 = var1.l().a(this.aS());
         if (var2 != null && !var2.a(this.R())) {
            var1.l().a(var2, "You must buy the full game to use that plane configuration", "connection lost");
         }
      }

   }

   private void E() {
      if (this.av()) {
         this.aQ.c = this.I() * this.aP.c;
         this.aQ.d = this.I() * this.aP.d * dh.b(this.aD);
      }

      if (this.q != null) {
         this.q.a.f(this.aD);
      }

      if (this.aw()) {
         this.aQ.c = this.I() * this.aP.c;
         this.aQ.d = this.I() * this.aP.d * dh.b(this.aD);
         this.r.a.f(this.aD);
      }

      if (this.getGame().F()) {
         this.aS.c = this.I() * this.aR.c;
         this.aS.d = this.I() * this.aR.d * dh.b(this.aD);
      }

   }

   public void a(int var1, int var2, int var3) {
      this.az.a(var1, var2);
      float var4 = (float) var3 * this.an / 100.0F;
      if (this instanceof nk) {
         var4 *= 0.8F;
      }

      if (this.a(RR.an)) {
         var4 *= 0.8F;
      }

      this.o(-var4);
      if (this.aT.F()) {
         var1 = this.az.p();
         if (this.bd != null && this.bd.q()) {
            this.bd.c().c((float) var1);
         } else {
            this.bd = new Ax(this.aT, this, new qt((float) var1));
            this.aT.R().b(this.bd);
         }
      }

   }

   public void a(int var1, Mb var2) {
      this.az.a(var1, var2);
      if (this.aT.F()) {
         var1 = this.az.r();
         if (this.be != null && this.be.q()) {
            this.be.c().c((float) var1);
         } else {
            this.be = new PG(this.aT, this, new qt((float) var1));
            this.aT.R().b(this.be);
         }
      }

   }

   public void ad() {
      if (this.aX != null) {
         float var1;
         if (!this.A && !this.az.e()) {
            this.aX.a(this.f_());
            this.aX.b(this.b());
            var1 = this.ap();
            float var2 = 0.37F + 0.63F * var1;
            float var3 = this.aX.s();
            float var4 = dh.a(var2 - var3, -0.03F, 0.03F);
            this.aX.b(var3 + var4);
            if (this.aX.r() < 1.0F) {
               this.aX.a(dh.c(this.aX.r() + 0.03F, 1.0F));
            }

            if (!this.aX.g()) {
               this.aX.l();
            }
         } else {
            var1 = 0.37F;
            if (this.aX.s() > var1) {
               this.aX.b(dh.d(this.aX.s() - 0.03F, var1));
            }

            if ((double) this.aX.r() > 0.2D) {
               this.aX.a(dh.d(this.aX.r() - 0.03F, 0.0F));
            } else {
               this.aX.m();
            }
         }
      }

      if (this.aZ != null) {
         if (this.bc > 0.0F) {
            if (this.bc < 1.0F) {
               this.aZ.a(0.7F);
            } else {
               this.aZ.a(1.0F);
            }

            this.aZ.a(this.f_());
            this.aZ.b(this.b());
            if (!this.aZ.g()) {
               this.aZ.l();
            }
         } else {
            this.aZ.m();
         }
      }

      if (this.az.i()) {
         if (this.bb == null) {
            kr var5 = this.getGame().G().b("map/mode/bomb_defusing.wav");
            this.bb = this.getGame().ah().a(var5, this.f_(), dc.a);
            this.bb.b(true);
         }

         this.bb.a(this.f_());
         this.bb.b(this.b());
         this.bb.a(true);
      } else if (this.bb != null) {
         this.bb.a(false);
      }

   }

   private void F() {
      float var1 = this.f_().e();
      float var2 = this.b().e();
      if (dh.h(var1) || dh.h(var2) || (double) var2 > 1.0E7D) {
         a.error("quickstop " + this.getClass().getName() + " : invalid p=" + this.f_() + ", v=" + this.b());
         this.bg();
         this.f_().c = -1.0F;
         this.f_().d = -1.0F;
      }

      Dimension var3 = this.aT.R().f().c();
      if ((this.af() || this.au) && (this.f_().d < 0.0F || this.f_().d > (float) var3.height)) {
         float var4 = this.f_().d < 0.0F ? 90.0F : 270.0F;
         if (this.au) {
            this.aF();
         } else {
            this.a((Mb) null, (float) this.ad, var4);
         }
      }

      this.f_().c = dh.b(this.f_().c, 0.0F, (float) var3.width);
   }

   private void G() {
      this.o(this.as());

      for (bE var2 : this.X) {
         if (var2 != null) {
            var2.c();
         }
      }

      if (this.af()) {
         if (this.l && this.X().b() && this.aO()) {
            this.aT.ab().a((kY) (new fJ(this.aT, this, 0)));
         }

         if (this.m && this.Y().b() && this.aO()) {
            this.aT.ab().a((kY) (new fJ(this.aT, this, 1)));
         }

         if (this.Y != null && this.n && this.W && this.aO()) {
            this.W = false;
            this.aT.ab().a((kY) (new fJ(this.aT, this, 3)));
         }
      }

      if (this.Y != null && !this.aa().b()) {
         this.ae();
         if (this.Z != null) {
            this.Z.c().j();
         }
      }

   }

   public void a(SR var1, boolean var2) {
      float var3;
      if (var1 instanceof tc) {
         this.al = var2 ? this.ah : 1.0F;
         var3 = this.v * (1.0F - this.ak);
         this.w += var2 ? -var3 : var3;
         this.bG = var2;
         if (this.aT.F()) {
            if (var2) {
               this.q = new HF(this);
               if (!this.af()) {
                  this.bH = new Kq(this.getGame(), this, var1);
                  this.getGame().R().b(this.bH);
               }
            } else {
               this.q = null;
            }
         }
      } else if (var1 instanceof Jg) {
         this.al = var2 ? this.ah : 1.0F;
         var3 = this.v * (1.0F - this.aj);
         this.w += var2 ? -var3 : var3;
         this.bG = var2;
      } else if (var1 instanceof vS) {
         this.r = new HF(this);
         if (this.aT.F() && !this.af() && this.getGame().T().i().size() == 2 && this.aU().equals(this.aT.J().u().ad()) && var2) {
            this.bH = new Kq(this.getGame(), this, var1);
            this.getGame().R().b(this.bH);
         }
      }

   }

   private void b(SR var1) {
      this.ae();
      this.Y = var1;
      var1.a(0.0F, 0.0F);
      this.d(var1.a(this));
      this.a(var1, true);
      this.az.g(0);
   }

   public void ae() {
      if (this.Y != null) {
         this.a(this.Y, false);
         this.r = null;
         this.Y = null;
         this.d((bE) null);
      }

   }

   public boolean af() {
      return !this.au && this.aT.b(this.aS());
   }

   public String ag() {
      return this.aa;
   }

   public boolean ah() {
      return this.E > 0.97F;
   }

   public boolean ai() {
      return this.A;
   }

   public float aj() {
      return this.x;
   }

   public void a(boolean var1) {
      this.A = var1;
   }

   public float ak() {
      return (float) this.ad;
   }

   public float al() {
      return this.ae;
   }

   public float am() {
      return this.af;
   }

   public float an() {
      return this.ae / (float) this.ad;
   }

   public void a(float var1, boolean var2) {
      if (var2) {
         this.ae = dh.a(this.ae + var1, 0.0F, (float) this.ad);
      } else {
         this.af = dh.c(this.af + var1, (float) this.ad - this.ae);
      }

   }

   public void n(float var1) {
      this.G = var1 * 1000.0F;
   }

   public float ao() {
      return this.I;
   }

   public float ap() {
      return this.G / 1000.0F;
   }

   public float aq() {
      return this.ao;
   }

   public float ar() {
      return this.ap;
   }

   public float as() {
      return this.ap * this.getGame().J().u().ak().y();
   }

   public float at() {
      return this.an;
   }

   public void o(float var1) {
      this.ao = dh.a(this.ao + var1, 0.0F, this.an);
   }

   public void d(float var1, float var2) {
      this.f_().a(var1, var2);
      this.u = var2;
   }

   public int u() {
      return DX.a(this.af(), this.ba().c());
   }

   public int b(float var1) {
      return Math.round(this.f_().c + (this.b().c + this.bM) * var1);
   }

   public int c(float var1) {
      return Math.round(this.f_().d + (this.b().d + this.bN) * var1);
   }

   public boolean c() {
      return this.aO();
   }

   public String s_() {
      return this.aT().c();
   }

   public int d(float var1) {
      return Math.round(this.m() + this.bL * var1);
   }

   public void a(float var1) {
      int var2 = this.b(var1);
      int var3 = this.c(var1);
      int var4 = this.d(var1);
      qd.k();
      qd.b((float) var2, (float) var3);
      qd.c((float) var4);
      this.c(this.au(), this.aD);
      qd.l();
      if (dp.a) {
         super.a(var1);
      }

   }

   public void b(ip var1, float var2) {
      float var3 = this.bu;
      if (var3 != 1.0F) {
         qd.d(1.0F / var3);
      }

      this.a(var1, var2);
   }

   public void a(ip var1, float var2) {
      this.c(var1, dh.b(var2, -180.0F, 180.0F));
   }

   public ip au() {
      return this.bA;
   }

   public void a(ip var1) {
      this.bA = var1;
   }

   public void c(ip var1, float var2) {
      qd.b(var1);
      int var3 = this.u(var2);
      PY var4 = this.aF[var3];
      var4.d();
      if (this.aG != null) {
         PY var5 = this.aG[var3];
         if (var5.a() != null) {
            var5.d();
         }
      }

   }

   public boolean av() {
      return this.bG;
   }

   public boolean aw() {
      return this.r != null;
   }

   public Yr ax() {
      return this.aQ;
   }

   public Yr ay() {
      return this.aS;
   }

   protected boolean az() {
      return this.aZ() < 0.0F;
   }

   public void a(NG var1, vX var2) {
      if (this.getGame().F()) {
         if (this.f().a(var1)) {
            var2.a(this, this.u());
            n var3 = this.getGame().J().u().ab().k();
            if (var3 == this && this.aO() && (dp.d || this.af())) {
               if (this.bD == null) {
                  this.bD = new OP(this);
               }
            } else {
               this.bD = null;
            }

            boolean var4 = this.bD != null;
            int var5 = var4 ? bC : DX.c(12, this.aT.G().a(KN.f).b());
            var2.a(this.bE, var5);
            var2.a(this.bF, var4 ? bC : DX.c(12, (eK) null));
            if (var4 && !dp.b) {
               var2.a(this.bD, bC);
            }

            boolean var6;
            eK var7;
            int var8;
            if (this.q != null) {
               var6 = this.az();
               var7 = this.q.a.B().c();
               var8 = var6 ? DX.b(12, var7) : DX.c(1, var7);
               var2.a(this.q, var8);
            }

            if (this.aw()) {
               var6 = this.az();
               var7 = this.r.a.B().c();
               var8 = var6 ? DX.b(12, var7) : DX.c(1, var7);
               var2.a(this.r, var8);
            }

            if (this.e.d()) {
               var2.a(this.e, DX.c(10, (eK) null));
            }
         }

         if (this.bt != null && this.bt.e() && this.aO()) {
            var2.a(this.bt, DX.c(11, (eK) null));
         }
      }

   }

   public float aA() {
      return this.aJ;
   }

   public void p(float var1) {
      int var2 = this.b(var1);
      int var3 = this.c(var1);
      NY var4 = this.aT.G().a(KN.f);
      float var5 = this.aJ;
      int var7 = Math.round((float) var3 + var5);
      String var8 = this.aT().c();
      boolean var10000 = dp.a;
      if (this.bD == null && !dp.c) {
         dp var9 = this.getGame().J().u();
         ip var10 = var9.g().b();
         if (this.au) {
            qd.a(var10.q * 0.6F, var10.r * 0.6F, var10.s * 0.6F, this.bA.t);
         } else {
            qd.a(var10, this.bA.t);
         }

         if (var9.ab().h()) {
            float var11 = var9.ak().e();
            Yr var12 = var9.ab().e();
            int var13 = Math.round((float) (var2 - (int) var12.c) / var11);
            int var14 = Math.round((float) (var7 - (int) var12.d) / var11);
            qd.k();
            qd.b((float) ((int) var12.c), (float) ((int) var12.d));
            qd.d(var11);
            var4.a(var8, var13, var14, 33);
            qd.l();
         } else {
            var4.a(var8, var2, var7, 33);
         }
      }

      float var19;
      int var20;
      float var34;
      if (this.az.c()) {
         var19 = 1.0F;
         if ((float) this.az.d() < t) {
            var19 = (float) this.az.d() / t;
         }

         qd.a(1.0F, 1.0F, 1.0F, var19);
         var20 = this.az.w() / 2 % 8;
         List var24 = this.aT.G().f("effects/invuln/invuln.animatedpoly").a();
         PY var29 = ((xz) var24.get(var20)).b().e();
         var34 = this.aJ / 25.0F;
         qd.k();
         qd.b((float) var2, (float) var3);
         qd.c((float) this.d(var1));
         qd.d(var34);
         var29.d();
         qd.l();
      }

      if (!dp.c && this.aO() && this.ab() != null && (this.bH == null || !this.bH.q()) && !this.af() && this.getGame().T().i().size() == 2 && !(this.ab() instanceof Jg)) {
         oD var21 = this.getGame().J().u().ad();
         if (dp.d) {
            n var22 = this.getGame().J().u().ab().k();
            if (var22 instanceof mN) {
               var21 = ((mN) var22).aU();
            }
         }

         boolean var23 = (var21.equals(oD.c) || var21.equals(this.aU())) && this.bD == null;
         if (var23) {
            qd.b(ip.a);
            qd.k();
            int var26 = var2 + var4.a((CharSequence) var8) / 2 + 4;
            int var31 = var7 - 2;
            qd.b((float) var26, (float) var31);
            this.getGame().G().a("hud/over_plane/powerup_container.png").a();
            qd.b(10.0F, 10.0F);
            var34 = 9.0F / this.ab().z();
            qd.d(var34);
            this.ab().e(var1);
            qd.l();
         }
      }

      float var33;
      if (this.az.n()) {
         var19 = this.az.b(var1);
         float var25 = 1.0F;
         if (var19 < 0.5F) {
            var25 = var19 / 0.5F;
            var25 *= var25;
         }

         qd.a(this.au(), var25);
         em var28 = RR.b(this.getGame().G(), this.R().k());
         qd.k();
         qd.b((float) var2, (float) var3);
         var33 = 0.5F;
         var34 = -11.0F;
         float var36 = this.aY() ? 4.0F : -4.0F;
         float var15 = dh.b(this.m());
         float var16 = dh.c(this.m());
         float var17 = var34 * var15 - var36 * var16 - var33 * (float) var28.c() / 2.0F;
         float var18 = var34 * var16 + var36 * var15 - var33 * (float) var28.d() / 2.0F;
         qd.b(var17, var18);
         qd.d(var33);
         var28.a();
         qd.l();
      }

      if (this.az.u()) {
         em var27 = this.getGame().G().a("planes/loopy/crosshair.png");
         em var32 = this.getGame().G().a("planes/bomber/crosshair_locked.png");
         var33 = this.aA();
         qd.k();
         qd.b((float) var2, (float) var3);
         qd.d(2.0F * var33 / (float) (var27.c() - 10));
         qd.b(ip.a);
         qd.b((float) (-var32.c() / 2), (float) (-var32.d() / 2));
         var32.a();
         qd.l();
      }

      if (this.az.i()) {
         var19 = this.az.a(var1);
         qd.k();
         var20 = 40;
         if (!this.af() && this.aU().equals(this.aT.J().u().ad())) {
            var20 += 22;
         }

         qd.b((float) var2 - 20.0F, (float) var3 + var5 + (float) var20);
         Eb.a(this.aT.G(), var19, ip.f);
         qd.l();
         NY var30 = this.aT.G().a(KN.r);
         Eb var35 = this.az.l();
         String var37;
         if (var35.aU().equals(this.aU())) {
            var37 = "Defusing";
         } else {
            var37 = "Planting";
         }

         var20 += 8;
         var30.a(var37, var2, Math.round((float) var3 + var5 + (float) var20), 33);
      }

   }

   private void a(int var1, int var2) {
      dp var3 = this.getGame().J().u();
      if (var3.ab().h()) {
         float var4 = var3.ak().e();
         Yr var5 = var3.ab().e();
         int var6 = Math.round((float) (var1 - (int) var5.c) / var4);
         int var7 = Math.round((float) (var2 - (int) var5.d) / var4);
         qd.b((float) ((int) var5.c), (float) ((int) var5.d));
         qd.d(var4);
         qd.b((float) var6, (float) var7);
         qd.d(1.0F / var4);
      } else {
         qd.b((float) var1, (float) var2);
      }

   }

   public void q(float var1) {
      if (this.bn != 0 && !dp.c) {
         int var2 = this.b(var1);
         int var3 = this.c(var1) + Math.round(this.aJ);
         NY var4 = this.aT.G().a(KN.f);
         String var5 = this.aT().c();
         int var6 = var4.a((CharSequence) var5);
         qd.b(this.au());
         int var7 = this.bn < 4 ? 0 : (this.bn < 7 ? 1 : 2);
         em var8 = this.getGame().G().a(bB[var7]);

         int var9;
         for (var9 = this.bn; var9 > 3; var9 -= 3) {
            ;
         }

         qd.k();
         int var10;
         int var11;
         if (this.bD == null) {
            var10 = var2 - var6 / 2 - 19;
            var11 = var3 - 2;
         } else {
            var10 = var2 - var8.c() - 20 - 4;
            var11 = var3 + 5;
         }

         this.a(var10, var11);

         for (int var12 = 0; var12 < var9; ++var12) {
            var8.a();
            qd.b(0.0F, 6.0F);
         }

         qd.l();
      }
   }

   public void r(float var1) {
      this.M += var1;
   }

   public void aB() {
      this.N.f();
      this.K.f();
   }

   public void aC() {
      this.M = 0.0F;
      this.O = 0.0F;
      this.B = 0.0F;
      this.P = 0.0F;
      this.a_(0.0F);
   }

   public void e(float x, float y) {
      if (!this.aQ()) {
         if (!this.A) {
            float var3 = dh.f(this.f);
            float var4 = dh.b(this.m());
            float var5 = dh.c(this.m());
            float var6 = x * var4 + y * var5;
            float var7 = var3 + var6;
            if (var7 > 0.0F) {
               this.f = var7 * var7;
               x -= var4 * var6;
               y -= var5 * var6;
            } else {
               this.f = 0.0F;
               x += var4 * var3;
               y += var5 * var3;
            }
         }
         this.K.c += x;
         this.K.d += y;
      }
   }

   public int o() {
      return qf.a;
   }

   public int p() {
      return this.getGame().B() ? 0 : qf.c;
   }

   private boolean a(uM var1) {
      cV var2 = this.getGame().T();
      if (this.au) {
         if (var2.a((Mb) var1.G(), (Mb) this)) {
            return true;
         }
      } else {
         if (!var2.a((Mb) var1.G(), (Sp) this)) {
            return true;
         }

         if (!this.af() && var1.F().i() < uM.b.a()) {
            return true;
         }
      }

      return false;
   }

   public void a(Yl var1) {
      qW var2 = (qW) var1;
      vk var3 = b(this, var2);
      if (var3 != null) {
         Yr var4 = this.f_();
         boolean var5 = var2 instanceof uM;
         if (var5 && this.a((uM) var2)) {
            return;
         }

         if (this.az.e() && this.aC == var2) {
            return;
         }

         if (++this.S > 1) {
            this.af();
         }

         Yr var6 = this.b().e(var2.b());
         Yr var7 = new Yr(this.b());
         float var8 = this.h();
         float var9 = var3.d;
         JZ var10 = var3.a();
         JZ var11 = var3.b();
         float var12 = dh.a((var10.c() + var11.c()) * 0.5F, 0.0F, 1.0F);
         float var13 = dh.a((var10.e() + var11.e()) * 0.5F, 0.0F, 1.0F);
         float var14 = this.getGame().J().u().ak().w();
         if (this.a(RR.an)) {
            var14 = Math.min(var14 * 3.5F, 3.5F);
         }

         if (var5) {
            var14 = 1.0F;
         }

         float var15 = dh.a(var11.b() * var14, 0.0F, 1.0F);
         var3.a(var12, var13, var15);
         this.T = var3;
         Yr var16 = new Yr(this.b());
         float var17 = this.h();
         if (var9 > 0.0F) {
            this.U = true;
            float var18 = var3.d;
            float var19 = 1.0F - var3.d;
            var4.c += var7.c * var18 + var16.c * var19;
            var4.d += var7.d * var18 + var16.d * var19;
            this.g(this.m() + var8 * var18 + var17 * var19);
         }

         this.a(var2, var6, var3, var5);
         if (this.bP < 0) {
            this.bP = 6;
            this.aD();
         }
      }

   }

   public void aD() {
      this.bO = 0;
   }

   private void H() {
      if (this.au) {
         this.bh();
      } else {
         float var1 = this.C;
         float var2 = this.D;
         float var3;
         if (this.az.o()) {
            var3 = 1.0F;
            if (var2 > 3.385F) {
               var3 = (3.385F + (var2 - 3.385F) * 0.33F) / var2;
               if (this instanceof nk) {
                  var3 = 0.2F + 0.8F * var3;
               }

               if (this.a(RR.an)) {
                  var3 = 0.2F + 0.8F * var3;
               }
            }

            var1 *= var3;
            var2 *= var3;
         }

         if (!this.g || !this.h) {
            if (this.g) {
               this.B = dh.a(this.B + var1, var1, var2);
            } else if (this.h) {
               this.B = dh.a(this.B - var1, -var2, -var1);
            } else {
               this.B = 0.0F;
            }
         }

         this.E = Math.abs(this.B) / var2;
         var3 = this.h();
         var3 += this.B - this.P;
         var3 += this.M - this.O;
         this.P = this.B;
         this.O = this.M;
         this.a_(var3);
      }
   }

   public void aE() {
      Yr var1 = this.b();
      var1.d(this.N);
      this.H();
      int var2 = Math.round(this.m() + this.h());
      float var3 = dh.b((float) var2);
      float var4 = dh.c((float) var2);
      float var5 = this.j ? -this.I : (this.i ? this.H : 0.0F);
      if (this.az.o()) {
         var5 *= 0.5F;
      }

      this.e.a();
      this.bc = 0.0F;
      float var6;
      if (this.G == 1000.0F && var5 > 0.0F && !this.az.o()) {
         this.k = true;
         var6 = dh.a(this.aq(), 0.0F, this.aq);
         this.s(var6);
         if (this.bt != null) {
            this.bt.b();
         }
      } else {
         this.k = false;
      }

      float var7;
      float var8;
      if (this.a(RR.at) && var5 < 0.0F && (this.G == 0.0F || this.A)) {
         var6 = 0.8333F * this.ar();
         var7 = dh.a(this.aq(), 0.0F, var6);
         this.o(-var7);
         var8 = var7 / var6;
         if (this.az.o()) {
            var8 *= 0.4F;
         }

         this.bc = dh.d(0.01F, var8);
         this.w(var8);
      }

      this.G = dh.a(this.G + var5, 0.0F, 1000.0F);
      if (this.A) {
         this.G = 1000.0F;
      }

      var6 = this.G / 1000.0F;
      float var9;
      float var10;
      float var11;
      float var12;
      float var13;
      float var14;
      if (!this.A) {
         var7 = dh.f(this.f) * var6;
         var8 = var7 * var3;
         var9 = var7 * var4;
         var10 = var7 / this.w;
         var11 = 0.65F;
         if (this.E > this.F && var10 > var11) {
            var12 = 0.55F;
            var13 = (this.E - this.F) / (1.0F - this.F);
            if (var10 > 2.0F) {
               var10 = 2.0F;
            }

            var14 = -0.4814815F + 0.7407407F * var10;
            float var15 = var12 * var13 * var14;
            this.e((var1.c - var8) * var15, (var1.d - var9) * var15);
         }

         var1.a(var8, var9);
      }

      var1.c(this.K);
      this.N.a(this.K);
      var7 = var1.c * var3 + var1.d * var4;
      if (!this.A && (var7 < this.x || this.az.t())) {
         this.A = true;
         if (this.aY != null) {
            this.aY.a(this.f_());
            this.aY.b(this.b());
            this.aY.m();
            this.aY.l();
         }
      } else if (this.A && this.aO() && var7 * var6 > this.y && !this.az.t()) {
         this.A = false;
         this.f = var7 * var7;
         var7 *= var6;
         this.K.c = var1.c - var7 * var3;
         this.K.d = var1.d - var7 * var4;
         if (this.aY != null) {
            this.aY.m();
         }
      }

      if (this.E > this.F) {
         if (this.a(RR.aq)) {
            this.f = (float) ((double) this.f * 0.995D);
         } else {
            this.f = (float) ((double) this.f * 0.989D);
         }
      }

      var8 = this.aT.K();
      var9 = -2.0F * var8;
      var10 = this.f_().d;
      this.f += var9 * (this.u - var10);
      this.u = var10;
      this.f = Math.max(this.f, 0.1F);
      if (this.A) {
         var1.d += var8;
         if (!this.au) {
            var11 = var1.e();
            var12 = this.w;
            var13 = var12 * var12;
            if (var11 > var13) {
               var14 = 1.0F - (float) (0.0019000000320374966D * Math.pow((double) (var11 / var13), 2.5D));
               var1.b(var14);
            }
         }
      } else {
         var11 = this.w * this.w;
         if (this.f < var11 && var7 < this.w) {
            this.f += this.z;
         } else if (this.f > 1.005F * var11 || var7 > this.w) {
            var12 = 1.0F - (float) (0.0019000000320374966D * Math.pow((double) (this.f / var11), 2.5D));
            this.f = Math.max(this.f * var12 * var12, 0.1F);
         }
      }

      this.K.e();
      if (this.A) {
         this.N.f();
         this.K.f();
      } else if (this.K.e() < 0.01F) {
         this.K.f();
      } else {
         this.K.b(J);
      }

      if (Math.abs(this.M) < 0.1F) {
         this.M = 0.0F;
      } else {
         this.M *= L;
      }

   }

   protected void s(float var1) {
      this.o(-var1);
      float var2 = var1 / this.M();
      if (var2 >= 1.0F) {
         this.e.c();
      } else if (this.e.b().d(0.99F) < 0.26666F) {
         this.e.b().c(0.7F * this.e.b().c());
      }

      this.bc = dh.d(0.01F, var2);
      float var3 = this.N() * var2 * this.P();
      float var4 = dh.b(this.m());
      float var5 = dh.c(this.m());
      if (this.a(RR.an) || this.getGame().S() && this.aT().e().d().c() == 1 || this.aT.K() == 0.0F) {
         float var6 = dh.a(this.b().d(var4, var5), 4.24F, 2.756F + 0.35F * this.K(), 2.0F, 1.0F);
         var3 *= var6;
      }

      this.e(var3 * var4, var3 * var5);
   }

   private void w(float var1) {
      float var2 = dh.b(this.m());
      float var3 = dh.c(this.m());
      var1 = dh.a(this.b().d(var2, var3), -this.K(), -this.K() * 0.1F, 0.0F, 2.9F);
      if (this.aT.F() && this.aT.a(this.f_().c, this.f_().d, 40.0F)) {
         int var4 = 1 + Math.round(2.3F * var1);
         this.aT.R().b(new zo(this.getGame(), this.m(), 13.0F, this.f_(), this.b(), var4, ip.h));
      }

      float var5 = -0.44F * var1 * this.P();
      this.e(var5 * var2, var5 * var3);
   }

   public void aF() {
      this.b().f();
      this.bM = this.bN = this.bL = 0.0F;
      this.P = 0.0F;
      this.B = 0.0F;
      this.O = 0.0F;
      this.M = 0.0F;
      this.N.f();
      this.K.f();
      this.Q.f();
      this.R = 0.0F;
      this.av = true;
      if (this.be != null) {
         this.be.c().j();
      }

      if (this.bf != null) {
         this.bf.c().j();
      }

   }

   private void a(qW var1, Yr var2, vk var3, boolean var4) {
      float var5 = var2.f(var3.c);
      float var6 = var5 * -3.5F;
      float var7 = var5 / var2.d();
      if (var7 < -0.3F) {
         var6 *= 1.0F - var7;
      }

      if (var2.d() > this.w) {
         var6 *= 1.5F;
      }

      float var8 = var6;
      float var9 = 1.0F;
      if (!var4) {
         var9 *= this.getGame().J().u().ak().x();
         if (this.a(RR.an)) {
            var9 *= 0.1F;
         }
      }

      var9 = Math.max(var9, 0.1F);
      var6 *= var9;
      if (var1 instanceof ex) {
         var6 *= ((ex) var1).A();
      }

      if (this.au) {
         if (this.aw && this.aT.F() && this.aT.a(this.f_().c, this.f_().d, 110.0F)) {
            this.aT.R().b(new ky(this.aT, this.aT.R(), new Yr(this.f_()), 70.0F, 0));
         }

         this.aF();
      }

      boolean var10 = false;
      boolean var14;
      if (!this.au && var1 instanceof Eb) {
         this.az.d(30);
         Eb var11 = (Eb) var1;
         if (var11.a(this) && var11.a(this.m(), var3, var2)) {
            var10 = true;
            if (!this.i && !this.az.g()) {
               if (this.A) {
                  this.b().b(0.95F);
               } else {
                  this.f *= 0.9025F;
               }

               if (this.af() && this.aT.J().u().ac().a()) {
                  this.aT.J().u().ac().d();
               }

               float var12 = dh.a(var11.C());
               float var13 = dh.b(var12 - this.m(), -180.0F, 180.0F);
               if (Math.abs(var13) > 90.0F) {
                  var13 = dh.b(var13 + 180.0F, -180.0F, 180.0F);
               }

               if (Math.abs(var13) > 1.0F) {
                  this.g(this.m() + (var13 > 0.0F ? 1.0F : -1.0F));
               }

               if (Math.abs(var13) < 2.0F && this.b().d() < 1.0F) {
                  this.aC = var11;
                  this.bg();
                  if (this.getGame().T() instanceof bX) {
                     var14 = this.aU().equals(var11.aU());
                     int var15;
                     if (var14 && var11.G().size() == 0) {
                        var15 = Math.round(30.0F);
                        this.az.b(var15);
                        this.az.c(var15 + 30);
                     } else {
                        var15 = var14 ? bX.b : bX.a;
                        this.az.b(var15);
                        this.az.a(var15, var15, var11);
                        this.az.c(var15 + 30);
                     }
                  } else {
                     byte var21 = 30;
                     this.az.b(var21);
                     this.az.c(var21 + 30);
                     this.ae = (float) this.ad;
                     this.a((SR) (new aaE(this.getGame(), 100, true)));
                     this.ay.a();
                  }
               }
            }
         }
      }

      if (!var10 && var6 > 0.001F) {
         mN var18 = var4 ? ((uM) var1).G() : this;
         var6 = dh.a(var6, 1.5F, 101.0F);
         int var19 = (int) var3.a.c;
         int var20 = (int) var3.a.d;
         if (this.af()) {
            int var22 = Math.round(var6 * (float) this.ad / 100.0F);
            float var23 = dh.a(var2);
            this.aT.ab().a((kY) (new IU(this.aT, var18, this, (float) var22, var23)));
            if (var4) {
               Sb var16 = new Sb(this.getGame(), var18.aS(), this, uM.c, 0);
               this.getGame().ab().a((kY) var16);
            }
         }

         var14 = this.aT.F() && this.aT.a(this.f_().c, this.f_().d, 40.0F) && (this.aO() || this.aw);
         if (var14 && var6 > 5.0F) {
            if (this.getGame().t().c().l() != Uu.c && var1 instanceof ex) {
               Kb var24 = this.aT.R();
               var24.b(new Ou(var24, this, (ex) var1, var2, var3, var6));
            }

            if (this.a(RR.an)) {
               this.aR().a(true);
            }
         }

         if (var14 && (var6 > 5.0F || var8 > 9.0F)) {
            kr var25 = this.getGame().G().b("planes/plane_hit_wall_direct.wav");
            DA var26 = this.getGame().ah().a(var25, this.f_(), this.b(), dc.c(this.aT, this));
            float var17 = (var8 + var6) / 2.0F;
            var26.a(dh.a(var17 / 80.0F, 0.25F, 1.0F));
            var26.l();
         }
      }

   }

   private void bg() {
      this.Q.f();
      this.f = this.w * this.w;
      this.b().f();
      this.aB();
      this.aC();
      this.G = 1000.0F;
      this.A = false;
      this.e.b().c(0.0F);
      this.e.a();
   }

   public sQ aG() {
      return this.bt;
   }

   public float aH() {
      return this.bo;
   }

   private float a(Mb var1, mN var2, float var3) {
      hp var4 = Ja.R().m();
      rT var5 = var4.Q().c();
      int var6 = BU.b.indexOf(var4.U());
      boolean var7 = var2 != null && var2.aT().a();
      boolean var8 = this.aT().a();
      if (var2 != null) {
         if (!var7 && var8) {
            if (var5.equals(rT.a)) {
               var3 *= 1.75F;
            } else if (var5.equals(rT.b)) {
               var3 *= 1.25F;
            }

            if (var6 == 0 || var6 == 1 && !var5.equals(rT.d)) {
               var3 *= 1.5F;
            }
         } else if (var7 && !var8) {
            if (var5.equals(rT.a)) {
               var3 *= 0.5F;
            } else if (var5.equals(rT.b)) {
               var3 *= 0.75F;
            }
         }
      } else if (var1 instanceof Ww && !var8) {
         if (var5.equals(rT.a)) {
            var3 *= 0.3F;
         } else if (var5.equals(rT.b)) {
            var3 *= 0.6F;
         }
      }

      return var3;
   }

   public void a(Mb var1, float var2, float var3) {
      mN var4 = var1 instanceof mN ? (mN) var1 : null;
      if (var4 != null && this != var4) {
         var2 *= var4.aH();
      }

      if (this.az.q()) {
         var2 *= 1.33F;
      }

      boolean var5 = this.a(RR.an) || this.aT.J().u().ak().v();
      if (var5 && var1 != this && var1 instanceof gJ) {
         var2 *= 1.07F;
      }

      if (this.getGame().S()) {
         var2 = this.a(var1, var4, var2);
      }

      if (this.ba != null && !this.au && var4 != null && this != var4 && var2 > 12.0F) {
         this.ba.a(this.f_());
         this.ba.l();
      }

      if (!this.au) {
         if (this.bt != null && var2 > 0.0F) {
            this.bt.c();
         }

         this.x(var2);
         float var6 = dh.a(var2, this.ae - (float) this.ad, this.ae);
         this.ae -= var6;
         if (this.aT.F() && var2 > 0.0F && !this.af()) {
            this.az.b();
         }

         this.aT().g().f.a(100.0F * var6 / this.ak());
         if (var2 > 0.0F && var4 != null && var4.aS() != this.aS() && var4.aS() != -1) {
            var4.aT().g().e.a(100.0F * var6 / this.ak());
         }

         if (var2 > 0.0F && var1 != this) {
            this.ay.a(var1, var6);
         }

         if (this.ae < 0.1F) {
            if (this.af()) {
               this.aT.ab().b(new Nu(this.aT, this, false));
               this.aJ();
            } else if (this.aT.B()) {
               bC var7 = new bC(this.aT, this, this.ay);
               VS var8 = this.aT.u().c().l();
               var8.a((kY) var7);
            }
         }

      }
   }

   public void a(int var1, mN var2) {
      xs var3 = this.aT().g();
      if (var1 == -1) {
         var3.c.a(1);
      }

      if (this.aT.B()) {
         var3.q.a(1);
      }

      var3.b.a(1);
      if (this.aT.B()) {
         Jt var4 = this.aT.u().c().l().a(this.aS());
         if (var4 != null) {
            var4.q();
         }
      }

      if (this.aT.a(this.aT()) && !this.getGame().S() && (var2 == null || var2.aT() == null || !var2.aT().a())) {
         this.aT.y().a(this.bc(), nd.b).a(1);
         this.getGame().b(Lu.i);
      }

      a(this.aT());
      this.aJ();
   }

   public void c(int var1) {
      this.e(var1);
      xs var2 = this.aT().g();
      if (this.aO()) {
         ++this.bk;
         ++this.bj;
         if (this.bj > (Integer) var2.i.a()) {
            var2.i.a(Integer.valueOf(this.bj));
         }
      }

      int var3 = this.getGame().Y();
      if (this.bg > 0 && (float) (var3 - this.bh) > bi) {
         this.bg = 0;
      }

      ++this.bg;
      this.bh = var3;
      if (this.bg > 1 && this.bg > (Integer) var2.j.a()) {
         var2.j.a(Integer.valueOf(this.bg));
      }

      if (this.bg > 1 && this.getGame().F()) {
         String var4;
         if (this.bg == 2) {
            var4 = "Double Kill";
         } else if (this.bg == 3) {
            var4 = "Triple Kill";
         } else if (this.bg == 4) {
            var4 = "Quadruple Kill";
         } else {
            var4 = "MEGA KILL x " + this.bg;
         }

         if (this.bI != null) {
            this.bI.c().j();
         }

         this.bI = new Hf(sx.d(1.3F), this.aT.G().a(KN.t), 33, ip.h, var4, this, 0.0F, 20.0F, 0.0F, 80.0F);
         this.getGame().R().b(this.bI);
      }

      if (this.bv != null) {
         this.bv.a(iD.c).a(1);
      }

   }

   public void d(int var1) {
      this.e(var1);
      if (this.bv != null) {
         this.bv.a(iD.a).a(1);
      }

   }

   public void a(mN var1) {
      var1.e(this.bm);
      var1.bj = this.bj;
      if (ty.a(var1.bp, this.bp)) {
         var1.bk = this.bk;
      }

      var1.bl = this.aR().w() + this.bl;
   }

   public void aI() {
      this.bm = 0;
   }

   public void e(int var1) {
      this.bm = dh.a(this.bm + var1, 0, 90);
      int var2 = 10;
      if (this.a(RR.au)) {
         var2 = Math.round((float) var2 * 0.8F);
      }

      float var7;
      for (int var3 = dh.a(this.bm / var2, 0, 9); this.bn < var3; this.bo += var7) {
         ++this.bn;
         float var4 = 1.01F;
         float var5 = 1.013F;
         float var6 = 0.01F * this.v;
         var7 = 0.02F;
         if (this.a(RR.au)) {
            var4 += 0.004717F;
            var5 += 0.00603F;
            var6 *= 1.4F;
            var7 *= 1.5F;
         }

         this.C *= var4;
         this.D *= var4;
         this.ap *= var5;
         this.w += var6;
      }

      if (this.bv != null) {
         this.bv.a(iD.b).a(Integer.valueOf(this.bn));
      }

   }

   public static void a(mF var0) {
      mN var1 = var0.i();
      if (var1 != null) {
         float var2 = (float) (var1.bl + var1.az.w()) / 30.0F;
         if ((float) (Integer) var0.g().h.a() < var2) {
            var0.g().h.a(Integer.valueOf(Math.round(var2)));
         }
      }

   }

   private void x(float var1) {
      if (this.aT.F()) {
         if (var1 > 0.0F && this.a(RR.ao)) {
            this.aR().a(true);
         }

      }
   }

   public void aJ() {
      this.a(true, true);
   }

   public void aK() {
      SR var1 = this.ab();
      if (var1 != null && this.aa().b()) {
         LX var2 = LX.a(var1.getClass());
         SR var3 = LX.a(this.aT, var2);
         var3.f_().a(this.f_());
         float var4 = 0.54F;
         float var5 = this.b().c * var4;
         float var6 = this.b().d * var4;
         var3.b().a(var5, var6);
         var3.d(true);
         if (ty.a(var2, (Object) LX.g)) {
            Jg var7 = (Jg) var3;
            var7.M().a(SR.e);
            var7.d(this);
            var7.a(false);
            var7.a((Jg) var1);
         } else {
            var3.M().a(SR.g);
         }

         if ((var2.equals(LX.e) || var2.equals(LX.f)) && this.getGame().T().i().size() == 2) {
            var3.a(var1.w());
         }

         this.aT.ab().a((kY) (new oa(this.aT, var3)));
      }

   }

   public void aL() {
      this.a(false, false);
      this.a(this.ak(), true);
      ip var1 = new ip(1.0F, 1.0F, 1.0F, 1.0F);
      if (this.getGame().Q()) {
         this.getGame().R().b(new MD(this, bJ, (Runnable) null, var1));
      }

      this.a(var1);
   }

   protected void a(boolean var1, boolean var2) {
      if (!this.au) {

         if (this.getGame().B()) {
            xs stats = this.aT().g();
            Map<String, Number> statMap = stats.getThisLife();

            da log = JL.a("despawn");
            log.a("player", this.aS());
            log.a("team", this.aU().c());
            log.a("plane", this.R().c().g);
            log.a("perkRed", this.R().i().b());
            log.a("perkGreen", this.R().k().b());
            log.a("perkBlue", this.R().l().b());
            log.a("skin", this.R().j().b());
            log.a("stats", statMap);
            this.getGame().u().c().a(log);

            stats.clearThisLife();
         }

         this.b().d(this.N);
         this.b().c(this.K);
         this.N.a(this.K);
         if (var2 && this.af()) {
            this.aK();
            float var3 = this.getGame().T().d();
            if (var3 > 0.0F) {
               aaE var4 = new aaE(this.aT, Math.round(var3), false);
               var4.f_().a(this.f_());
               var4.M().a(SR.g);
               this.aT.ab().a((kY) (new oa(this.aT, var4)));
            }
         }

         this.bG = false;
         this.q = null;
         this.r = null;
         if (this.getGame().B()) {
            this.bx = new oU(bw);
         }

         if (var1 && this.aT.F() && this.aT.a(this.f_().c, this.f_().d, 110.0F)) {
            this.aT.R().b(new ky(this.aT, this.aT.R(), new Yr(this.f_()), 70.0F, 0));
         }

         this.aw = var1;
         if (var1) {
            kr var5 = this.getGame().G().b("planes/plane_explode.wav");
            DA var7 = this.getGame().ah().a(var5, this.f_(), dc.d(this.aT, this));
            var7.l();
         }

         this.au = true;
         this.ae = 0.0F;
         this.a(bz);
         this.A = true;
         this.aD();
         this.az.a(-1, -1, (Eb) null);
         this.az.c(-1);
         this.az.b(-1);
         if (Math.abs(this.aE) < 10.0F && dh.a(3)) {
            boolean var6 = this.aE == 0.0F ? dh.a(2) : this.aE < 0.0F;
            this.aE = var6 ? -10.0F : 10.0F;
         }

         if (this.aX != null && this.aX.g()) {
            this.aX.m();
         }

         if (this.aY != null && this.aY.g()) {
            this.aY.m();
         }

         if (this.aZ != null && this.aZ.g()) {
            this.aZ.m();
         }

         if (this.ba != null && this.ba.g()) {
            this.ba.m();
         }

         if (this.bd != null) {
            this.bd.c().j();
         }

      }
   }

   public void h_() {
      if (this.aT.B()) {
         this.aT.u().c().l().a((kY) (new PJ(this.aT, this)));
      }

      if (this.aZ != null) {
         this.aZ.n();
      }

      if (this.aX != null) {
         this.aX.n();
      }

      if (this.aY != null) {
         this.aY.n();
      }

      if (this.ba != null) {
         this.ba.n();
      }

      if (this.bb != null) {
         this.bb.n();
      }

      this.ay.a();
      this.az.a(0, (Mb) null);
   }

   public boolean aM() {
      return this.k;
   }

   public boolean aN() {
      return this.aO() && !this.aR().c();
   }

   public boolean aO() {
      return !this.au;
   }

   public boolean aP() {
      return this.au;
   }

   public boolean aQ() {
      return this.av;
   }

   public UH aR() {
      return this.az;
   }

   public boolean q() {
      boolean var1 = !this.av;
      if (!var1 && this.getGame().B() && !this.bx.e()) {
         var1 = true;
      }

      return var1;
   }

   public JB d() {
      this.bS = new sU(this);
      return this.bS;
   }

   public int aS() {
      return this.as;
   }

   public mF aT() {
      return this.aT.J().u().c(this.aS());
   }

   public void f(int var1) {
      this.as = var1;
   }

   public oD aU() {
      return this.at;
   }

   public String aV() {
      return this.ac;
   }

   protected void a(Yr var1) {
      this.aP = var1;
      this.aQ.a(var1);
   }

   protected void b(Yr var1) {
      this.aR = var1;
      this.aS.a(var1);
   }

   protected void t(float var1) {
      this.aL = var1;
      this.aM = var1 / 6.0F;
      float var2 = 16.0F;
      float var3 = 180.0F - 2.0F * var1;
      float var4 = (float) ((int) Math.ceil((double) (var3 / var2)));
      this.aN = var3 / var4 - 0.1F;
   }

   public float aW() {
      return this.aL;
   }

   private void bh() {
      float var1 = this.h();
      if (var1 == 0.0F) {
         var1 = dh.a(-1.0F, 1.0F);
      }

      float var2 = 2.0F * var1 * 2.0F * var1;
      if (var2 < this.b().e()) {
         if (var1 > 0.0F) {
            var1 += 0.2F;
         } else {
            var1 -= 0.2F;
         }
      }

      this.a_(var1);
   }

   private void bi() {
      if (this.aE == 0.0F) {
         this.aE = dh.a(-1.0F, 1.0F);
      }

      float var1 = this.aE * this.aE;
      if (var1 < this.b().e()) {
         if (this.aE > 0.0F) {
            this.aE += 0.2F;
         } else {
            this.aE -= 0.2F;
         }
      }

      this.aD = dh.b(this.aD + this.aE, -180.0F, 180.0F);
   }

   private void bj() {
      if (this.au) {
         this.bi();
      } else {
         float var1 = this.m();
         boolean var2 = var1 > 150.0F || var1 < -150.0F;
         boolean var3 = -30.0F < var1 && var1 < 30.0F;
         if (!this.aB && !this.ai() && !this.ah() && (this.aA && var3 || !this.aA && var2)) {
            this.aB = true;
            this.aE = var2 ^ this.aD < 0.0F ? this.aN : -this.aN;
         }

         float var4;
         float var5;
         if (this.aB) {
            var4 = this.aL;
            var5 = dh.b(this.aD + this.aE, -180.0F, 180.0F);
            if (this.aA && -var4 < var5 && var5 < var4) {
               this.b(false);
            } else if (!this.aA && (var5 > 180.0F - var4 || var5 < -(180.0F - var4))) {
               this.b(true);
            }
         }

         if (!this.aB) {
            var4 = this.E * dh.g(this.B) + this.M * 0.5F;
            var5 = this.aD;
            if (this.aA) {
               var5 = this.aD < 0.0F ? this.aD + 180.0F : this.aD - 180.0F;
            }

            float var6 = this.aL * var4;
            this.aE = dh.a(var6 - var5, -this.aM, this.aM);
         }

         this.aD = dh.b(this.aD + this.aE, -180.0F, 180.0F);
         int var7 = this.u(this.aD);
         if (!this.aB && this.aI[var7] != null) {
            this.a(var7);
         }

      }
   }

   protected void b(boolean var1) {
      this.aA = var1;
      this.aB = false;
      this.aE = 0.0F;
   }

   public boolean aX() {
      float var1 = this.m();
      return -90.0F < var1 && var1 < 90.0F;
   }

   public boolean aY() {
      return this.aA;
   }

   public float aZ() {
      return this.aD;
   }

   public int u(float var1) {
      float var2 = 100000.0F;
      int var3 = -1;

      for (int var4 = 0; var4 < this.aH.length; ++var4) {
         float var5 = Math.abs(dh.b(this.aH[var4] - var1, -180.0F, 180.0F));
         if (var5 < var2) {
            var3 = var4;
            var2 = var5;
         }
      }

      return var3;
   }

   public PY g(int var1) {
      return this.aF[var1];
   }

   public PY ba() {
      int var1 = this.u(this.aD);
      return this.aF[var1];
   }

   public ip y() {
      return OP.d;
   }

   public void b(nu var1) {
      var1.a(this.g);
      var1.a(this.h);
      var1.a(this.i);
      var1.a(this.j);
      var1.a(this.k);
   }

   public void b(nQ var1) {
      this.g = var1.d();
      this.h = var1.d();
      this.i = var1.d();
      this.j = var1.d();
      this.k = var1.d();
   }

   public void a(nu var1, boolean var2) {
      if (var2) {
         var1.a(this.A);
         var1.a(0.0F, 1000.0F, 0.5F, this.G);
         var1.a(-180, 180, Math.round(this.m()));
         var1.a(-25.0F, 25.0F, 10.0F, this.B);
         var1.a(-25.0F, 25.0F, 10.0F, this.P);
         var1.a(-35.0F, 35.0F, 7.0F, this.h());
         boolean var3 = this.M != 0.0F || this.O != 0.0F;
         var1.a(var3);
         if (var3) {
            var1.a(-50.0F, 50.0F, 5.0F, this.M);
         }

         Vh.a((nu) var1, this.f_(), this.aT.R(), 50);
         var1.a(-25.0F, 25.0F, 10.0F, this.b().c);
         var1.a(-25.0F, 25.0F, 10.0F, this.b().d);
         boolean var4 = this.K.c != 0.0F || this.K.d != 0.0F;
         var1.a(var4);
         if (var4) {
            var1.a(-25.0F, 25.0F, 5.0F, this.K.c);
            var1.a(-25.0F, 25.0F, 5.0F, this.K.d);
         }

         var1.a(0.0F, 1000.0F, 10.0F, this.f);
         if (this.ae > (float) this.ad) {
            System.out.println("Plane.this=" + this);
            System.out.println("healthCurrent=" + this.ae);
            System.out.println("healthMax=" + this.ad);
         }

         var1.a(0.0F, 100.0F, 0.5F, 100.0F * this.ae / (float) this.ad);
         var1.a(0.0F, 1350.0F, 0.1F, this.ao);
         var1.a(this.au);
         var1.a(this.az.e());
         if (this.az.e()) {
            var1.a(0, 250, this.az.f());
            boolean var5 = this.az.j() > 0;
            var1.a(var5);
            if (var5) {
               var1.a(0, 500, this.az.k());
               var1.a(0, 500, this.az.j());
               int var6 = this.getGame().R().f().j().indexOf(this.az.l());
               var1.a(0, 250, var6);
            }

            boolean var7 = this.az.h() > 0;
            var1.a(var7);
            if (var7) {
               var1.a(0, 500, this.az.h());
            }
         }

      }
   }

   public float getLastX() {
      return this.last_x;
   }

   public float getLastY() {
      return this.last_y;
   }

   public float getLastAngle() {
      return this.last_angle;
   }

   public void a(nQ var1, boolean var2) {
      if (var2) {
         if (this.f() == null) {
            this.at = (oD) oD.m.get(0);
            this.a("render/planes/random/" + this.at.d() + ".animatedpoly");
         }

         float var3 = this.f_().c;
         float var4 = this.f_().d;
         float var5 = this.m();
         boolean var6 = !this.aT.B();
         this.A = var1.d();
         this.G = var1.a(0.0F, 1000.0F, 0.5F);
         this.g((float) var1.a(-180, 180));
         this.B = var1.a(-25.0F, 25.0F, 10.0F);
         this.P = var1.a(-25.0F, 25.0F, 10.0F);
         this.a_(var1.a(-35.0F, 35.0F, 7.0F));
         boolean var7 = var1.d();
         if (var7) {
            this.M = var1.a(-50.0F, 50.0F, 5.0F);
            this.O = this.M * 1.0F / L;
         } else {
            this.M = 0.0F;
            this.O = 0.0F;
         }

         Vh.a((nQ) var1, this.f_(), this.aT.R(), 50);

         // Update the position after it has been read from the message in the line above
         // The angle has been read before and set by this.g(float)
         this.last_x = this.f_().c;
         this.last_y = this.f_().d;
         this.last_angle = this.m();

         this.u = this.f_().d;
         this.b().c = var1.a(-25.0F, 25.0F, 10.0F);
         this.b().d = var1.a(-25.0F, 25.0F, 10.0F);
         boolean var8 = var1.d();
         if (var8) {
            this.K.c = var1.a(-25.0F, 25.0F, 5.0F);
            this.K.d = var1.a(-25.0F, 25.0F, 5.0F);
            this.N.c = this.K.c * 1.0F / J;
            this.N.d = this.K.d * 1.0F / J;
         } else {
            this.K.f();
            this.N.f();
         }

         this.f = var1.a(0.0F, 1000.0F, 10.0F);
         int var9 = Math.round(var1.a(0.0F, 100.0F, 0.5F) * (float) this.ad / 100.0F);
         if (!this.az.a() || (float) var9 <= this.ae) {
            this.ae = (float) var9;
         }

         this.ao = var1.a(0.0F, 1350.0F, 0.1F);
         if (var1.d()) {
            this.aJ();
         }

         boolean var10 = var1.d();
         if (var10) {
            this.az.b(var1.a(0, 250));
            boolean var11 = var1.d();
            if (var11) {
               int var12 = var1.a(0, 500);
               int var13 = var1.a(0, 500);
               int var14 = var1.a(0, 250);

               try {
                  Eb var15 = (Eb) this.getGame().R().f().j().get(var14);
                  this.az.a(var12, var13, var15);
               } catch (Exception var16) {
                  a.error(var16, var16);
               }
            }

            boolean var17 = var1.d();
            if (var17) {
               this.az.c(var1.a(0, 500));
               this.bg();
            }

            var6 = false;
            this.bM = 0.0F;
            this.bN = 0.0F;
            this.bL = 0.0F;
         }

         if (var6) {
            this.bM = this.f_().c - var3;
            this.bN = this.f_().d - var4;
            this.bL = dh.b(this.m() - var5, -180.0F, 180.0F);
            this.a(var3, var4);
            this.g(var5);
            this.B();
         }

         if (this.aT.B()) {
            this.bZ = true;
         }

         this.bR = 0;
         if (this.av) {
            this.aF();
         }

      }
   }

   public void a(nQ var1) {
      this.f(Vh.b(var1));
      this.a((oD) oD.p.b(var1));
      this.a((jV) jV.d.b(var1));
      Vh.a((nQ) var1, this.f_(), this.aT.R(), 50);
      this.g((float) Vh.a(var1));
      boolean var2 = var1.d();
      if (var2) {
         SR var3 = (SR) this.aT.ab().m().h().a(var1);
         this.a(var3);
      }

      boolean var4 = var1.d();
      if (var4) {
         this.aJ();
      }

      this.az.f(var1.b(16));
      this.az.g(var1.b(16));
      this.e(var1.a(0, 90));
   }

   public void a(nu var1) {
      Vh.a(var1, this.aS());
      oD.p.a(this.at, var1);
      jV.d.a(this.bp, var1);
      Vh.a((nu) var1, this.f_(), this.aT.R(), 50);
      Vh.a(var1, this.m());
      boolean var2 = this.Y != null;
      var1.a(var2);
      if (var2) {
         this.aT.ab().m().h().a(this.Y, var1);
      }

      var1.a(this.au);
      var1.b(16, Math.min('\uffff', this.az.w()));
      var1.b(16, Math.min('\uffff', this.az.x()));
      var1.a(0, 90, this.bm);
   }

   public void b(fe var1) {
      var1.R().b(this);
      if (var1.F()) {
         boolean var2 = this.getGame().J().u().g().a();
         var1.R().b(new JX(this, var2));
         kr var3;
         if (this.af()) {
            if (var1.t().e().c()) {
               var1.R().b(new IF(this));
            }

            var3 = this.getGame().G().b("planes/" + this.ag().toLowerCase() + "/engine_loop.wav");
            this.aX = this.getGame().ah().a(var3, dc.a);
            this.aX.b(true);
            this.aX.a(1.0F);
            var3 = this.getGame().G().b("planes/" + this.ag().toLowerCase() + "/engine_stall.wav");
            this.aY = this.getGame().ah().a(var3, dc.a);
            this.aY.a(1.0F);
            var3 = this.getGame().G().b("planes/damage_taken.wav");
            this.ba = this.getGame().ah().a(var3, dc.a);
         }

         if (this.getClass() != TG.class) {
            var3 = this.getGame().G().b("planes/afterburner.wav");
            this.aZ = this.getGame().ah().a(var3, dc.b(this.getGame(), this));
            this.aZ.b(true);
         }
      }

      if (!this.au) {
         this.aT().b(this);
         this.U();
         if (this.getGame().B()) {
            try {
               da var5 = JL.a("spawn");
               var5.a("player", this.aS());
               var5.a("team", this.aU().c());
               var5.a("plane", (Object) this.R().c().g);
               var5.a("perkRed", (Object) this.R().i().b());
               var5.a("perkGreen", (Object) this.R().k().b());
               var5.a("perkBlue", (Object) this.R().l().b());
               var5.a("skin", (Object) this.R().j().b());
               this.getGame().u().c().a(var5);
            } catch (Exception var4) {
               a.error(var4, var4);
            }
         }
      }

   }

   public void a(fe var1) {
      this.aJ();
   }

   public String toString() {
      return "Plane[" + this.aS() + "]" + super.toString();
   }

   public void v(float var1) {
      this.aD = dh.b(var1, -180.0F, 180.0F);
   }

   public iD bb() {
      return this.bv;
   }

   public void a(iD var1) {
      this.bv = var1;
   }

   public tK bc() {
      return this.aK;
   }

   public int bd() {
      return this.bk;
   }

   public int be() {
      return this.bg;
   }

   // $FF: synthetic method
   static Yr b(mN var0) {
      return var0.aQ;
   }

   // $FF: synthetic method
   static void a(mN var0, boolean var1) {
      var0.bZ = var1;
   }

   // $FF: synthetic method
   static float bf() {
      return ca;
   }

   // $FF: synthetic method
   static boolean c(mN var0) {
      return var0.bZ;
   }

   // $FF: synthetic method
   static boolean d(mN var0) {
      return var0.av;
   }

   // $FF: synthetic method
   static int e(mN var0) {
      return var0.ax;
   }

   // $FF: synthetic method
   static boolean f(mN var0) {
      return var0.bQ;
   }

   // $FF: synthetic method
   static int g(mN var0) {
      return var0.bO;
   }

   // $FF: synthetic method
   static fe h(mN var0) {
      return var0.aT;
   }

   // $FF: synthetic method
   static void b(mN var0, boolean var1) {
      var0.bQ = var1;
   }

   // $FF: synthetic method
   static void a(mN var0, int var1) {
      var0.bO = var1;
   }

   // $FF: synthetic method
   static void b(mN var0, int var1) {
      var0.ax = var1;
   }
}
