
/** Set the plane setup for bots to use the next time they spawn */
class BotPlaneSetupCmd extends Command {
   private IntParam bot;
   private IntParam plane;
   private IntParam red;
   private IntParam green;
   private IntParam blue;
   private IntParam skin;

   public BotPlaneSetupCmd(fe game) {
      super(game, "botPlaneSetup", PermissionGroup.admin);
      this.bot = new IntParam("bot", "Bot (0 for all, -1 for default)");
      this.plane = new IntParam("plane", "plane");
      this.red = new IntParam("red", "red");
      this.green = new IntParam("green", "green");
      this.blue = new IntParam("blue", "blue");
      this.skin = new IntParam("skin", "skin");
      this.setParams(new JI[]{bot, plane, red, green, blue, skin});
   }

   protected void execute(int[] a, String... b) {
      tK plane_type = null;
      switch (this.plane.value) {
         case 0:
            plane_type = tK.d;
            break; // loopy
         case 1:
            plane_type = tK.b;
            break; // bomber
         case 2:
            plane_type = tK.c;
            break; // explodet
         case 3:
            plane_type = tK.a;
            break; // biplane
         case 4:
            plane_type = tK.e;
            break; // miranda
         default:
            return;
      }
      jV setup = new jV(plane_type, red.value, green.value, blue.value, skin.value);
      fN server = this.getGame().u().c();
      En bot_manager = server.n();
      bot_manager.setPlaneSetup(this.bot.value, setup);
   }
}
