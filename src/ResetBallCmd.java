class ResetBallCmd extends Command {
   private FloatParam x;
   private FloatParam y;

   public ResetBallCmd(fe game) {
      super(game, "resetBall", PermissionGroup.admin);
      this.x = new FloatParam("x", "position X");
      this.y = new FloatParam("y", "position Y");
      this.setParams(new JI[]{x, y});
   }

   protected void execute(int[] a, String... b) {
      cV gameMode = this.getGame().T();

      if (gameMode instanceof QK) {
         QK ballGameMode = (QK) gameMode;
         ballGameMode.looseBall();
         ballGameMode.newBall(new Yr(this.x.value, this.y.value));
      }
   }
}
