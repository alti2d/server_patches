import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Ld extends Os {
   public static final Set a = new HashSet();
   private IntParam value;

   public Ld(fe var1) {
      super("testHealthModifier", var1);
      a.add(UUID.fromString("9d3a0dc4-6ca0-4805-a393-a4967bec332b"));
      this.value = new IntParam("%", "x");
      this.a(new JI[]{this.value});
   }

   protected void a(int[] var1, String... var2) {
      fN var3 = this.j.u().c();
      var3.m().b((float)this.value.value / 100.0F);
      var3.l().b();
      var3.l().a("testHealthModifier set to " + this.value.value + "%");
   }

   protected boolean a(NB var1, FW var2, Jt var3, String[] var4) {
      return true;
   }

   public String a() {
      return String.valueOf(Math.round(100.0F * this.j.J().u().ak().z()));
   }

   public String b() {
      return "100";
   }
}
