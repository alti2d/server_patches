
/** A floating point parameter to a server command */
class FloatParam extends JI {
   public float value;

   FloatParam(String name, String description) {
      super(name, description);
   }

   public wH[] a(String var1) {
      return null;
   }

   public boolean a(NB var1, String str) {
      try {
         this.value = Float.parseFloat(str);
         return true;
      } catch (Exception e) {
         var1.a("Parameter " + this.c() + " must be a number", false);
         return false;
      }
   }
}
