import java.util.Map;

class OverrideTDMScoreCmd extends Command {
   private IntParam score_l;
   private IntParam score_r;

   public OverrideTDMScoreCmd(fe game) {
      super(game, "overrideTdmScore", PermissionGroup.admin);
      this.score_l = new IntParam("scoreL", "The score for the left team");
      this.score_r = new IntParam("scoreR", "The score for the right team");
      this.setParams(new JI[]{score_l, score_r});
   }

   protected void execute(int[] a, String... b) {
      cV x = this.getGame().T();
      if (x instanceof sA) {
         sA gameMode = (sA) x;
         Map scores = gameMode.getScores();
         scores.put(gameMode.team_a(), score_l.value);
         scores.put(gameMode.team_b(), score_r.value);

         // Send the update to all players
         fN server = this.getGame().u().c();
         VS gameMsgHandler = server.l();
         h gameModeEv = new h(this.getGame(), gameMsgHandler);
         gameMsgHandler.a(gameModeEv);
      }
   }
}
