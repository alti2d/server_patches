import mods.Config;
import org.apache.log4j.Logger;
import org.hjson.JsonValue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

// ServerCommandManager
// Parses commands.txt and forwards commands to the relevant server.
public class uT {
   private static final Logger log = Logger.getLogger(uT.class);
   private static final Pattern pattern = Pattern.compile("([0-9]+),([a-zA-Z0-9_-]+),(.+)");
   private jt launcher;
   private hY manager;
   private BufferedReader file;
   private boolean started;
   private boolean shutdown;

   public uT() {
      jt.e.delete();
      jt.e.getParentFile().mkdirs();
      try {
         jt.e.createNewFile();
         this.file = new BufferedReader(new FileReader(jt.e));
      } catch (IOException e) {
         log.error("Failed to create commands.txt", e);
      }
   }

   public void a(jt launcher, hY manager) {
      if (this.started) {
         throw new IllegalStateException();
      } else {
         this.launcher = launcher;
         this.manager = manager;
         this.started = true;

         if (Config.global.fix_commands_latency) kS.b(new WatchRunner());
         else kS.b(new PollRunner());
      }
   }

   private void parse(String line) {
      List servers = this.manager.f();
      line = line.trim();
      Matcher matcher = pattern.matcher(line);
      int port;
      String message;
      if (matcher.matches()) {
         port = -1;
         try {
            port = Integer.parseInt(matcher.group(1));
         } catch (Exception var9) {
         }

         sl thread = this.manager.a(Integer.valueOf(port));
         if (thread == null) {
            StringBuilder str = new StringBuilder();
            str.append("Invalid server port \"").append(matcher.group(1)).append("\", valid values: ");

            for (Object server : servers) {
               sl t = (sl) server;
               str.append(t.a.m().a());
               str.append(",");
            }

            str.setLength(str.length() - 1);
            log.warn(str.toString());
            return;
         }

         message = matcher.group(2).toLowerCase();
         if (message.equals("console")) {
            String data = matcher.group(3);
            log.info("Executing: port=" + thread.a.m().a() + ", type=" + message + ", data=" + data);
            this.execute(thread.a, data);
         } else {
            if (!message.equals("restart")) {
               log.warn("Invalid command type \"" + matcher.group(2) + "\", valid values: " + "console,restart");
               return;
            }

            log.info("Command reader got \'restart\' command.");
            this.launcher.b();
         }
      } else {
         log.warn("Invalid input \"" + line + "\", format is [server port],[command type],[data]");
         port = 27275;
         String type = "console";
         message = "echo this is a test server message";
         if (servers.size() > 0) {
            fN server = ((sl) servers.get(0)).a;
            port = server.m().a();
         }

         log.warn("Example: " + port + "," + type + "," + message);
      }

   }

   private void execute(fN server, String command) {
      server.o().a(new Command(server, command));
   }

   public void a() {
      try {
         this.shutdown = true;
         this.manager = null;
         if (this.file != null) {
            this.file.close();
            this.file = null;
         }
      } catch (Throwable e) {
         log.error("Failed to shutdown command reader", e);
      }

   }

   // The default runner, polls for new data every 250ms
   private class PollRunner implements Runnable {
      public void run() {
         while (started && !shutdown) {
            try {
               String line = file.readLine();
               if (line != null) {
                  parse(line);
               } else {
                  kS.a(250);
               }
            } catch (Exception e) {
               log.error("Failed to read console command", e);
            }
         }

      }
   }

   // A custom runner, waits for file changes.
   private class WatchRunner implements Runnable {
      public void run() {
         WatchService watcher;
         try {
            watcher = FileSystems.getDefault().newWatchService();
            Path servers = jt.a.toPath();
            servers.register(watcher, ENTRY_MODIFY);
         } catch (IOException e) {
            log.error("Failed to start watcher for commands.txt, falling back to polling.");
            kS.b(new PollRunner());
            return;
         }

         log.info("Watching for changes to 'commands.txt'");
         while (started && !shutdown) {
            WatchKey key;
            try {
               key = watcher.take();
            } catch (InterruptedException e) {
               return;
            }

            for (WatchEvent<?> event : key.pollEvents()) {
               if (event.kind() == ENTRY_MODIFY) {
                  try {
                     String line;
                     while ((line = file.readLine()) != null) {
                        parse(line);
                     }
                  } catch (Exception e) {
                     log.error("Failed to read console command", e);
                  }
               }
            }

            key.reset();
         }
      }
   }

   private class Command implements Runnable {
      private final fN server;
      private final String command;

      Command(fN server, String command) {
         this.server = server;
         this.command = command;
      }

      public void run() {
         fe game = this.server.l().n();
         Kg var2 = game.ao();
         iM var3 = lj.a(var2, this.command);
         lj var4 = var3.b();
         if (var4 == null) {
            log.info("Invalid command: \"" + this.command + "\"");
         } else {
            lj var5 = var3.b();
            if (!(var5 instanceof Os)) {
               log.info("Non server command: \"" + this.command + "\"");
            } else {
               Os var6 = (Os) var5;
               FW var7 = new FW(new lY());
               var7.a(YY.b);
               jX var8 = new jX();
               var6.a(var8, var7, (Jt) null, (int[]) null, var3.c());
            }
         }
      }

      class jX extends NB {
         public void a(String var1, boolean var2) {
            var1 = var1.replaceAll("\\^[0-9a-fA-F]{8}\\^", "");
            log.info(var1);
         }
      }
   }
}
