
// Player statistic
public class LV extends SX {
   private int min;
   private int max;

   LV(xs cont, String var2, int initial, int min, int max) {
      super(cont, var2);
      this.min = min;
      this.max = max;
      this.a(Integer.valueOf(initial));
      this.setThisLife(initial);
   }

   public void a(Integer value) {
      value = dh.a(value, this.min, this.max);
      super.a(value);
   }

   public void setThisLife(Integer value) {
      value = dh.a(value, this.min, this.max);
      super.setThisLife(value);
   }

   public void a(nQ msg_in) {
      this.a(Integer.valueOf(msg_in.a(this.min, this.max)));
   }

   public void a(nu msg_out) {
      msg_out.a(this.min, this.max, (Integer) this.a());
   }

   public void a(int amount) {
      this.a(Integer.valueOf(this.a().intValue() + amount));
      this.setThisLife(this.getThisLife().intValue() + amount);
   }

   public String toString() {
      return Qk.a(this.value.intValue());
   }
}
