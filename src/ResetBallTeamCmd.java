class ResetBallTeamCmd extends Command {
   private IntParam team;

   public ResetBallTeamCmd(fe game) {
      super(game, "resetBallTeam", PermissionGroup.admin);
      this.team = new IntParam("team", "team");
      this.setParams(new JI[]{team});
   }

   protected void execute(int[] a, String... b) {
      cV gameMode = this.getGame().T();

      if (gameMode instanceof QK) {
         QK ballGameMode = (QK) gameMode;

         oD team = oD.c;
         if (this.team.value > -1 && this.team.value < 2)
            team = (oD) ballGameMode.i().get(this.team.value);

         ballGameMode.looseBall();
         ballGameMode.newBall(ballGameMode.ballSpawnPos(team));
      }
   }
}
