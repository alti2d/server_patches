import mods.Config;

// The startTournament command. We remove the requirement that the players
// must not be bots when bots.allow_join_tournament is true.
public class VK extends Os {
   public VK(fe var1) {
      super("startTournament", var1);
      this.a(0.95D);
   }

   protected void a(int[] var1, String... var2) {
      this.j.u().c().l().p();
   }

   protected boolean a(NB var1, FW var2, Jt var3, String[] var4) {
      int eligiblePlayers = 0;

      for (Object o : this.j.J().u().j()) {
         mF player = (mF) o;

         boolean count_bots = Config.get(port()).bots.allow_join_tournament;
         if (!player.e().c().equals(oD.c)) {
            if(!player.a() || count_bots) ++eligiblePlayers;
         }
      }

      if (eligiblePlayers < 2) {
         var1.a(var1.c() + "Tournaments require 2 or more non-spectating players.", false);
         return false;
      } else {
         return true;
      }
   }

   public String a() {
      return "";
   }

   public String b() {
      return "";
   }
}
