
// RangedPlayerStat
public class EL extends SX {
   private float min;
   private float max;
   private float scale;

   EL(xs cont, String name, float initial, float min, float max, float scale) {
      super(cont, name);
      this.min = min;
      this.max = max;
      this.scale = scale;
      this.a(Float.valueOf(initial));
      this.setThisLife(initial);
   }

   public void a(Float value) {
      value = dh.a(value, this.min, this.max);
      super.a(value);
   }

   public void setThisLife(Float value) {
      value = dh.a(value, this.min, this.max);
      super.setThisLife(value);
   }

   public void a(nQ var1) {
      this.a(Float.valueOf(var1.a(this.min, this.max, this.scale)));
   }

   public void a(nu var1) {
      var1.a(this.min, this.max, this.scale, this.a().floatValue());
   }

   public void a(float amount) {
      this.a(Float.valueOf(this.a().floatValue() + amount));
      this.setThisLife(this.getThisLife().floatValue() + amount);
   }
}
