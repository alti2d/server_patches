import org.apache.log4j.Logger;

import java.util.*;

// GameMode
// - `getTimer` method added.
public abstract class cV {
   private static final Logger a = Logger.getLogger(cV.class);
   private DK b;
   private boolean c;
   private oU e;
   private oU f;
   private oU g;
   private kD h;
   private int i;
   private oU j;
   private boolean k;
   private long l;
   private long m;
   private String n = "?";
   private static final qt o = sx.d(14.9F);
   private boolean p;
   boolean d;
   private oD q;
   private Map r;
   private int s;
   private int t;
   private int u;
   private static final qt v = sx.d(3.0F);

   public oU getTimer() {
      return this.j;
   }

   public cV(DK var1, kD var2) {
      this.q = oD.c;
      this.r = new HashMap();
      this.b = var1;
      this.a(o);
      this.h = var2;
      this.i = 0;
      if (var2.e() > 0) {
         this.j = new oU(sx.d((float) var2.e()));
      }

      if (this.t().B()) {
         this.g = new oU(sx.d(30.0F));
      }

      this.s = var2.f() * 30;
      this.t = this.s;
      if (Ja.R() != null) {
         this.t = 0;
      }

   }

   protected void n() {
      ArrayList var1 = new ArrayList(this.v().h());
      if (this.i().size() == 2) {
         oD var2 = (oD) this.i().get(0);
         oD var3 = (oD) this.i().get(1);
         ArrayList var4 = new ArrayList();
         ArrayList var5 = new ArrayList();
         Iterator var7 = var1.iterator();

         while (var7.hasNext()) {
            LK var6 = (LK) var7.next();
            oD var8 = var6.aU();
            if (var8.equals(var2)) {
               var4.add(var6);
            } else if (var8.equals(var3)) {
               var5.add(var6);
            }
         }

         if (var4.size() == 0) {
            var4.addAll(var1);
         }

         if (var5.size() == 0) {
            var5.addAll(var1);
         }

         this.r.put(var2, new Hv(var4, false));
         this.r.put(var3, new Hv(var5, false));
      } else {
         this.r.put(oD.c, new Hv(var1, false));
      }

   }

   public abstract String c();

   public String a(mF var1) {
      return var1.g().k.a() + " XP";
   }

   public Comparator o() {
      return new vT(this);
   }

   public boolean p() {
      return false;
   }

   public n q() {
      return null;
   }

   public String r() {
      return "Objective Camera";
   }

   public DK s() {
      return this.b;
   }

   public fe t() {
      return this.b.n();
   }

   public Kb u() {
      return this.b.h();
   }

   public ax v() {
      return this.u().f();
   }

   public void a(qt var1) {
      this.e = new oU(var1, true);
      float var2 = Math.max(0.0F, var1.a() - 30.0F);
      this.f = new oU(new qt(var2));
   }

   public boolean w() {
      return !this.e.e();
   }

   public boolean h() {
      return false;
   }

   public void c(float var1) {
      this.e.c(var1);
   }

   public float d(float var1) {
      return this.e.f(var1);
   }

   public boolean x() {
      return this.j != null;
   }

   public float e(float var1) {
      return this.j == null ? 0.0F : sx.e(this.j.f(var1));
   }

   public float y() {
      return this.j == null ? 0.0F : sx.e(this.j.c());
   }

   public void z() {
      this.d = false;
      this.q = oD.c;
   }

   public void a() {
      this.c = false;
      if (this.j != null) {
         this.j.d();
      }

      Iterator var2 = this.t().J().u().j().iterator();

      while (var2.hasNext()) {
         mF var1 = (mF) var2.next();
         var1.f();
      }

      this.A();
   }

   protected void A() {
      ArrayList var1 = new ArrayList(this.t().R().aB());
      Iterator var3 = var1.iterator();

      while (var3.hasNext()) {
         Yl var2 = (Yl) var3.next();
         if (var2 instanceof bu) {
            ((bu) var2).a();
         }
      }

      var3 = this.v().j().iterator();

      while (var3.hasNext()) {
         Eb var4 = (Eb) var3.next();
         var4.E();
      }

      var3 = this.v().k().iterator();

      while (var3.hasNext()) {
         Ww var5 = (Ww) var3.next();
         var5.J();
      }

      var3 = this.v().i().iterator();

      while (var3.hasNext()) {
         HM var6 = (HM) var3.next();
         var6.c();
      }

   }

   public long B() {
      return this.l;
   }

   public long C() {
      return this.m;
   }

   public String D() {
      return this.n;
   }

   public String E() {
      return this.c();
   }

   public abstract String a(oD var1);

   private void k() {
      dp var1 = this.t().J().u();
      if (!this.p && !var1.x() && !var1.y()) {
         this.p = true;
         String var2 = XY.a(ip.h) + RU.M().a(this.E());
         var1.O().a(var2, false, (int) sx.d(5.0F).a());
      }

      if (var1.m()) {
         oD var4 = var1.ad();
         if (!this.d && !ty.a((Object) oD.c, (Object) var4) && !ty.a((Object) this.q, (Object) var4)) {
            this.d = true;
            String var3 = XY.a(ip.h) + RU.M().a(this.a(var4));
            var1.O().a(var3, false, (int) sx.d(6.0F).a());
         }

         this.q = var4;
      } else if (ty.a((Object) var1.ad(), (Object) oD.c)) {
         this.q = oD.c;
      }

   }

   public final void F() {
      if (this.b.n().F()) {
         this.k();
      }

      if (!this.e.e()) {
         this.e.b();
         if (this.t().B() && this.i < this.h.g() && this.t().u().c().m().e() && !this.t().u().c().l().r()) {
            int var1 = (int) this.e.h();
            if (var1 == 60) {
               this.t().u().c().l().o();
            }
         }
      } else {
         ++this.l;
      }

      if (this.j != null && !this.c) {
         this.j.b();
         if (this.j.e() && this.t().B()) {
            this.G();
         }
      }

      if (this.c && !this.f.e()) {
         this.f.b();
         if (this.f.e() && this.t().B()) {
            this.m();
         }
      }

      if (this.t().B()) {
         this.g.b();
         if (this.g.e()) {
            VS var3 = this.t().u().c().l();
            h var2 = new h(this.t(), var3);
            var3.a((kY) var2);
            this.g.d();
         }
      }

      if (this.t > 0) {
         --this.t;
         if (this.t().B() && this.t().u().c().m().e() && !this.t().u().c().l().r() && this.t == 60) {
            this.t().u().c().l().o();
         }
      }

      if (this.u > 0) {
         --this.u;
      }

      if (!this.k) {
         this.b();
      }

   }

   public abstract void b();

   private void m() {
      if (!this.t().B()) {
         throw new IllegalStateException();
      } else {
         VS var1 = this.t().u().c().l();
         boolean var2 = this.i >= this.h.g();
         if (var2) {
            var1.k();
            this.k = true;
         } else {
            XV var3 = new XV(this.t());
            var1.a((kY) var3);
         }

      }
   }

   public void G() {
      if (!this.t().B()) {
         throw new IllegalStateException();
      } else {
         fN var1 = this.t().u().c();
         int var2 = this.i + 1;
         boolean var3 = var2 >= this.h.g();
         if (var1.m().Z() && var1.m().i().size() >= 3 && var3) {
            vO var4 = new vO(this);
            long var5 = (long) Math.round(sx.f(this.e.c()));
            var5 -= 2000L;
            if (var5 < 0L) {
               var5 = 100L;
            }

            long var7 = 500L;
            if (var7 > var5) {
               var7 = var5 / 4L;
            }

            List var9 = var1.s().a();
            this.t().ao().a(var4, (Jt) null, Kg.c, new eH(this.t()), (String[]) var9.toArray(new String[var9.size()]), var5, var7);
         }

         List var16 = this.t().J().u().j();
         Iterator var6 = var16.iterator();

         while (var6.hasNext()) {
            mF var18 = (mF) var6.next();
            mN.a(var18);
         }

         ze var19 = new ze(this.t(), this);
         VS var17 = this.t().u().c().l();
         var17.a((kY) var19);

         try {
            HashMap var20 = new HashMap();
            Iterator var22 = this.l().iterator();

            mF var10;
            while (var22.hasNext()) {
               Sr var8 = (Sr) var22.next();
               var10 = zW.a(var8, var16);
               var20.put(var8.a(), Integer.valueOf(var10.h()));
            }

            ArrayList var21 = new ArrayList();
            Iterator var25 = var16.iterator();

            while (var25.hasNext()) {
               mF var23 = (mF) var25.next();
               var21.add(Integer.valueOf(var23.h()));
            }

            HashMap var24 = new HashMap();
            Iterator var11 = (new xs()).r.iterator();

            while (var11.hasNext()) {
               SX var26 = (SX) var11.next();
               var24.put(var26.b(), new ArrayList(var16.size()));
            }

            var11 = var16.iterator();

            while (var11.hasNext()) {
               var10 = (mF) var11.next();
               Iterator var13 = var10.g().r.iterator();

               while (var13.hasNext()) {
                  SX var12 = (SX) var13.next();
                  List var14 = (List) var24.get(var12.b());
                  var14.add(var12.a());
               }
            }

            da var27 = JL.a("roundEnd");
            var27.a("winnerByAward", (Map) var20);
            var27.a("participants", (Collection) var21);
            var27.a("participantStatsByName", (Map) var24);
            this.t().u().c().a(var27);
         } catch (Exception var15) {
            a.error(var15, var15);
         }

      }
   }

   public void H() {
      this.c = true;
      ++this.i;
      this.e.d();
      this.f.d();
      this.l = 0L;
      this.t().J().u().Z();
   }

   public boolean g() {
      return true;
   }

   public float I() {
      return this.L() ? (float) this.t : v.a();
   }

   protected Map J() {
      return this.r;
   }

   public LK b(mF var1) {
      Hv var2;
      if (this.i().size() == 2) {
         var2 = (Hv) this.r.get(var1.e().c());
      } else {
         var2 = (Hv) this.r.get(oD.c);
      }

      return (LK) var2.a();
   }

   public List l() {
      return this.i().size() == 2 ? Sr.n : Sr.o;
   }

   public void a(nu var1) {
      float var2 = Math.min(3600.0F, (float) this.l / 30.0F);
      var1.a(0.0F, 3600.0F, 4.0F, var2);
      int var3 = (int) this.d(0.0F);
      var1.a(0, 18000, var3);
      if (this.j != null) {
         var1.a(0, (int) this.j.c(), (int) this.j.h());
      }

      var1.a(0, this.s, this.t);
   }

   public void a(nQ var1) {
      this.l = (long) Math.round(var1.a(0.0F, 3600.0F, 4.0F) * 30.0F);
      int var2 = var1.a(0, 18000);
      this.c((float) var2);
      if (var2 > 0) {
         this.t().J().u().b((float) var2);
      }

      if (this.j != null) {
         this.j.c((float) var1.a(0, (int) this.j.c()));
      }

      this.t = var1.a(0, this.s);
   }

   public int K() {
      return this.u;
   }

   public void a(int var1) {
      this.u = var1;
   }

   public boolean L() {
      return this.t > 0;
   }

   public int M() {
      return this.t;
   }

   public void a(byte var1, nu var2) {
   }

   public void a(byte var1, nQ var2) {
   }

   public void a(gk var1) {
      float var2 = Math.min(10800.0F, (float) this.l / 30.0F);
      var1.a(0.0F, 10800.0F, 1.0F, var2);
      String var3 = this.t().u().c().l().e();
      int var4 = this.i + 1;
      if (var4 >= this.h.g()) {
         var3 = this.t().u().c().l().l();
      } else {
         var3 = var3 + " (round " + (var4 + 1) + " of " + this.h.g() + ")";
      }

      var1.a(var3);
   }

   public void a(LG var1) {
      this.m = (long) Math.round(var1.a(0.0F, 10800.0F, 1.0F));
      this.n = var1.j();
      this.H();
   }

   public void b(float var1) {
   }

   public void a(float var1) {
   }

   public float d() {
      return 35.0F;
   }

   public float f() {
      return 1.0F;
   }

   public List i() {
      return oD.m;
   }

   public int a(Mb var1) {
      return var1.aU().c();
   }

   public boolean a(Mb var1, Mb var2) {
      return this.a(var1) == this.a(var2);
   }

   public boolean a(Mb var1, Sp var2) {
      return !this.a((Mb) var1, (Mb) var2) && var2.aN();
   }

   public boolean a(Mb var1, mN var2) {
      return (var1 == var2 || !this.a((Mb) var1, (Mb) var2)) && var2.aN();
   }

   public void a(mN var1, mN var2) {
   }

   public void a(DP var1) {
   }

   public void a(At var1) {
   }

   public yr j() {
      return new rq(this.t(), (Eb) null);
   }

   public float e() {
      return 1.0F;
   }

   public void c(oD var1) {
      if (this.t().B() && this.t().J().u().ak().p()) {
         if (var1.equals(oD.c)) {
            a.info("[Tournament End Round] Tie!");

            try {
               da var2 = JL.a("tournamentRoundEnd");
               var2.a("result", (Object) "tie");
               var2.a("winners", (Collection) (new ArrayList()));
               var2.a("losers", (Collection) (new ArrayList()));
               this.t().u().c().a(var2);
               this.t().u().c().p();
            } catch (Exception var11) {
               a.error(var11, var11);
            }
         } else {
            ArrayList var12 = new ArrayList();
            ArrayList var3 = new ArrayList();
            Iterator var5 = this.t().J().u().j().iterator();

            while (var5.hasNext()) {
               mF var4 = (mF) var5.next();
               if (var4.e().c().equals(var1)) {
                  var12.add(var4.c());
               } else if (this.i().indexOf(var4.e().c()) != -1) {
                  var3.add(var4.c());
               }
            }

            a.info("[Tournament End Round] Team " + this.i().indexOf(var1) + " wins");
            a.info("[Tournament End Round] Winners: " + var12);
            a.info("[Tournament End Round]  Losers: " + var3);
            var12 = new ArrayList();
            var3 = new ArrayList();
            int var13 = this.i().indexOf(var1);
            Map var14 = this.t().J().u().ak().r();
            Iterator var7 = var14.keySet().iterator();

            while (var7.hasNext()) {
               UUID var6 = (UUID) var7.next();
               boolean var8 = ((Integer) var14.get(var6)).intValue() == var13;
               ArrayList var9 = var8 ? var12 : var3;
               var9.add(var6.toString());
            }

            try {
               da var15 = JL.a("tournamentRoundEnd");
               var15.a("result", (Object) "decisive");
               var15.a("winners", (Collection) var12);
               var15.a("losers", (Collection) var3);
               this.t().u().c().a(var15);
               this.t().u().c().p();
            } catch (Exception var10) {
               a.error(var10, var10);
            }

            this.t().u().c().p();
         }
      }

   }

   public void a(ip var1, String var2, float var3) {
      var2 = XY.a(var1) + RU.M().a(var2);
      int var4 = Math.round(30.0F * var3);
      this.t().J().u().O().a(var2, false, var4);
   }

   public void a(Hc var1) {
   }

   public boolean N() {
      return false;
   }
}
