import mods.Config;
import org.hjson.JsonObject;
import org.hjson.JsonValue;

import java.util.*;

// `NicknameParam` - A parameter to many commands which need a player as a
// parameter. The restriction on bots is removed for the server code, so
// these commands can now be applied to bots through `commands.txt`.

public class Sz extends WP {
   protected fe c;

   public Sz(fe var1) {
      super("Nickname", "Enter the nickname from list players", (Object[]) null, (String[]) null);
      this.c = var1;
   }

   protected wH[] a() {
      List list = this.d();
      return (wH[]) list.toArray(new wH[list.size()]);
   }

   protected List d() {
      ArrayList list = new ArrayList();
      if (this.c.B()) {
         fN server = this.c.t().h().c();
         Collection clients = server.l().j();

         for (Object aVar3 : clients) {
            Jt client = (Jt) aVar3;
            UUID vID = client.f().D();

            int port = this.c.u().c().port();
            boolean allow_bot = Config.get(port).bots.commands_target_bots;

            if (allow_bot || !vID.equals(mF.a)) {
               int playerNo = client.g();
               String nick = client.f().E();
               JK ids = new JK(playerNo, vID, nick);
               list.add(new wH(ids, nick));
            }
         }
      } else {
         List var10 = this.c.J().u().j();
         Iterator var12 = var10.iterator();

         while (var12.hasNext()) {
            mF var11 = (mF) var12.next();
            UUID var13 = var11.e().a().D();
            // Commented to allow selecting bots from a modified client.
            //if(!var13.equals(mF.a)) {
            int var14 = var11.h();
            String var15 = var11.e().a().E();
            JK var16 = new JK(var14, var13, var15);
            list.add(new wH(var16, var15));
            //}
         }
      }

      return list;
   }
}
