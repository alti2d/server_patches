import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadPoolExecutor;

import mods.Config;
import org.apache.log4j.Logger;

// AltitudeServer
// - Added new permissions system
public class fN implements Runnable {
   private static final Logger log = Logger.getLogger(fN.class);
   // Multicast socket addresses:
   public static final DZ a = new DZ(new byte[]{(byte)-29, (byte)27, (byte)27, (byte)4}, 27260);
   public static final DZ b = new DZ(new byte[]{(byte)-29, (byte)27, (byte)27, (byte)5}, 27261);

   private hp config;
   private boolean running;
   private boolean shutdown;
   private aV time;
   private aaz dispatcher;
   private jj joinReqHandler;
   private Ye dlReqHandler;
   private VS gameHdr;
   private EG pingHandler;
   private YB ipReqHandler;
   private En botManager;
   private Gp metaHandlers;
   private DZ socketAddr;
   private r runQueue;
   private Yu mapRatings;
   private ke latencyConf;
   private boolean t;
   private long startTime;
   private boolean mapChanged;
   private boolean specsAllChat = true;
   private Permissions permissions;
   private HashMap<Integer, Boolean> specchat = new HashMap<>();

   public fN() {
      this.runQueue = new r();
      this.latencyConf = null;
      this.t = false;
   }

   public fN(DZ var1, hp var2) {
      this();
      this.a(var1, var2);
   }

   public void a(DZ var1, hp var2) {
      try {
         this.b(var1, var2);
      } catch (Exception var4) {
         this.e();
         throw var4;
      }
   }

   public boolean allowAllChat(int playerNo) {
      if (this.gameHdr.isTournament())
			if(this.specchat.containsKey(playerNo))
			   if(this.specchat.get(playerNo)) return false;

      if (this.specsAllChat) return true;
      if (playerNo == -1) return true;

      if (this.gameHdr.isTournament()) {
         dp player_cont = this.gameHdr.n().J().u();
         UUID vaporID = player_cont.c(playerNo).e().a().D();
         return (gameHdr.c(vaporID) != oD.c);
      } else {
         dp player_cont = this.gameHdr.n().J().u();
         return (player_cont.c(playerNo).e().c() != oD.c);
      }
   }

   public boolean shouldForwardChat(int from, int to, boolean isTeam) {
      if (from == -1) return true;

      dp player_cont = this.gameHdr.n().J().u();
      UUID fromUUID = player_cont.c(from).e().a().D();
      UUID toUUID = player_cont.c(to).e().a().D();
      oD fromTeam;
      oD toTeam;

      Config config = Config.get(port());
      if (this.gameHdr.isTournament() && config.tournament_team_chat) {
         fromTeam = gameHdr.c(fromUUID);
         toTeam = gameHdr.c(toUUID);
      } else {
         fromTeam = player_cont.c(from).e().c();
         toTeam = player_cont.c(to).e().c();
      }

      if (isTeam && !fromTeam.equals(toTeam)) return false;
      if (!this.gameHdr.isTournament()) return true;

      // If a tournament player is sitting, show them messages in all chat
      if (player_cont.c(to).e().c() == oD.c) return true;

      return isTeam || toTeam == oD.c || !this.specchat.containsKey(to) || !this.specchat.get(to);
   }

   public void setSpecAllChat(boolean value) {
      this.specsAllChat = value;
   }

   public void setSpecChat(int player, boolean value) {
   	this.specchat.put(player, value);
   }

   public void loadPermissions() {
      this.permissions = new Permissions(this.port());
   }

   public Permissions getPermissions() {
      return this.permissions;
   }

   public int port() {
      if(this.m() != null) return this.m().a();
      else return -1;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("AltitudeServer<");
      var1.append("config=`").append(this.config).append("\'");
      var1.append("running=`").append(this.running).append("\'");
      var1.append(">");
      return var1.toString();
   }

   private void b(DZ var1, hp var2) {
      if(this.dispatcher != null) {
         throw new IllegalStateException("Already initialized");
      } else {
         this.loadPermissions();
         this.config = var2;
         var2.b().h().a(this);
         this.time = new aV(30, -1);
         this.mapRatings = new Yu(this);

         try {
            da var3 = JL.a("serverInit");
            var3.a("name", (Object)var2.c());
            var3.a("maxPlayerCount", var2.d());
            this.a(var3);
         } catch (Exception var7) {
            log.error(var7, var7);
         }

         int var8 = var2.a();
         log.info("Starting server on port " + var8);
         this.latencyConf = Zj.a();
         this.dispatcher = new aaz(var2.X(), var8, true, true, (ThreadPoolExecutor)null, this.latencyConf, var2.Y(), (lZ)null);
         this.dispatcher.b(true);
         this.dispatcher.a(true);
         this.joinReqHandler = new jj(this);
         float var4 = (float)(var2.x() * 1024);
         this.dlReqHandler = new Ye(this, fe.f, var4);
         this.gameHdr = new VS(this, var2);
         this.gameHdr.a();
         this.pingHandler = new EG();
         this.ipReqHandler = new YB(this);
         this.dispatcher.a((byte)70, (wr)this.joinReqHandler);
         this.dispatcher.a((byte)72, (wr)this.dlReqHandler);
         this.dispatcher.a((byte)71, (wr)this.gameHdr);
         this.dispatcher.a((byte)73, (wr)this.pingHandler);
         if(!var2.T() && !this.gameHdr.n().E()) {
            this.dispatcher.b(a);
            this.dispatcher.a((byte)74, (wr)this.ipReqHandler);
         }

         this.botManager = new En(this, this.dispatcher);
         int var5 = BU.b.indexOf(var2.U());

         for(int var6 = 0; var6 < var2.Q().b(); ++var6) {
            this.botManager.a(var2.Q().c(), var5);
         }

         if(!this.gameHdr.n().E()) {
            this.metaHandlers = new Gp(this, var1, this.dispatcher);
         }

         this.dispatcher.b(false);
      }
   }

   public CR a() {
      return this.dispatcher != null?this.dispatcher.g():null;
   }

   public void b() {
      this.time.a();
   }

   public boolean c() {
      return this.metaHandlers != null;
   }

   public Gp d() {
      return this.metaHandlers;
   }

   private void t() {
      boolean var1 = this.mapChanged;
      this.mapChanged = false;
      if(this.startTime == 0L) {
         this.startTime = System.currentTimeMillis();
      }

      if(this.gameHdr.n().Y() != 0) {
         List<Exception> exceptionList = this.runQueue.a();
         for(Exception e : exceptionList) {
            log.error("Exception in server run queue", e);
         }
      }

      float var2 = (float)this.time.b();
      if(this.c()) {
         this.metaHandlers.a(var2);
      }

      if(var2 > 400.0F) {
         if(System.currentTimeMillis() - this.startTime > 5000L) {
            try {
               da var3 = JL.a("serverHitch");
               var3.a("duration", (double)var2);
               var3.a("changedMap", var1);
               this.a(var3);
            } catch (Exception var5) {
               log.error(var5, var5);
            }

            log.warn("Server hitch detected: " + var2 + " milliseconds, free memory is " + ml.b());
         }

         var2 = 400.0F;
      }

      this.botManager.a(var2);
      this.joinReqHandler.a(var2);
      this.dispatcher.e();
      int var6 = this.gameHdr.a(var2);

      for(int var4 = 0; var4 < var6; ++var4) {
         this.dlReqHandler.b();
      }

      if(var6 > 0) {
         this.pingHandler.a();
      }

      this.dispatcher.d();
      kS.a(5);
   }

   public void run() {
      this.running = true;

      try {
         da var1 = JL.a("serverStart");
         this.a(var1);
      } catch (Exception var6) {
         log.error(var6, var6);
      }

      try {
         while(this.running) {
            this.t();
         }
      } catch (Throwable var7) {
         log.error("serverLoop disrupted", var7);
      } finally {
         this.e();
      }

   }

   public void e() {
      this.shutdown = true;

      try {
         da var1 = JL.a("serverShutdown");
         this.a(var1);
      } catch (Exception var4) {
         log.error(var4, var4);
      }

      log.info("shutting down server");

      try {
         if(this.dispatcher != null) {
            this.dispatcher.d();
            this.dispatcher.h();
            this.dispatcher = null;
         }
      } finally {
         if(this.botManager != null) {
            this.botManager.c();
            this.botManager = null;
         }

         if(this.metaHandlers != null) {
            this.metaHandlers.a();
            this.metaHandlers = null;
         }

         this.time = null;
         this.joinReqHandler = null;
         this.dlReqHandler = null;
         this.gameHdr = null;
         this.pingHandler = null;
         this.ipReqHandler = null;
         if(this.runQueue != null) {
            this.runQueue.b();
            this.runQueue = null;
         }

      }

   }

   public boolean f() {
      return this.shutdown;
   }

   public void g() {
      this.running = false;
   }

   public DZ h() {
      int var1;
      if(this.config != null) {
         var1 = this.config.a();
      } else {
         var1 = 0;
      }

      return new DZ(DZ.c, var1);
   }

   // dl req handler
   public Ye i() {
      return this.dlReqHandler;
   }

   // join req handler
   public jj j() {
      return this.joinReqHandler;
   }

   // ping handler
   public EG k() {
      return this.pingHandler;
   }

   // game handler
   public VS l() {
      return this.gameHdr;
   }

   // config
   public hp m() {
      return this.config;
   }

   // bot manager
   public En n() {
      return this.botManager;
   }

   // run queue
   public r o() {
      return this.runQueue;
   }

   public void a(da var1) {
      hp var2 = this.m();
      kg var3 = var2.S();
      if(var3 != null) {
         var3.a(var2.a(), var1);
      }

   }

   public void p() {
      hp var1 = this.m();
      kg var2 = var1.S();
      if(var2 != null) {
         var2.b();
      }

   }

   public void a(DZ var1) {
      this.socketAddr = new DZ(var1);
      if(this.config.a() != this.socketAddr.h() || this.metaHandlers.b().b() && this.metaHandlers.b().d()) {
         log.warn("Your server (" + this.config.c() + ") is behind a firewall. Only players on your LAN will be able to join your server. Go to http://wiki.altitudegame.com/index.php?title=Altitude_Server_Manual#Router_and_Firewall_Port_Forwarding for advanced firewall and router instructions");
      }

   }

   public DZ q() {
      return this.socketAddr;
   }

   public aaz r() {
      return this.dispatcher;
   }

   public void a(boolean var1) {
      this.mapChanged = var1;
   }

   public Yu s() {
      return this.mapRatings;
   }
}
