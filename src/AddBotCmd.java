import org.apache.log4j.Logger;

/** Creates a new bot with a given difficulty level */
class AddBotCmd extends Command {
   private static final Logger logger = Logger.getLogger(AddBotCmd.class);
   private IntParam difficulty;

   public AddBotCmd(fe game) {
      super(game, "addBot", PermissionGroup.admin);
      this.difficulty = new IntParam("difficulty", "difficulty (0 to 3)");
      this.setParams(new JI[]{difficulty});
   }

   protected void execute(int[] a, String... b) {
      fN server = this.getGame().u().c();
      En bot_manager = server.n();
      switch (this.difficulty.value) {
         case 0:
            bot_manager.addBot(rT.a, 0);
            break;
         case 1:
            bot_manager.addBot(rT.b, 0);
            break;
         case 2:
            bot_manager.addBot(rT.c, 0);
            break;
         case 3:
            bot_manager.addBot(rT.d, 0);
            break;
         default:
            logger.error("Invalid bot difficulty " + this.difficulty.value);
      }
   }
}
