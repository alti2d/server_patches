class UnblockPerkCmd extends Command {
   private StringParam perk_name;

   public UnblockPerkCmd(fe game) {
      super(game, "unblockPerk", PermissionGroup.admin);
      this.perk_name = new StringParam("perkName", "The name of the perk to unblock");
      this.setParams(new JI[]{perk_name});
   }

   protected void execute(int[] a, String... b) {
      this.getServer().l().blockedPerks.remove(perk_name.value);
   }
}
