import org.apache.log4j.Logger;

import java.util.UUID;

// Join request handler - Sends and parses join requests and responses
// - call CustomJoin.checkJoin in parseJoinReq
public class jj extends wr {

   private static final Logger a = Logger.getLogger(jj.class);
   private fN server;
   private yS joiningList = new yS(16000.0F);

   public jj(fN server) {
      this.server = server;
   }

   public void a(DZ ip, int counter, qm resource, Ui srvConfig) { // sendLoad
      try {
         gk msg = UY.a(Za.b);
         msg.b(2, counter);
         qm.b.a(resource, msg);
         srvConfig.a(msg);
         this.a(ip, zq.c, msg);
      } catch (Exception e) {
         a.error(e, e);
      }

   }

   public void a(DZ ip, String reason) { // sendDC
      try {
         gk var3 = UY.a(Za.c);
         var3.a(reason);
         this.a(ip, zq.c, var3);
      } catch (Exception var4) {
         a.error(var4, var4);
      }

   }

   public void a(DZ ip) {
      try {
         gk var2 = UY.a(Za.d);
         this.a(ip, zq.b, var2);
      } catch (Exception var3) {
         a.error(var3, var3);
      }

   }

   public void a(DC validation) {
      try {
         lY id = validation.a();
         if (this.server.l().a(id.D())) {
            Jt var3;
            if (validation.b() && validation.c() && validation.h()) {
               var3 = this.server.l().b(id.D());
               this.server.l().a(var3, id.E(), validation);
            } else {
               var3 = this.server.l().b(id.D());
               a.info("Validation booting " + var3 + "; reason: " + validation.f());
               this.server.l().a(var3, "Validation failed: " + validation.f(), "connection lost");
            }
         } else {
            eE reqInfo = (eE) this.joiningList.a(id.f());
            if (reqInfo == null) {
               a.info("Ignoring join request: no valid context for " + id);
               return;
            }

            if (!ty.a(reqInfo.d, (Object) id.b())) {
               a.info("Remapping " + id.b() + " to " + reqInfo.d);
               id.a(reqInfo.d);
            }

            reqInfo.a = id;
            String origReason = this.checkJoin(reqInfo);
            String failReason = CustomJoin.checkJoin(makeJoinReqInfo(reqInfo),
                    this.server.m().a(), origReason);

            if (id.a()) { //if id.isLoggedIn()
               if (failReason != null) {
                  this.a(reqInfo, false, failReason, null); // this.join fail
               } else {
                  if (validation.b() && validation.c() && validation.h()) {
                     this.a(reqInfo, true, null, validation);
                  } else {
                     this.a(reqInfo, false, validation.f(), validation);
                  }
               }
            }
         }
      } catch (Exception var5) {
         a.error(var5, var5);
      }

   }

   public void a(eE var1, boolean var2, String var3, DC var4) {
      lY var5 = var1.a;
      UUID var6 = var5.D();
      DZ var7 = var5.b();
      if (!mF.a.equals(var6)) {
         if (this.server.l().a(var6)) {
            a.info(var7 + " attempted to join while already connected; disconnecting");
            this.server.l().a(this.server.l().b(var6), null, (String) null);
         } else if (this.server.l().a(var7)) {
            a.info(var7 + " attempted to join while already connected; disconnecting");
            this.server.l().a(this.server.l().b(var7), null, (String) null);
         }
      }

      gk var8 = UY.a(Za.a);
      var8.a(var2);
      if (!var2) {
         var8.a(var3);
      }

      this.a(var7, zq.c, var8);
      if (var2) {
         this.server.l().a(var5, var4, var1.e);
      }

   }

   private String checkJoin(eE joinReq) {
      long time = System.currentTimeMillis();
      long banExpires = this.server.m().R().a(joinReq.a);
      if (banExpires > time) {
         long var12 = banExpires - time;
         String var14 = this.server.m().R().b(joinReq.a);
         var14 = var14 + "\n\nThis ban expires in " + me.a(var12, true) + ".\n\nOnly the player hosting this server can repeal your ban.\nNimbly Games is not affiliated with this server.";
         return var14;
      } else {
         String message;
         if (!joinReq.d.k()) {
            String var6 = this.server.m().p();
            if (var6 != null && var6.length() > 0 && !ty.a(var6, (Object) joinReq.c)) {
               if (joinReq.c.length() == 0) {
                  message = "Private Server requires Secret Code";
               } else {
                  message = "Incorrect Secret Code";
               }

               return message;
            }

            int var7 = ql.a(joinReq.e.a(), joinReq.e.b()).c();
            int var8 = this.server.m().n();
            if (var8 != 0 && var7 < var8) {
               String var15 = "To join this server you must be level " + var8 + " or higher.";
               var15 = var15 + "\nYou are level " + var7 + ".";
               return var15;
            }

            int var9 = this.server.m().o();
            if (var9 != 0 && var7 > var9) {
               String var10 = "To join this server you must be level " + var9 + " or lower.";
               var10 = var10 + "\nYou are level " + var7 + ".";
               return var10;
            }
         }

         PK var11 = this.server.l().n().C();
         if (!joinReq.b.equals(var11)) {
            if (joinReq.b.a(var11)) {
               message = "Server is running a newer version of Altitude.\n\nRestart Altitude to update.";
            } else {
               message = "Server is running an outdated version of Altitude.";
            }

            return message;
         } else {
            return !this.server.l().a(joinReq.a) ? "Server is full" : null;
         }
      }
   }

   private void parseJoinReq(DZ ip, LG msg) {
      PK myVersion = this.server.l().n().C();
      PK clientVersion = null;
      String pass;
      lY id;
      b level;
      String failReason;

      // Attempt to read the client's version, password, level, and ID from the
      // message.
      try {
         clientVersion = PK.a(msg.j());
         pass = msg.j();
         level = (b) b.a.b(msg);
         id = (lY) Sj.d.b(msg);
      } catch (Exception e) {
         // If it fails, there's probably a version mismatch
         a.info("Failed to parse join request from " + ip);
         if (clientVersion != null && !clientVersion.a(myVersion)) {
            failReason = "Server is running an outdated version of Altitude.";
         } else {
            failReason = "Server is running a newer version of Altitude.\n\nRestart Altitude to update.";
         }

         gk out = UY.a(Za.a);
         out.a(false);
         out.a(failReason);
         this.a(ip, zq.c, out);
         return;
      }

      // The data was parsed successfully, let's log that we're doing something
      // with it.
      a.info("Handling join request from " + ip + " -> " + id);
      // If the IP address we received the packet from doesn't match the IP
      // associated with the user through vapor (e.g. when connecting over
      // lan, the vapor IP will be the global IP and the packetIP the LAN IP)
      // , so we map one to the other.
      if (!ip.equals(id.b())) {
         a.info("join request IP mismatch; packetIp=" + ip + " vs vaporIp=" + id.b());
         id.a(ip);
      }

      // Create the JoinReqInfo object from the previously read data
      eE reqInfo = new eE(id, clientVersion, pass, ip, level); //join req info

      // Run the vanilla `checkJoin` function, saving the response to pass to
      // our custom handler.
      String origReason = this.checkJoin(reqInfo);
      // Call our custom handler
      failReason = CustomJoin.checkJoin(makeJoinReqInfo(reqInfo),
              this.server.m().a(), origReason);

      // If the handler refuses the request
      if (failReason != null) {
         // Refuse the join request with the given message
         this.a(reqInfo, false, failReason, null);
      } else {
         // Otherwise, start the process for the client to fully join the
         // server.
         this.joiningList.a(id.f(), reqInfo);
         if (!ip.k()) {
            if (!mF.a.equals(id.D())) {
               if (this.server.l().a(id.D())) {
                  a.info(id + " attempted to join while already connected; disconnecting");
                  this.server.l().a(this.server.l().b(id.D()), null, (String) null);
               } else if (this.server.l().a(id.b())) {
                  a.info(id + " attempted to join while already connected; disconnecting");
                  this.server.l().a(this.server.l().b(id.b()), null, (String) null);
               }
            }
            this.server.d().a(id);
         } else {
            this.a(reqInfo, true, null, null);
         }

      }
   }

   private static JoinReqInfo makeJoinReqInfo(eE o) {
      JoinReqInfo ret = new JoinReqInfo();
      ret.vaporID = o.a.D();
      ret.nick = o.a.F();
      ret.password = o.c;
      ret.ace = o.e.a();
      ret.level = o.e.b();
      ret.kills = o.e.c();
      ret.deaths = o.e.d();
      return ret;
   }

   public void a(float var1) {
      this.joiningList.a(var1);
   }

   public void a(DZ var1, LG var2) {
      try {
         switch (UY.a(var2)) {
            case a:
               this.parseJoinReq(var1, var2);
               break;
            case b:
               qm var3 = (qm) qm.b.b(var2);
               this.server.l().a(var1, var3);
               break;
            case c:
               if (this.server.l().a(var1)) {
                  Jt var4 = this.server.l().b(var1);
                  a.info("Server got disconnect request from " + var4);
                  this.server.l().a(var4, null, (String) null);
               }
               break;
            case d:
               if (this.server.l().a(var1)) {
                  this.server.l().b(var1).j();
               }
         }
      } catch (Exception var5) {
         a.error(var5, var5);
      }

   }

   static class eE {
      lY a;
      PK b;
      String c;
      DZ d;
      b e;

      private eE(lY var2, PK var3, String var4, DZ var5, b var6) {
         this.a = var2;
         this.b = var3;
         this.c = var4;
         this.d = var5;
         this.e = var6;
      }
   }
}
