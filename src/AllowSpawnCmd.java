import java.util.UUID;

class AllowSpawnCmd extends Command {
   private IntParam player_no;

   public AllowSpawnCmd(fe game) {
      super(game, "allowSpawn", PermissionGroup.admin);
      this.player_no = new IntParam("playerNo", "Player number");
      this.setParams(new JI[]{player_no});
   }

   protected void execute(int[] a, String... b) {
      dp playerCont = this.getGame().J().u();
      mF player = playerCont.c(player_no.value);
      if(player != mF.c) {
         UUID vaporID = player.e().a().D();
         this.getServer().l().canSpawn.put(vaporID, true);
         this.getServer().l().spawnDenyReason.remove(vaporID);
      }
   }
}
