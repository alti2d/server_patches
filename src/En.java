import mods.Config;
import org.apache.log4j.Logger;

import java.util.*;

// BotManager
// - added `removeBot()`
// - added `setPlaneSetup()`
public class En {
   private static final Logger log = Logger.getLogger(En.class);
   private final fN server;
   private final aaz dispatcher;
   private final Rl resourceMgr;
   private List<fe> bots;
   private ke latencyConf;
   private int firstPort = 100001;
   private int maxPort;
   private int port;
   private jV defaultSetup = null;

   public En(fN var1, aaz var2) {
      this.maxPort = this.firstPort + 1000;
      this.port = this.firstPort;
      this.server = var1;
      this.dispatcher = var2;
      this.resourceMgr = var1.l().n().G();
      this.bots = new ArrayList<>();
      this.latencyConf = Zj.a();
   }

   void setPlaneSetup(int index, jV config) {
      if (index < -1) {
         log.error("setPlaneSetup invalid index: " + index);
      } else if(index == -1) {
         this.defaultSetup = config;
      } else if (index == 0) {
         this.defaultSetup = config;
         for (fe bot : this.bots) {
            Mg controller = bot.u().f();
            if (controller != null && controller instanceof UE)
               ((UE) controller).setNextSetup(config);
         }
      } else if ((index - 1) < this.bots.size()) {
         Mg controller = this.bots.get(index - 1).u().f();
         if (controller != null && controller instanceof UE)
            ((UE) controller).setNextSetup(config);
      } else {
         log.error("setPlaneSetup invalid index: " + index);
      }
   }

   void addBot(rT difficulty, int tutorialStage) {
      fe var3 = this.a();
      UE controller = new UE(var3, difficulty, tutorialStage);
      controller.setNextSetup(this.defaultSetup);
      var3.u().a(controller);
   }

   void removeBot() {
      if(this.bots.size() > 0) {
         int index = this.bots.size() - 1;
         this.bots.get(index).J().a((String) null);
         this.bots.get(index).f();
         this.bots.remove(index);
         --this.port;
      }
   }

   public void a(float diff) {
      for (int i = 0; i < this.bots.size(); ++i) {
         try {
            this.bots.get(i).a(diff);
         } catch (Exception e) {
            log.error("Error updating fakePlayer " + i, e);
         }
      }
   }

   public fe a() {
      int port = this.nextPort();
      Gg conf = new Gg();
      conf.b().a(port);
      conf.h().a(this.server.l().n().u().m());
      conf.h().a(new DZ(DZ.c, port));
      conf.h().a(this.server.l().n().u().n());
      conf.h().a(this.latencyConf);

      Config mod_conf = Config.get(server.port());

      List<String> name_list = mod_conf.bots.names;

      int bot_index = (port - this.firstPort);
      String bot_name = Integer.toString(1 + bot_index);
      if(bot_index < name_list.size()){
         bot_name = name_list.get(bot_index);
      }

      conf.h().a(true, this.server, this.dispatcher, this.resourceMgr,  mod_conf.bots.name_prefix + bot_name);
      fe bot = new fe(conf);
      log.info("FPM: initializing bot on port " + port);

      try {
         bot.d(); // bot.start();
         this.bots.add(bot);
      } catch (Exception e) {
         log.error(e, e);
      }

      return bot;
   }

   public List b() {
      return this.bots;
   }

   public void a(rT difficulty, int tutorialStage) {
      this.addBot(difficulty, tutorialStage);
   }

   private int nextPort() {
      // Map the collection of bots to the set of used ports
      Set set = nT.a(this.bots, (new Lo(this)));

      if (set.size() >= this.maxPort - this.firstPort + 1) {
         throw new RuntimeException("All ports in use");
      } else {
         while (set.contains(this.port)) {
            ++this.port;
            if (this.port > this.maxPort) {
               this.port = this.firstPort;
            }
         }
         return this.port++;
      }
   }

   public void c() {
      for (fe bot : this.bots) { bot.f(); }
   }
}
