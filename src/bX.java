import org.apache.log4j.Logger;

import java.util.*;
import java.util.Map.Entry;

// OneLifeGameMode.
// - Methods added for timer manipulation.
public class bX extends cV {
   private static final Logger e = Logger.getLogger(bX.class);
   public static final int a = Math.round(sx.d(3.0F).a());
   public static final int b = Math.round(sx.d(4.0F).a());
   public static final int c = Math.round(sx.d(22.0F).a());
   private G f;
   private int g;
   private int h;
   private oU i = new oU(sx.d(2.0F));
   private boolean j = false;
   private oU k = new oU(sx.d(2.0F));
   private oU l;
   private oU m;
   private oU n = new oU(sx.d(4.0F), true);
   private oD o;
   private String p;
   private oD q;
   private boolean r;
   private int s;
   private boolean t;
   private List u;
   private oD v;
   private oD w;
   private oD x;
   private oD y;
   private Map z;
   private Map A;
   private CL B;
   private FD C;
   private boolean D;
   private boolean E;
   private boolean F;
   private boolean G;
   private boolean H;
   private int I;
   private int J;
   private mN K;
   private Eb L;
   // $FF: synthetic field
   private static int[] M;

   public int getRoundTime() {
      return this.h;
   }

   public int getInitialRoundTime() {
      return this.g;
   }

   public void setRoundTime(int time) {
      this.h = time;
   }

   public bX(DK var1, G var2, UV var3) {
      super(var1, var2);
      this.o = oD.c;
      this.q = oD.c;
      this.r = false;
      this.z = new HashMap();
      this.A = new HashMap();
      this.B = new CL(1280, 720);
      this.G = false;
      this.H = false;
      this.f = var2;
      this.s = this.f.b();
      List var4 = var1.h().f().h();
      LK var5 = null;
      HashSet var6 = new HashSet();

      LK var7;
      for (Iterator var8 = var4.iterator(); var8.hasNext(); var6.add(var7.aU())) {
         var7 = (LK) var8.next();
         if (var5 == null || var7.f_().c < var5.f_().c) {
            var5 = var7;
         }
      }

      this.v = var5.aU();
      var6.remove(this.v);
      this.w = (oD) var6.iterator().next();
      HashSet var12 = new HashSet();
      Iterator var9 = var1.h().f().j().iterator();

      while (var9.hasNext()) {
         Eb var13 = (Eb) var9.next();
         var12.add(var13.aU());
      }

      NG var14 = var1.h().f().d();
      ArrayList var15 = new ArrayList();
      Iterator var11 = var1.h().f().i().iterator();

      while (var11.hasNext()) {
         HM var10 = (HM) var11.next();
         if (var10.m().contains(LX.f)) {
            var15.add(var10);
            var10.c(true);
            var10.b(9999.0F);
            if (var14.a(var10.f_().c, var10.f_().d)) {
               this.D = true;
            }
         }
      }

      this.E = var15.size() == 1 && !((HM) var15.get(0)).aU().equals(oD.c);
      if (var12.size() == 1) {
         this.y = (oD) var12.iterator().next();
      } else {
         this.y = this.v;
      }

      this.x = this.y == this.v ? this.w : this.v;
      if (var12.size() == 0) {
         this.C = FD.a;
      } else if (var12.size() == 1) {
         this.C = FD.b;
      } else if (var12.size() == 2) {
         this.C = FD.c;
      }

      this.u = nT.a((Object[]) (new oD[]{this.v, this.w}));
      var11 = nT.a((List) this.u, (Object[]) (new oD[]{oD.c})).iterator();

      while (var11.hasNext()) {
         oD var16 = (oD) var11.next();
         this.z.put(var16, new Integer(0));
         this.A.put(var16, new Integer(0));
      }

      this.g = var3.a() * 30;
      this.h = this.g;
      this.l = new oU(sx.d((float) var2.d()));
      this.m = new oU(new qt(this.l.c() * -2.0F * 30.0F), true);
   }

   public void a() {
      super.a();
      this.i.d();
      this.h = this.g;
      Iterator var2 = this.u.iterator();

      while (var2.hasNext()) {
         oD var1 = (oD) var2.next();
         this.A.put(var1, Integer.valueOf(0));
      }

   }

   private boolean O() {
      int var1 = ((Integer) this.A.get(this.v)).intValue();
      int var2 = ((Integer) this.A.get(this.w)).intValue();
      boolean var3 = Math.abs(var1 - var2) >= this.f.c();
      boolean var4 = Math.max(var1, var2) > this.f.a() / 2;
      boolean var5 = var1 + var2 >= this.f.a();
      return var3 && (var4 || var5);
   }

   public void b() {
      this.B.e_();
      this.T();
      if (!this.w()) {
         if (!this.L()) {
            if (!this.m.e()) {
               if (this.n.e()) {
                  this.m.b();
               }

               if (this.m.e() && this.t().B() && this.O()) {
                  this.G();
               }
            }

            if (this.t().F()) {
               this.P();
            }

            if (!this.l.e()) {
               if (this.n.e()) {
                  this.l.b();
               }

               if (this.l.e() && this.t().B()) {
                  this.a((byte) 0);
               }
            } else {
               if (this.h > 0) {
                  --this.h;
               }

               this.i.b();
               if (this.i.e() && this.t().B()) {
                  this.S();
               }
            }

            if (!this.n.e()) {
               this.n.b();
               if (this.t().F() && (int) this.n.h() == (int) (this.n.c() / 2.0F)) {
                  String var1 = "Switching Sides";
                  this.a(ip.h, var1, 5.0F);
                  kr var2 = this.t().G().b("map/mode/switching_sides.wav");
                  DA var3 = this.t().ah().a(var2);
                  var3.l();
                  this.z();
               }

               if (this.n.e()) {
                  this.U();
               }
            }

         }
      }
   }

   public String c() {
      switch (m()[this.C.ordinal()]) {
         case 1:
            return "1-Life Deathmatch";
         case 2:
         case 3:
            return this.D ? "1-Life Demolition" : "1-Life Base Destruction";
         default:
            throw new RuntimeException("unknown mode");
      }
   }

   public String a(oD var1) {
      boolean var2 = this.x.equals(this.t().J().u().ad()) || this.C == FD.c;
      switch (m()[this.C.ordinal()]) {
         case 1:
            return "Eliminate all enemy planes.";
         case 2:
         case 3:
            if (var2) {
               return this.D ? "Plant the demolition charge on the enemy base." : "Use the big bomb to destroy the enemy base.";
            }

            return this.D ? "Defend your base from enemy demolition charges." : "Defend your base from enemy bombs.";
         default:
            throw new RuntimeException("unknown mode");
      }
   }

   private void P() {
      if (this.l.e() && this.i.e()) {
         kr var1;
         DA var2;
         if (!this.G && this.R()) {
            this.G = true;
            this.a(this.x.f(), this.x.h() + " Eliminated", 3.0F);
            var1 = this.t().G().b("planes/bomber/lock_acquired.wav");
            var2 = this.t().ah().a(var1);
            var2.l();
         }

         if (!this.H && this.Q()) {
            this.H = true;
            this.a(this.y.f(), this.y.h() + " Eliminated", 3.0F);
            var1 = this.t().G().b("planes/bomber/lock_acquired.wav");
            var2 = this.t().ah().a(var1);
            var2.l();
         }
      }

   }

   private boolean Q() {
      return !this.r && this.J == 0;
   }

   private boolean R() {
      return !this.r && this.I == 0;
   }

   private void S() {
      if (this.j) {
         this.k.b();
      }

      int var1 = 0;
      int var2 = 0;
      boolean var3 = false;
      boolean var4 = false;
      List var5 = this.t().R().f().j();

      for (int var6 = 0; var6 < var5.size(); ++var6) {
         Eb var7 = (Eb) var5.get(var6);
         if (var7.aU().equals(this.x)) {
            var1 += var7.G().size();
            var3 |= !var7.F();
         } else if (var7.aU().equals(this.y)) {
            var2 += var7.G().size();
            var4 |= !var7.F();
         }
      }

      boolean var13 = var4 && var1 == 0;
      boolean var14 = var3 && var2 == 0;
      boolean var8 = this.Q() && var1 == 0 && !var3;
      boolean var9 = this.R() && var2 == 0 && !var4;
      boolean var10 = false;
      var10 |= var4 && var1 > 0 && this.R();
      var10 |= var3 && var2 > 0 && this.Q();
      if (var10 || var13 && var14) {
         this.a(oD.c, "Mutual Base Destruction");
      } else if (!var13 && !var14) {
         if (this.F && this.E) {
            this.a(this.y, "Bomb Defused");
         } else if (var9 && var8) {
            this.a(oD.c, "Mutual Elimination");
         } else {
            oD var12;
            if (!var9 && !var8) {
               if (this.h <= 0 && var1 == 0 && var2 == 0) {
                  int var15 = this.I - this.J;
                  switch (m()[this.C.ordinal()]) {
                     case 1:
                     case 3:
                        var12 = oD.c;
                        if (var15 != 0) {
                           var12 = var15 > 0 ? this.x : this.y;
                        }

                        this.a(var12, "Timer Expired");
                        break;
                     case 2:
                        this.a(this.y, "Timer Expired");
                  }
               } else if (this.j) {
                  this.j = false;
                  this.k.d();
               }
            } else {
               oD var11 = var8 ? this.x : this.y;
               var12 = var8 ? this.y : this.x;
               this.a(var11, var12.h() + " Eliminated");
            }
         }
      } else {
         this.a(var13 ? this.x : this.y, "Base Destroyed");
      }

   }

   private void a(byte var1) {
      VS var2 = this.t().u().c().l();
      cG var3 = new cG(this.t(), var2, var1);
      var2.a((kY) var3);
   }

   private void T() {
      this.I = 0;
      this.J = 0;
      List var1 = this.t().J().u().j();

      for (int var2 = 0; var2 < var1.size(); ++var2) {
         mN var3 = ((mF) var1.get(var2)).i();
         if (var3 != null && var3.aO()) {
            oD var4 = var3.aU();
            if (var4.equals(this.x)) {
               ++this.I;
            } else if (var4.equals(this.y)) {
               ++this.J;
            }
         }
      }

   }

   public float d() {
      return super.d() * 0.6F;
   }

   public float e() {
      return 0.6F;
   }

   public float f() {
      return 2.5F;
   }

   public boolean g() {
      return this.r ? this.l.e() : false;
   }

   public boolean h() {
      return !this.l.e();
   }

   public void a(gk var1) {
      this.q = oD.c;
      int var2 = ((Integer) this.A.get(this.x)).intValue();
      int var3 = ((Integer) this.A.get(this.y)).intValue();
      if (var2 != var3) {
         this.q = var2 > var3 ? this.x : this.y;
      }

      oD.p.a(this.q, var1);
      this.c(this.q);
      super.a(var1);
   }

   public void a(LG var1) {
      this.q = (oD) oD.p.b(var1);
      Integer var2 = (Integer) this.z.get(this.q);
      Integer var3 = Integer.valueOf(var2.intValue() + 1);
      this.z.put(this.q, var3);
      if (this.t().F()) {
         this.t().J().u().O().a();
      }

      super.a(var1);
   }

   public void a(byte var1, nu var2) {
      if (var1 == 1) {
         oD.p.a(this.o, var2);
         var2.a(this.p);
         var2.a(this.h);
         var2.a(0, 63, this.s);
      } else if (var1 == 0) {
         var2.a(this.t);
      } else if (var1 == 2) {
         this.t().a(var2, this.t().a((nq) this.K));
         this.t().a(var2, this.t().a((nq) this.L));
      }

   }

   public void a(byte var1, nQ var2) {
      if (var1 == 1) {
         this.o = (oD) oD.p.b(var2);
         this.p = var2.j();
         this.h = var2.g();
         float var3 = sx.e((float) (this.g - this.h));
         this.s = var2.a(0, 63);
         Integer var4 = (Integer) this.A.get(this.o);
         Integer var5 = Integer.valueOf(var4.intValue() + 1);
         this.A.put(this.o, var5);
         if (this.t().F()) {
            String var6 = this.o.equals(oD.c) ? "Tie" : this.o.h() + " Wins";
            this.a(ip.h, var6, 5.0F);
            boolean var7 = this.t().J().u().i().e().c().equals(this.o);
            String var8 = var7 ? "map/mode/team_wins.wav" : "map/mode/team_loses.wav";
            kr var9 = this.t().G().b(var8);
            DA var10 = this.t().ah().a(var9);
            var10.l();
         }

         this.l.d();
         this.m.d();
         Iterator var24 = this.t().R().f().j().iterator();

         while (var24.hasNext()) {
            Eb var21 = (Eb) var24.next();
            Iterator var26 = var21.G().iterator();

            while (var26.hasNext()) {
               JW var25 = (JW) var26.next();
               var25.a();
            }

            var21.G().clear();
         }

         var24 = this.t().J().u().k().iterator();

         while (var24.hasNext()) {
            mN var22 = (mN) var24.next();
            var22.aI();
            if (var22.aO()) {
               var22.aL();
               var22.aT().a(var22);
            }
         }

         this.G = true;
         this.H = true;
         if (this.s > 0) {
            --this.s;
            if (this.s == 0) {
               if (!this.O()) {
                  this.n.d();
               }

               this.s = this.f.b();
            }
         }

         if (this.t().B()) {
            try {
               da var23 = JL.a("objectiveGameEnd");
               var23.a("winningTeam", this.o.c());
               JL.a(var23, "timeSeconds", var3);
               this.t().u().c().a(var23);
            } catch (Exception var13) {
               e.error(var13, var13);
            }
         }
      } else if (var1 == 0) {
         boolean var14 = var2.d();
         this.a(var14);
         if (this.t().B()) {
            try {
               da var17 = JL.a("objectiveGameStart");
               var17.a("leftTeam", this.v.c());
               var17.a("rightTeam", this.w.c());
               this.t().u().c().a(var17);
            } catch (Exception var12) {
               e.error(var12, var12);
            }
         }
      } else if (var1 == 2) {
         if (!this.t().B()) {
            this.K = (mN) this.t().a(var2, mN.class).b(this.t());
            this.L = (Eb) this.t().a(var2, Eb.class).b(this.t());
         }

         Iterator var19 = this.L.G().iterator();

         while (var19.hasNext()) {
            JW var15 = (JW) var19.next();
            var15.a();
         }

         this.L.G().clear();
         if (this.t().F()) {
            kr var16 = this.t().G().b("map/mode/bomb_defused.wav");
            DA var20 = this.t().ah().a(var16);
            this.t().J().u().N().a("Defused Charge!", var20, this.K.aT(), this.K);
         }

         if (this.t().B()) {
            try {
               da var18 = JL.a("demolitionChargeDefused");
               var18.a("player", this.K.aS());
               this.t().u().c().a(var18);
            } catch (Exception var11) {
               e.error(var11, var11);
            }
         }
      }

   }

   public void a(mN var1, Eb var2) {
      if (this.t().F() && this.l.e()) {
         SR var3 = LX.a(this.t(), LX.f);
         var3.a(var1.aU());
         var3.f_().a(((JW) var2.G().get(0)).f_());
         Sq var4 = new Sq(this, this.t(), (mN) null, var3, var2);
         this.t().R().b(var4);
         kr var6 = this.t().G().b("map/mode/bomb_planted.wav");
         DA var8 = this.t().ah().a(var6);
         this.t().J().u().N().a("Planted Charge!", var8, var1.aT(), var1);
      }

      if (this.t().B() && this.l.e()) {
         try {
            da var7 = JL.a("demolitionChargePlanted");
            var7.a("player", var1.aS());
            this.t().u().c().a(var7);
         } catch (Exception var5) {
            e.error(var5, var5);
         }
      }

   }

   public void b(mN var1, Eb var2) {
      if (!this.t().B()) {
         e.warn("OID_NS");
      } else {
         if (var1.aO() && var2.G().size() > 0) {
            this.K = var1;
            this.L = var2;
            Er var3 = new Er(this.t(), var1, var2, 20);
            this.t().u().c().l().a((kY) var3);
            this.a((byte) 2);
            this.F = true;
         }

      }
   }

   private void a(oD var1, String var2) {
      if (!this.j) {
         this.j = true;
         if (this.k.h() > (float) this.h) {
            this.k.c((float) this.h);
         }
      } else if (this.k.e()) {
         this.o = var1;
         this.p = var2;
         this.a((byte) 1);
      }

   }

   private void a(boolean var1) {
      if (var1 != this.t) {
         this.U();
      }

      Iterator var3 = this.J().values().iterator();

      while (var3.hasNext()) {
         Hv var2 = (Hv) var3.next();
         var2.b();
      }

      dp var7 = this.t().J().u();
      if (!this.t().B()) {
         oD var8 = var7.i().e().c();
         if (!ty.a((Object) var8, (Object) oD.c) || this.t().D()) {
            var7.T();
         }
      }

      this.A();
      Iterator var4 = this.t().R().f().j().iterator();

      while (var4.hasNext()) {
         Eb var9 = (Eb) var4.next();
         byte var5 = 20;
         int var6 = (int) Math.ceil((double) ((float) var5 * 48.0F));
         var9.b(var6);
      }

      this.i.d();
      this.j = false;
      this.k.d();
      this.h = this.g;
      this.F = false;
      this.G = false;
      this.H = false;
   }

   private void U() {
      this.t = !this.t;
      this.n.j();
      oD var1 = this.v;
      oD var2 = this.w;
      this.v = var2;
      this.w = var1;
      oD var3 = this.x;
      oD var4 = this.y;
      this.x = var4;
      this.y = var3;
      Iterator var6 = this.t().R().f().e().values().iterator();

      while (var6.hasNext()) {
         List var5 = (List) var6.next();
         Iterator var8 = var5.iterator();

         while (var8.hasNext()) {
            Yl var7 = (Yl) var8.next();
            if (var7 instanceof ou) {
               ou var9 = (ou) var7;
               oD var10 = var9.aU();
               if (ty.a((Object) var10, (Object) var3)) {
                  var9.a(var4);
               } else if (ty.a((Object) var10, (Object) var4)) {
                  var9.a(var3);
               }
            }
         }
      }

      this.n();
   }

   public void a(nQ var1) {
      super.a(var1);
      boolean var2 = var1.d();
      if (var2 != this.t) {
         this.U();
      }

      this.s = var1.a(0, 63);
      Iterator var4 = this.u.iterator();

      while (var4.hasNext()) {
         oD var3 = (oD) var4.next();
         this.z.put(var3, Integer.valueOf(var1.a(0, 31)));
         this.A.put(var3, Integer.valueOf(var1.a(0, 255)));
      }

      if (this.V()) {
         this.h = var1.a(0, this.g);
      }

      this.l.c((float) var1.a(0, (int) this.l.c() + 1));
   }

   public void a(nu var1) {
      super.a(var1);
      var1.a(this.t);
      var1.a(0, 63, this.s);
      Iterator var3 = this.u.iterator();

      while (var3.hasNext()) {
         oD var2 = (oD) var3.next();
         var1.a(0, 31, ((Integer) this.z.get(var2)).intValue());
         var1.a(0, 255, ((Integer) this.A.get(var2)).intValue());
      }

      if (this.V()) {
         var1.a(0, this.g, this.h);
      }

      var1.a(0, (int) this.l.c() + 1, (int) this.l.h());
   }

   public void a(float var1) {
      super.a(var1);
      this.B.a(var1);
   }

   public void b(float var1) {
      super.b(var1);
      NY var2 = this.t().G().a(KN.r);
      qd.b(this.v.f());
      int var3 = 715;
      int var4 = ((Integer) this.A.get(this.v)).intValue();
      int var5 = ((Integer) this.A.get(this.w)).intValue();
      int var6 = Math.max(0, this.f.a() - var4 - var5);
      int var7 = var2.a((CharSequence) String.valueOf(var6));
      short var8 = 500;
      int var9 = var7 / 2 + 11;
      qd.a(0.8F, 0.8F, 0.8F, 1.0F);
      var2.a(String.valueOf(var6), var8, var3, 17);
      qd.b(this.v.f());
      var2.a(String.valueOf(var4), var8 - var9, var3, 24);
      qd.b(this.w.f());
      var2.a(String.valueOf(var5), var8 + var9, var3, 20);
      int var10;
      int var11;
      if (this.v == this.x) {
         var10 = this.I;
         var11 = this.J;
      } else {
         var10 = this.J;
         var11 = this.I;
      }

      int var12 = var2.a((CharSequence) " vs ");
      short var13 = 780;
      int var14 = var12 / 2;
      qd.a(0.8F, 0.8F, 0.8F, 1.0F);
      var2.a(" vs ", var13, var3, 17);
      qd.b(this.v.f());
      var2.a(String.valueOf(var10), var13 - var14, var3, 24);
      qd.b(this.w.f());
      var2.a(String.valueOf(var11), var13 + var14, var3, 20);
      qd.a(0.8F, 0.8F, 0.8F, 1.0F);
      if (this.x()) {
         float var15 = 1000.0F * this.e(var1);
         float var16 = 1000.0F * this.y();
         String var17 = me.a((long) var15, (long) var16);
         var2.a("Round: " + var17, 640, var3, 17);
         var3 -= 25;
      }

      if (this.L()) {
         var2.a("Warmup ends in " + (int) Math.ceil((double) sx.e((float) this.M())), 640, var3, 17);
      } else if (!this.l.e() && this.m.e()) {
         var2.a("Game starts in " + (int) Math.ceil((double) sx.e(this.l.h())), 640, var3, 17);
      } else if (this.V()) {
         long var20 = (long) Math.round(sx.f((float) this.h));
         long var21 = (long) Math.round(sx.f((float) this.g));
         String var19 = me.a(var20, var21);
         var2.a(var19, 640, var3, 17);
      }

   }

   private boolean V() {
      return this.g > 0;
   }

   public oD b(oD var1) {
      return var1 == this.x ? this.y : this.x;
   }

   public List i() {
      return this.u;
   }

   public void a(Hc var1) {
      Map var2 = this.k();
      Iterator var4 = var2.entrySet().iterator();

      while (var4.hasNext()) {
         Entry var3 = (Entry) var4.next();
         var1.a((oD) var3.getKey(), ((Integer) var3.getValue()).intValue());
      }

   }

   public yr j() {
      Eb var1 = null;
      if (this.q != oD.c) {
         var1 = new Eb(this.t());
         var1.a(this.q);
         lE var2 = new lE();
         var2.d = this.q == this.w;
         var1.a(var2);
         var1.c(this.t());
      }

      return new Wu(this.t(), this.q, this.v, this.w, this.A);
   }

   public Map k() {
      return this.z;
   }

   public List l() {
      return Sr.m;
   }

   // $FF: synthetic method
   static int[] m() {
      int[] var10000 = M;
      if (M != null) {
         return var10000;
      } else {
         int[] var0 = new int[FD.values().length];

         try {
            var0[FD.b.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            var0[FD.a.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            var0[FD.c.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

         M = var0;
         return var0;
      }
   }
}
