import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class cW extends Os {
   private static final Logger a = Logger.getLogger(cW.class);

   public cW(fe var1) {
      super("logServerStatus", var1);
   }

   public String a() {
      return null;
   }

   public String b() {
      return null;
   }

   protected boolean a(NB var1, FW var2, Jt var3, String[] var4) {
      return true;
   }

   protected void a(int[] var1, String... var2) {
      fN var3 = this.j.u().c();
      ArrayList ids = new ArrayList();
      ArrayList nicknames = new ArrayList();
      ArrayList ips = new ArrayList();
      ArrayList vaporIds = new ArrayList();
      ArrayList aceRanks = new ArrayList();
      ArrayList levels = new ArrayList();
      ArrayList loggedInTimes = new ArrayList();
      Iterator clients = var3.l().f().iterator();

      while(clients.hasNext()) {
         Jt client = (Jt) clients.next();
         ids.add(client.g());
         nicknames.add(client.f().E());
         ips.add(client.e().toString());
         vaporIds.add(client.f().D().toString());
         loggedInTimes.add(client.f().g());
         aceRanks.add(client.level.a());
         levels.add(client.level.b());
      }

      boolean var11 = var3.l().r();

      try {
         da var12 = JL.a("logServerStatus");
         var12.a("playerIds", ids);
         var12.a("nicknames", nicknames);
         var12.a("ips", ips);
         var12.a("vaporIds", vaporIds);
         var12.a("loggedInTimes", loggedInTimes);
         var12.a("aceRanks", aceRanks);
         var12.a("levels", levels);
         var12.a("tournamentInProgress", var11);
         var3.a(var12);
      } catch (Exception var10) {
         a.error(var10, var10);
      }

   }
}
